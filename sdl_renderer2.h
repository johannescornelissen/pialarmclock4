#pragma once

/*
https://www.reddit.com/r/rust_gamedev/comments/am84q9/using_sdl2_gfx_on_windows/
.\vcpkg.exe install sdl2-gfx --triplet x64-windows

todo: SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");
https://discourse.libsdl.org/t/drawing-lines-with-float-coordinates/27140
prob. not helping..
*/


#include <iostream>
#include <cstdlib>
#include <vector>
#include <unordered_map>
#include <mutex>

#include <filesystem>
namespace fs = std::filesystem;


#ifdef __linux__
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#else
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#endif

#include "SDL2_gfx/SDL2_gfxPrimitives.h"
#include "SDL2_gfx/SDL2_rotozoom.h"

class SDL2
{
public:
	bool sdl_init = false;
	bool ttf_init = false;
	bool mix_init = false;
	bool img_init = false;

	SDL2(bool aShowMouse)
	{
		// SDL init
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER | SDL_INIT_EVENTS) < 0)
		{
			std::cerr << "## could not initialize sdl2, error: " << SDL_GetError() << std::endl;
		}
		else if (TTF_Init() < 0) // init SDL ttf
		{
			sdl_init = true;
			std::cerr << "## could not initialize sdl2 ttf, error: " << TTF_GetError() << std::endl;
		}
		else if (!(Mix_Init(MIX_INIT_MP3) & MIX_INIT_MP3))
		{
			sdl_init = true;
			ttf_init = true;
			std::cerr << "## could not initialize sdl2 mixer, error: " << Mix_GetError() << std::endl;
		}
		else if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
		{
			sdl_init = true;
			ttf_init = true;
			mix_init = true;

			std::cerr << "##could not initialize sdl2 image, error: " << IMG_GetError() << std::endl;
		}
		else
		{
			sdl_init = true;
			ttf_init = true;
			mix_init = true;
			img_init = true;
			if (!aShowMouse)
				SDL_ShowCursor(SDL_DISABLE);
			std::cout << "sdl2 initialized" << std::endl;
		}
		// open audio device
		Mix_Volume(-1, 0);
		Mix_OpenAudioDevice(48000, MIX_DEFAULT_FORMAT, 2, 4096, nullptr, SDL_AUDIO_ALLOW_FORMAT_CHANGE | SDL_AUDIO_ALLOW_SAMPLES_CHANGE);
		std::cout << "audio initialized" << std::endl;
	}
	~SDL2()
	{
		stop();
	}
	void stop()
	{
		std::cout << "sdl stop" << std::endl;
		if (img_init)
		{
			IMG_Quit();
			img_init = false;
		}
		if (mix_init)
		{
			Mix_Quit();
			mix_init = false;
		}
		if (ttf_init)
		{
			TTF_Quit();
			ttf_init = false;
		}
		if (sdl_init)
		{
			SDL_ShowCursor(SDL_ENABLE); // show mouse
			SDL_Quit();
			sdl_init = false;
		}
		std::cout << "sdl stopped" << std::endl;
	}
};


class SDLRenderer
{
public:
	int width = 0;
	int height = 0;
	int depth = 0;
public:
	SDLRenderer(fs::path aFontPath, bool aRenderToWindow = false, int aWidth = 256, int aHeight = 256, int aDepth = 32)
	{
		width = aWidth;
		height = aHeight;
		depth = aDepth;
		fFontPath = aFontPath;
		fSurface = SDL_CreateRGBSurface(0, aWidth, aHeight, aDepth, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
		if (aRenderToWindow)
		{
			fWindow = SDL_CreateWindow("SDL2 output window", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, aWidth, aHeight, SDL_WINDOW_SHOWN);
			fRenderer = SDL_CreateRenderer(fWindow, -1, SDL_RENDERER_ACCELERATED);
		}
		else
		{
			fRenderer = SDL_CreateSoftwareRenderer(fSurface);
		}
	}
	~SDLRenderer()
	{
		stop();
	}
	void stop()
	{
		for (auto& texture : fTextures)
		{
			SDL_DestroyTexture(texture.second);
		}
		fTextures.clear();
		for (auto& surface : fSurfaces)
		{
			SDL_FreeSurface(surface.second);
		}
		fSurfaces.clear();
		for (auto& fontGroup : fFonts)
		{
			if (fontGroup.second)
			{
				for (auto& font : *fontGroup.second)
				{
					if (font.second)
						TTF_CloseFont(font.second);
				}
			}
		}
		fFonts.clear();
		if (fRenderer)
		{
			SDL_DestroyRenderer(fRenderer);
			fRenderer = nullptr;
		}
		if (fWindow)
		{
			SDL_DestroyWindow(fWindow);
			fWindow = nullptr;
		}
		if (fSurface)
		{
			SDL_FreeSurface(fSurface);
			fSurface = nullptr;
		}
		// cleanup cache
		if (gfxPrimitivesPolyIntsCache != nullptr)
		{
			free(gfxPrimitivesPolyIntsCache);
			gfxPrimitivesPolyIntsCache = nullptr;
		}
		gfxPrimitivesPolyAllocatedCache = 0;
	}
	void clear(Uint8 r = 0, Uint8 g = 0, Uint8 b = 0, Uint8 a = SDL_ALPHA_OPAQUE)
	{
		SDL_SetRenderDrawColor(fRenderer, r, g, b, a);
		SDL_RenderClear(fRenderer);
	}
	void update()
	{
		SDL_RenderPresent(fRenderer);
	}
	//SDL_Renderer* getRenderer() { return fRenderer; }
	TTF_Font* getFont(int aSize = 10, std::string aFontName = "")
	{ 
		// if not font name specified use the default ariel
		if (aFontName.length() == 0)
			aFontName = "arial.ttf";
		// find font group (font name) or add when not there yet
		std::shared_ptr<std::unordered_map<int, TTF_Font*>> fg;
		auto fgp = fFonts.find(aFontName);
		if (fgp == fFonts.end())
		{
			fg = std::make_shared<std::unordered_map<int, TTF_Font*>>();
			fFonts.insert({ aFontName, fg });
		}
		else
			fg = fgp->second;
		// find font on size in group, create and add if not there yet
		auto f = fg->find(aSize);
		if (f == fg->end())
		{
			auto newFontPath = fFontPath / aFontName;
			auto font = TTF_OpenFont(newFontPath.string().c_str(), aSize);
			fg->insert({ aSize, font });
			return font;
		}
		// fall through when already in list
		return f->second; 
	} 
public:
	/*
	void setDrawColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE)
	{
		if (a < 255)
			SDL_SetRenderDrawBlendMode(fRenderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(fRenderer, r, g, b, a);
	}
	void setDrawColor(uint32_t color) 
	{ 
		auto a = (Uint8)((color >> 24) & 0xff);
		if (a < 255)
			SDL_SetRenderDrawBlendMode(fRenderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(fRenderer, (Uint8)((color >> 16) & 0xff), (Uint8)((color >> 8) & 0xff), (Uint8)((color >> 0) & 0xff), a); 
	}
	void setDrawColor(uint32_t color, double aAlpha) 
	{ 

		Uint8 alpha = (Uint8)(aAlpha >= 1 ? (color >> 24) & 0xff : ((color >> 24) & 0xff)*aAlpha);
		if (alpha < 255)
			SDL_SetRenderDrawBlendMode(fRenderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(fRenderer, (Uint8)((color >> 16) & 0xff), (Uint8)((color >> 8) & 0xff), (Uint8)((color >> 0) & 0xff), alpha); 
	}
	void drawRect(SDL_Rect aRect) 
	{ 
		SDL_RenderDrawRect(fRenderer, &aRect); 
	}
	*/
	void drawRectFilled(SDL_Rect aRect, uint32_t aColor)
	{
		auto color = colorToSDLColor(aColor);
		if (color.a < 255)
			SDL_SetRenderDrawBlendMode(fRenderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(fRenderer, color.r, color.g, color.b, color.a);
		SDL_RenderFillRect(fRenderer, &aRect);
	}
	void drawRoundedRectFilled(SDL_Rect aRect, int aRR_Radius, uint32_t aColor)
	{
		roundedBoxColor(fRenderer, (Sint16)aRect.x, (Sint16)aRect.y, (Sint16)(aRect.x + aRect.w), (Sint16)(aRect.y + aRect.h), (Sint16)aRR_Radius, aColor);
	}
	void drawLine(int x1, int y1, int x2, int y2, uint32_t color)
	{
		// todo: which to use
		//setDrawColor(color);
		//SDL_RenderDrawLine(fRenderer, x1, y1, x2, y2);
		lineColor(fRenderer, x1, y1, x2, y2, color);
	}
	void drawAALine(int x1, int y1, int x2, int y2, uint32_t aColor)
	{
		aalineColor(fRenderer, Sint16(x1), Sint16(y1), Sint16(x2), Sint16(y2), aColor);
	}
	void drawHLineColor(int x1, int x2, int y, uint32_t aColor)
	{
		hlineColor(fRenderer, (Sint16)x1, (Sint16)x2, (Sint16)y, aColor);
	}
	void drawFilledTrigon(int x1, int y1, int x2, int y2, int x3, int y3, uint32_t aColor)
	{
		auto color = colorToSDLColor(aColor);
		filledTrigonRGBA(fRenderer, Sint16(x1), Sint16(y1), Sint16(x2), Sint16(y2), Sint16(x3), Sint16(y3), color.r, color.g, color.b, color.a);
	}
	void drawAAFilledTrigon(int x1, int y1, int x2, int y2, int x3, int y3, uint32_t aColor)
	{
		auto color = colorToSDLColor(aColor);
		filledTrigonRGBA(fRenderer, Sint16(x1), Sint16(y1), Sint16(x2), Sint16(y2), Sint16(x3), Sint16(y3), color.r, color.g, color.b, color.a);
		aatrigonRGBA(fRenderer, Sint16(x1), Sint16(y1), Sint16(x2), Sint16(y2), Sint16(x3), Sint16(y3), color.r, color.g, color.b, color.a);

	}
	SDL_Rect textRect(const std::string& aText, TTF_Font* aFont, int x, int y)
	{
		int w, h;
		TTF_SizeText(aFont, aText.c_str(), &w, &h);
		return SDL_Rect{ x,y,w,h };
	}
	void drawText(const std::string& aText, TTF_Font* aFont, uint32_t color, SDL_Rect* aRect)
	{
		auto surface = TTF_RenderUTF8_Blended(aFont, aText.c_str(), colorToSDLColor(color));
		drawTexture(surface, aRect);
		SDL_FreeSurface(surface);
	}
	/*
	void drawText(const std::string& aText, TTF_Font* aFont, uint32_t color, int x, int y)
	{
		auto surface = TTF_RenderUTF8_Blended(aFont, aText.c_str(), colorToSDLColor(color));
		SDL_Rect rect{ x,y,surface->w,surface->h };
		drawTexture(surface, &rect);
		SDL_FreeSurface(surface);
	}
	void drawText(const std::string& aText, TTF_Font* aFont, Uint8 r, Uint8 g, Uint8 b, Uint8 a, int x, int y)
	{
		SDL_Color color{ r, g, b, a };
		auto surface = TTF_RenderUTF8_Blended(aFont, aText.c_str(), color);
		auto rect = SDL_Rect{ x,y,surface->w,surface->h };
		drawTexture(surface, &rect);
		SDL_FreeSurface(surface);
	}
	void  drawText(const std::string& aText, TTF_Font* aFont, Uint8 r, Uint8 g, Uint8 b, Uint8 a, SDL_Rect* aRect)
	{
		SDL_Color color{ r, g, b, a };
		auto surface = TTF_RenderUTF8_Blended(aFont, aText.c_str(), color);
		drawTexture(surface, aRect);
		SDL_FreeSurface(surface);
	}
	void drawText(const std::string& aText, int aFontSize, int x, int y, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE)
	{ 
		auto font = getFont(aFontSize);
		if (font)
		{
			SDL_Surface* textSurface = TTF_RenderText_Solid(font, aText.c_str(), { r, g, b, a });
			SDL_Texture* textTexture = SDL_CreateTextureFromSurface(fRenderer, textSurface);
			SDL_Rect renderQuad{ x,y,textSurface->w,textSurface->h }; // SDL_Rect is x,y,w,h
			SDL_FreeSurface(textSurface);
			SDL_RenderCopy(fRenderer, textTexture, nullptr, &renderQuad);
			SDL_DestroyTexture(textTexture);
		}
	}
	*/
	void drawTextCenterAngled(const std::string& aText, const std::string& aFontName, int aFontSize, int x, int y, double aAngle, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE, Uint8 rb = 0, Uint8 gb = 0, Uint8 bb = 0, Uint8 ab = SDL_ALPHA_TRANSPARENT)
	{
		auto font = getFont(aFontSize, aFontName);
		if (font)
		{
			auto textSurface = TTF_RenderText_Blended(font, aText.c_str(), { r, g, b, a });
			auto rotatedTextSurface = aAngle != 0 ? rotozoomSurface(textSurface, aAngle, 1.0, SMOOTHING_ON) : nullptr;
			auto textTexture = SDL_CreateTextureFromSurface(fRenderer, rotatedTextSurface ? rotatedTextSurface : textSurface);
			auto w = rotatedTextSurface ? rotatedTextSurface->w : textSurface->w;
			auto h = rotatedTextSurface ? rotatedTextSurface->h : textSurface->h;
			SDL_Rect textRect{ x - (int)std::round((double)w / 2.0),y - (int)std::round((double)h / 2.0),w,h };
			SDL_FreeSurface(rotatedTextSurface);
			SDL_FreeSurface(textSurface);
			if (ab != SDL_ALPHA_TRANSPARENT)
			{
				// add some extra pixels in front en at end of string half font size 
				SDL_Surface* backgroundSurface = SDL_CreateRGBSurface(0, textSurface->w + aFontSize, textSurface->h, depth, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
				SDL_Rect backgroundSurfaceRect{0,0,backgroundSurface->w,backgroundSurface->h };
				SDL_FillRect(backgroundSurface, &backgroundSurfaceRect, SDL_MapRGB(backgroundSurface->format, rb, gb, bb));
				auto rotatedBackgroundSurface = rotozoomSurface(backgroundSurface, aAngle, 1.0, SMOOTHING_ON);
				if (rotatedBackgroundSurface)
				{
					auto backgroundTexture = SDL_CreateTextureFromSurface(fRenderer, rotatedBackgroundSurface);
					SDL_Rect backgroundRect{ x - (int)std::round((double)rotatedBackgroundSurface->w / 2.0),y - (int)std::round((double)rotatedBackgroundSurface->h / 2.0),rotatedBackgroundSurface->w,rotatedBackgroundSurface->h };
					SDL_RenderCopy(fRenderer, backgroundTexture, nullptr, &backgroundRect);
					SDL_FreeSurface(rotatedBackgroundSurface);
				}
				// else error, just ignore
				SDL_FreeSurface(backgroundSurface);
			}
			SDL_RenderCopy(fRenderer, textTexture, nullptr, &textRect);
			SDL_DestroyTexture(textTexture);
		}
	}
	void drawPoint(int x, int y, uint32_t color) // alpha color from world argb -> r,g,b,a
	{
		auto a = (Uint8)((color >> 24) & 0xff);
		if (a < 255)
			SDL_SetRenderDrawBlendMode(fRenderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(fRenderer, (Uint8)((color >> 16) & 0xff), (Uint8)((color >> 8) & 0xff), (Uint8)((color >> 0) & 0xff), a);
		SDL_RenderDrawPoint(fRenderer, x, y);
	}
	void drawEllipseFilled(int x, int y, int rx, int ry, uint32_t color)
	{
		filledEllipseRGBA(fRenderer, (Sint16)x, (Sint16)y, (Sint16)rx, (Sint16)ry, (Uint8)((color >> 16) & 0xff), (Uint8)((color >> 8) & 0xff), (Uint8)((color >> 0) & 0xff), (Uint8)((color >> 24) & 0xff));
	}
	void drawEllipseOutline(int x, int y, int rx, int ry, uint32_t color)
	{
		aaellipseRGBA(fRenderer, (Sint16)x, (Sint16)y, (Sint16)rx, (Sint16)ry, (Uint8)((color >> 16) & 0xff), (Uint8)((color >> 8) & 0xff), (Uint8)((color >> 0) & 0xff), (Uint8)((color >> 24) & 0xff));
	}
	void drawTexture(SDL_Texture* aTexture, SDL_Rect* aRect)
	{
		SDL_RenderCopy(fRenderer, aTexture, nullptr, aRect);
	}
	void drawTextureScale(SDL_Texture* aTexture, SDL_Rect* aRect)
	{
		SDL_RenderCopy(fRenderer, aTexture, nullptr, aRect); // sdl2 does scaling on texture already
	}
	void drawTexture(SDL_Surface* aSurface, SDL_Rect* aRect)
	{
		auto texture = SDL_CreateTextureFromSurface(fRenderer, aSurface);
		SDL_RenderCopy(fRenderer, texture, nullptr, aRect);
		SDL_DestroyTexture(texture);
	}
	void drawTextureRotated(SDL_Texture* aTexture, SDL_Rect* aRect, double aAngle, SDL_Point* aCenter)
	{
		SDL_RenderCopyEx(fRenderer, aTexture, nullptr, aRect, aAngle, aCenter, SDL_FLIP_NONE);
	}
	/*
	void drawTextureA(SDL_Texture* aTexture, SDL_Rect* aRect, uint8_t aAlpha)
	{
		SDL_SetTextureAlphaMod(aTexture, aAlpha);
		SDL_RenderCopy(fRenderer, aTexture, nullptr, aRect);
	}
	*/
	SDL_Texture* createTargetTexture(const fs::path& aPath, SDL_Texture* aRefTexture)
	{
		Uint32 format;
		int access, w, h;
		SDL_QueryTexture(aRefTexture, &format, &access, &w, &h);
		auto texture = SDL_CreateTexture(fRenderer, format, access | SDL_TEXTUREACCESS_TARGET, w, h);
		fTextures.insert({aPath.string(), texture});
		return texture;
	}
	void drawTextureOnTexture(SDL_Texture* aSrcTexture, SDL_Texture* aDstTexture)
	{
		auto res1 = SDL_SetRenderTarget(fRenderer, aDstTexture);
		if (res1)
		{
			std::cout << "## set render target error: " << res1 << ", " << SDL_GetError() << std::endl;
		}
		SDL_RenderCopy(fRenderer, aSrcTexture, nullptr, nullptr);
		SDL_SetRenderTarget(fRenderer, nullptr);
	}
public:
	SDL_Color colorToSDLColor(uint32_t aColor)
	{
		// aColor is 0xAABBGGRR
		// SDL_Color is rgba
		return SDL_Color{ (Uint8)((aColor >> 0) & 0xFF),(Uint8)((aColor >> 8) & 0xFF),(Uint8)((aColor >> 16) & 0xFF),(Uint8)((aColor >> 24) & 0xFF) };
	}
	uint32_t rgbaToColor(Uint8 r = 255, Uint8 g = 255, Uint8 b = 255, Uint8 a = SDL_ALPHA_OPAQUE)
	{
		return (uint32_t(a) << 24) | (uint32_t(b) << 16) | (uint32_t(g) << 8) | uint32_t(r);
	}
	uint32_t alphaColor(uint32_t aColor, double aAlpha)
	{
		auto color = colorToSDLColor(aColor);
		if (aAlpha >= 1)
			color.a = Uint8(double(color.a) * aAlpha);
		return rgbaToColor(color.r, color.g, color.b, color.a);
	}
	SDL_Texture* loadTexture(const fs::path& aPath, bool aAlpha = true, bool aOptimized = true, bool aStore = true)
	{
		auto pathStr = aPath.string();
		auto it = fTextures.find(pathStr);
		if (it == fTextures.end())
		{
			auto texture = IMG_LoadTexture(fRenderer, pathStr.c_str());
			if (aStore)
				fTextures.insert({ pathStr, texture});
			return texture;
		}
		else
			return it->second;
	}
	SDL_Surface* loadSurface(const fs::path& aPath, bool aAlpha = true, bool aOptimized = true, bool aStore = true)
	{
		auto pathStr = aPath.string();
		auto it = fSurfaces.find(pathStr);
		if (it == fSurfaces.end())
		{
			auto surface = IMG_Load(pathStr.c_str());
			if (aStore)
				fSurfaces.insert({ pathStr, surface });
			// we ignore the aAlpha and aOptimized for SDL2
			return surface;
		}
		else
			return it->second;
	}
	void freeSurface(SDL_Surface* aSurface)
	{
		if (aSurface)
			SDL_FreeSurface(aSurface);
	}
	void freeTexture(SDL_Texture* aTexture)
	{
		if (aTexture)
			SDL_DestroyTexture(aTexture);
	}
	SDL_Rect surfaceRect(SDL_Surface* aSurface)
	{
		return SDL_Rect{ 0,0,aSurface->w,aSurface->h };
	}
	SDL_Rect textureRect(SDL_Texture* aTexture)
	{
		SDL_Rect res{ 0,0,0,0 };
		SDL_QueryTexture(aTexture, nullptr, nullptr, &res.w, &res.h);
		return res;
	}
public:
	// polygon cached for SDL2_gfx MT functions
	int* gfxPrimitivesPolyIntsCache = nullptr;
	int gfxPrimitivesPolyAllocatedCache = 0;
private:
	SDL_Surface* fSurface = nullptr;
	SDL_Renderer* fRenderer = nullptr;
	SDL_Window* fWindow = nullptr;
	std::unordered_map<std::string, std::shared_ptr<std::unordered_map<int, TTF_Font*>>> fFonts;
	std::unordered_map<std::string, SDL_Texture*> fTextures;
	std::unordered_map<std::string, SDL_Surface*> fSurfaces;
	fs::path fFontPath;
};
