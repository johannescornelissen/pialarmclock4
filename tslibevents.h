#pragma once

#include <functional>

#include <tslib.h>

#ifdef SDL1
#include "sdl_renderer1.h"
#else
#include "sdl_renderer2.h"
#endif

class TSLibEvents
{
public:
    TSLibEvents(char* aDeviceName = nullptr)
	{
        ts = ts_setup(aDeviceName, 0);
	}
	~TSLibEvents()
	{
        active = false;
        if (ts)
        {
            ts_close(ts);
            ts = nullptr;
        }
	}
public:
    int TouchDNEventID = SDL_USEREVENT + 2;
    int TouchUPEventID = SDL_USEREVENT + 3;
    int TouchMVEventID = SDL_USEREVENT + 4;
    bool active = true;
public:
    void loop()
    {
        ts_sample_mt samp;
        memset(&samp, 0, sizeof(samp));
        ts_sample_mt* samp_mt;

        samp_mt = &samp;
        bool touchIsDown = false;
        while (active)
        {
            int ret = ts_read_mt(ts, &samp_mt, 1, 1);
            if (ret >= 0)
            {
                for (int j = 0; j < ret; j++)
                {
                    if (samp_mt[j].valid)
                    {
                        //std::cout << "touch " << samp_mt[j].x << "," << samp_mt[j].y << ": " << samp_mt[j].pressure << std::endl;
                        if (touchIsDown != (samp_mt[j].pressure >= 128))
                        {
                            touchIsDown = !touchIsDown;
                            if (touchIsDown)
                            {
                                std::cout << "touch DN " << samp_mt[j].x << "," << samp_mt[j].y << ": " << samp_mt[j].pressure << std::endl;
                                SDL_Event sdl_event;
                                SDL_memset(&sdl_event, 0, sizeof(sdl_event));
                                sdl_event.type = (Uint8)TouchDNEventID;
                                sdl_event.button.x = (Uint16)samp_mt[j].x;
                                sdl_event.button.y = (Uint16)samp_mt[j].y;
                                sdl_event.button.state = SDL_PRESSED;
                                SDL_PushEvent(&sdl_event);
                            }
                            else
                            {
                                std::cout << "touch UP " << samp_mt[j].x << "," << samp_mt[j].y << ": " << samp_mt[j].pressure << std::endl;
                                SDL_Event sdl_event;
                                SDL_memset(&sdl_event, 0, sizeof(sdl_event));
                                sdl_event.type = (Uint8)TouchUPEventID;
                                sdl_event.button.x = (Uint16)samp_mt[j].x;
                                sdl_event.button.y = (Uint16)samp_mt[j].y;
                                sdl_event.button.state = SDL_RELEASED;
                                SDL_PushEvent(&sdl_event);
                            }
                        }
                        else
                        {
                            std::cout << "touch MV " << samp_mt[j].x << "," << samp_mt[j].y << ": " << samp_mt[j].pressure << std::endl;
                            SDL_Event sdl_event;
                            SDL_memset(&sdl_event, 0, sizeof(sdl_event));
                            sdl_event.type = (Uint8)TouchMVEventID;
                            sdl_event.motion.x = (Uint16)samp_mt[j].x;
                            sdl_event.motion.y = (Uint16)samp_mt[j].y;
                            sdl_event.motion.state = touchIsDown ? SDL_PRESSED : SDL_RELEASED;
                            SDL_PushEvent(&sdl_event);
                        }
                    }
                    else
                        std::cout << "invalid touch" << std::endl;
                }
            }
            else
                std::cout << "no touch" << std::endl;
        }
    }
private:
    tsdev* ts;
};
