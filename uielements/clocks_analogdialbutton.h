#pragma once

#include "clocks.h"
#include "datetimeeditors.h"


class ClockAnalogDialButton : public Clock
{
public:
	ClockAnalogDialButton(fs::path aImagePath, std::vector<std::string> aImageNames, SDL_Rect aPosition)
		: Clock(aImagePath, aPosition, -1)
	{
		touchable = true; // override default clock behaviour because this is a button
		fImageNames = aImageNames;
		fTimerInterval = 10000;
	}
public:
	int activeImage = 0;
	std::shared_ptr<DateTimeEditor> dateTimeEditor;
public:
	virtual std::string elementName() override { return "ClockAnalogDialButton"; }
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			auto rect = relativeDisplayPosition(x, y);

			Uint32 color = 0x44FFFFFF;

			// start with black background, which is a button
			auto texture = getOrLoadTexture(aConfig.renderer, activeImage);
			aConfig.renderer->drawTextureScale(texture, &rect);

			rect = rectInflate(rect, -8, -8);

			double angle;
			auto center = centerOfRect(rect);

			// dial minutes
			for (int minute = 0; minute < 60; minute++)
			{
				calculateArmAngles(0, minute, 0, nullptr, &angle, nullptr);
				auto p1 = time_location(center, angle, int(std::round(0.425 * rect.h)));
				auto p2 = time_location(center, angle, int(std::round(0.450 * rect.h)));
				aConfig.renderer->drawAALine(p1.x, p1.y, p2.x, p2.y, color);
			}
			// dial hours
			for (int hour = 1; hour <= 12; hour++)
			{
				for (int i = -4; i <= 4; i++)
				{
					if (i != 0)
					{
						calculateArmAngles(hour, 0, i * 10, &angle, nullptr, nullptr);
						auto p1 = time_location(center, angle, int(std::round(0.40 * rect.h)));
						auto p2 = time_location(center, angle, int(std::round(0.45 * rect.h)));
						aConfig.renderer->drawAALine(p1.x, p1.y, p2.x, p2.y, color);
					}
				}
				calculateArmAngles(hour, 0, 0, &angle, nullptr, nullptr);
			}

			// arms
			int hour, minutes, seconds;
			LocalDateTime::timeParts(aConfig.currentTime, &hour, &minutes, &seconds);
			double hourArmAngle, minutesArmAngle;
			calculateArmAngles(hour, minutes, seconds, &hourArmAngle, &minutesArmAngle, nullptr);
			{
				// hour arm
				auto p1 = time_location(center, hourArmAngle - 5.0 / 8.0 * pi, int(std::round(0.01 * rect.h)));
				auto p2 = time_location(center, hourArmAngle, int(std::round(0.27 * rect.h)));
				auto p3 = time_location(center, hourArmAngle + 5.0 / 8.0 * pi, int(std::round(0.01 * rect.h)));
				aConfig.renderer->drawAAFilledTrigon(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, color);
			}
			{
				// minutes arm
				auto p1 = time_location(center, minutesArmAngle - 6.0 / 8.0 * pi, int(std::round(0.015 * rect.h)));
				auto p2 = time_location(center, minutesArmAngle, int(std::round(0.43 * rect.h)));
				auto p3 = time_location(center, minutesArmAngle + 6.0 / 8.0 * pi, int(std::round(0.015 * rect.h)));
				aConfig.renderer->drawAAFilledTrigon(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, color);
			}

			// make sure we are active
			activate(aConfig, false);

			return true;
		}
		else
			return false;
	}
	virtual int onTouchDown(TouchState& aTouchState, Config& aConfig) override
	{
		// start full time editor
		int hour, minutes, seconds;
		LocalDateTime::timeParts(aConfig.currentTime, &hour, &minutes, &seconds);
		// todo: create real time editor, year/month/day hour/minutes/seconds, resolution 1 sec, live update of shown date/time
		dateTimeEditor = std::make_shared<DateTimeEditor>(hour, minutes, parent->position);
		dateTimeEditor->onEditorDone = [this](DateTimeEditor* aDateTimeEditor, Config& aConfig) -> int
		{
			// todo: handle setting date/time

			// disable editor
			parent->removeChild(aDateTimeEditor);
			return RERENDER_ALL;
		};
		// add editor
		std::scoped_lock guard(aConfig.lock);
		parent->addChild(dateTimeEditor);
		return RERENDER_ALL;
	}
protected:
	SDL_Texture* getOrLoadTexture(SDLRenderer* aRenderer, int aIndex)
	{
		if (aIndex >= int(fImageTextures.size()) || !fImageTextures[aIndex])
		{
			while (aIndex >= int(fImageTextures.size()))
				fImageTextures.push_back(nullptr);
			fImageTextures[aIndex] = aRenderer->loadTexture(fImagePath / fImageNames[aIndex]);
		}
		return fImageTextures[aIndex];
	}
protected:
	std::vector<std::string> fImageNames;
	std::vector<SDL_Texture*> fImageTextures;
};
