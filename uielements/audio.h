// button that shows the current volume (minature) and toggles an expanded view with the current alarm volume and music name selector

#pragma once

#include "../sdl_eventloop.h"

class VerticalVolumeBar : public Element
{
public:
	VerticalVolumeBar(double aVolume, SDL_Rect aPosition) 
		: Element(aPosition, false, true)
	{
		fVolume = aVolume;
	}
public:
	void updateVolume(double aVolume, Config& aConfig)
	{
		std::scoped_lock guard(aConfig.lock);
		fVolume = aVolume;
	}
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			auto rect = relativeDisplayPosition(x, y);
			aConfig.renderer->drawRectFilled(rect, 0x20ffffff);
			for (int y = 0; y < rect.h; y++)
			{
				auto iy = rect.h - y - 1;
				auto v = double(iy) / rect.h;
				auto p = y % 4;
				if (v <= fVolume && (p == 0 || p == 1))
					aConfig.renderer->drawHLineColor(rect.x, rect.x + rect.w, rect.y + y, 0x80ffffff);
			}
			return true;
		}
		else
			return false;
	}
private:
	double fVolume;
};

class VolumeSlider : public Element
{
public:
	VolumeSlider(fs::path aImagePath, double aInitialValue, SDL_Rect aPosition) 
		: Element(aPosition, true, true)
	{
		fImagePath = aImagePath;
		fValue = aInitialValue;
	}
public:
	virtual std::string elementName() override { return "VolumeSlider"; }
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			if (!aConfig.bigScreen())
				fSliderWidth = 24;
			auto rect = relativeDisplayPosition(x, y);
			auto triangleRect = rect;
			triangleRect.x += fSliderWidth / 2;
			triangleRect.w -= fSliderWidth;
			triangleRect.y += fSliderWidth / 3;
			triangleRect.h -= 2 * fSliderWidth / 3;

			aConfig.renderer->drawAALine(
				triangleRect.x, triangleRect.y + triangleRect.h,
				triangleRect.x + triangleRect.w, triangleRect.y,
				0x30ffffff);
			aConfig.renderer->drawAALine(
				triangleRect.x, triangleRect.y + triangleRect.h,
				triangleRect.x + triangleRect.w, triangleRect.y,
				0x30ffffff);
			aConfig.renderer->drawFilledTrigon(
				triangleRect.x, triangleRect.y + triangleRect.h,
				triangleRect.x + triangleRect.w, triangleRect.y,
				triangleRect.x + triangleRect.w, triangleRect.y + triangleRect.h,
				0x40ffffff);

			// slider
			if (!fSliderTexture)
				fSliderTexture = aConfig.renderer->loadTexture(fImagePath / "button-48.png");
			fSliderRect = SDL_Rect{ rect.x + int((rect.w - fSliderWidth) * fValue),rect.y,fSliderWidth,rect.h };
			aConfig.renderer->drawTextureScale(fSliderTexture, &fSliderRect);
			return true;
		}
		else
			return false;
	}
	std::function<int(Config& aConfig, double aValue)> onSlidingStart;
	std::function<int(Config& aConfig, double aValue)> onSliding;
	std::function<int(Config& aConfig, double aValue)> onSlidingEnd;
	int virtual onTouchDown(TouchState& aTouchState, Config& aConfig) 
	{ 
		fSliding = aTouchState.touchDownInRect(fSliderRect);
		if (fSliding)
		{
			fStartSlidingValue = fValue;
			fStartSlidingRect = fSliderRect;
		}
		if (fSliding && onSlidingStart)
			return onSlidingStart(aConfig, fValue);
		else
			return RERENDER_NONE; 
	}
	int virtual onTouchUp(TouchState& aTouchState, Config& aConfig) 
	{ 
		if (fSliding && onSlidingEnd)
		{
			fSliding = false;
			return onSlidingEnd(aConfig, fValue);
		}
		else
		{
			fSliding = false;
			return RERENDER_NONE;
		}
	}
	int virtual onTouchUpLong(TouchState& aTouchState, Config& aConfig) 
	{ 
		if (fSliding && onSlidingEnd)
		{
			fSliding = false;
			return onSlidingEnd(aConfig, fValue);
		}
		else
		{
			fSliding = false;
			return RERENDER_NONE;
		}
	}
	int virtual onTouchMoveStart(TouchState& aTouchState, Config& aConfig) 
	{ 
		if (fSliding)
		{
			auto rect = relativeDisplayPosition(relativeXY());
			fValue = (double(fStartSlidingRect.x) + aTouchState.currentX - aTouchState.touchDownX - rect.x) / (double(rect.w) - fSliderWidth);
			fValue = std::max(std::min(fValue, 1.0), 0.0);
		}
		if (fSliding && onSliding)
			return onSliding(aConfig, fValue);
		else
			return RERENDER_NONE;
	}
	int virtual onTouchMove(TouchState& aTouchState, Config& aConfig) 
	{ 
		if (fSliding)
		{
			auto rect = relativeDisplayPosition(relativeXY());
			fValue = double(double(fStartSlidingRect.x) + aTouchState.currentX - aTouchState.touchDownX - rect.x) / (double(rect.w) - fSliderWidth);
			fValue = std::max(std::min(fValue, 1.0), 0.0);
		}
		if (fSliding && onSliding)
			return onSliding(aConfig, fValue);
		else
			return RERENDER_NONE; 
	}
	int virtual onTouchMoveEnd(TouchState& aTouchState, Config& aConfig) 
	{ 
		if (fSliding && onSlidingEnd)
		{
			fSliding = false;
			return onSlidingEnd(aConfig, fValue);
		}
		else
		{
			fSliding = false;
			return RERENDER_NONE;
		}
	}
protected:
	fs::path fImagePath;
	SDL_Texture* fSliderTexture = nullptr;
	double fValue = 0.5;
	int fSliderWidth = 32;
	bool fSliding = false;
	double fStartSlidingValue = 0.5;
	SDL_Rect fStartSlidingRect = { 0,0,0,0 };
	SDL_Rect fSliderRect{ 0,0,0,0 };
};

class AudioTitleScroller : public Element
{
public:
	AudioTitleScroller(std::vector<std::string> aEntries, int aSelectedEntry, int aBeforeEntryCount, int aAfterEntryCount, const SDL_Rect& aPosition)
		: Element(aPosition, true, true)
	{
		for (auto& entry : aEntries)
		{
			fEntries.push_back(fs::path(entry).replace_extension().string());
		}
		
		selectedEntry = aSelectedEntry;
		fBeforeEntryCount = aBeforeEntryCount;
		fAfterEntryCount = aAfterEntryCount;
	}
public:
	virtual std::string elementName() override { return "AutoTitleScroller"; }
	int selectedEntry = -1;
	std::function<int(AudioTitleScroller* aAudioTitleScroller, Config& aConfig)> onSelectionChanged;
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			auto rect = relativeDisplayPosition(x, y);

			// background selected text
			aConfig.renderer->drawRectFilled(rect, 0x40ffffff);
			// font
			if (!fFont)
				fFont = aConfig.renderer->getFont(int(position.h * 0.9), "seguisb.ttf");
			int sel = selectedEntry;
			float offset = float(fSligingOffset);
			while (offset < -0.5f * float(position.h))
			{
				sel++;
				offset += float(position.h);
			}
			while (offset > 0.5f * float(position.h))
			{
				sel--;
				offset -= float(position.h);
			}
			// selected text
			auto selectedRect = rect;
			selectedRect.y += int(offset);
			// show selected
			showText(aConfig.renderer, selectedRect, sel, 128);
			// show on top
			for (int i = 1; i <= fBeforeEntryCount; i++)
			{
				auto textRect = rect;
				textRect.y += int(offset);
				textRect.y -= i * rect.h;
				showText(aConfig.renderer, textRect, sel - i, uint8_t(128 - i * 20));
			}
			// show below
			for (int i = 1; i <= fAfterEntryCount; i++)
			{
				auto textRect = rect;
				textRect.y += int(offset);
				textRect.y += i * rect.h;
				showText(aConfig.renderer, textRect, sel + i, uint8_t(128 - i * 20));
			}
			return true;
		}
		else
			return false;
	}
	virtual bool inElement(int x, int y) override
	{
		return (position.x <= x) && (x - position.x <= position.w) && (position.y - position.h * fBeforeEntryCount <= y) && (y - position.y <= (fAfterEntryCount + 1) * position.h);
	}

	int virtual onTouchMoveStart(TouchState& aTouchState, Config& aConfig)
	{
		fSligingOffset = aTouchState.currentY - aTouchState.touchDownY;
		return RERENDER_ALL;
	}
	int virtual onTouchMove(TouchState& aTouchState, Config& aConfig)
	{
		fSligingOffset = aTouchState.currentY - aTouchState.touchDownY;
		return RERENDER_ALL;
	}
	int virtual onTouchMoveEnd(TouchState& aTouchState, Config& aConfig)
	{
		// snap to selected item
		auto delta = (fSligingOffset) / position.h;
		selectedEntry -= int(std::round(delta));
		if (selectedEntry < 0)
			selectedEntry = 0;
		else if (selectedEntry >= int(fEntries.size()))
			selectedEntry = int(fEntries.size()) - 1;
		fSligingOffset = 0;
		if (onSelectionChanged)
			return onSelectionChanged(this, aConfig);
		else
			return RERENDER_ALL;
	}
protected:
	void showText(SDLRenderer* aRenderer, const SDL_Rect& aRect, int aEntryIndex, uint8_t aAlpha)
	{
		if (0 <= aEntryIndex && aEntryIndex < int(fEntries.size()))
		{
			auto& entryText = fEntries[aEntryIndex];
			int textWidth = 0;
			int textHeight = 0;
			TTF_SizeUTF8(fFont, entryText.c_str(), &textWidth, &textHeight);
			if (aRect.w < textWidth)
			{
				// adjust text to fit
				entryText.resize(entryText.size() - 1);
				entryText += "...";
				TTF_SizeUTF8(fFont, entryText.c_str(), &textWidth, &textHeight);
				while (entryText.size() >= 3 && aRect.w < textWidth)
				{
					entryText.resize(entryText.size() - 4);
					entryText += "...";
					TTF_SizeUTF8(fFont, entryText.c_str(), &textWidth, &textHeight);
				}
				// store adjusted entry
				fEntries[aEntryIndex] = entryText;
			}
			SDL_Rect dstTextRect{ aRect.x,aRect.y + aRect.h - textHeight,textWidth,textHeight };
			aRenderer->drawText(entryText, fFont, aRenderer->rgbaToColor(0xff, 0xff, 0xff, aAlpha), &dstTextRect);

		}
	}
protected:
	std::vector<std::string> fEntries;
	int fBeforeEntryCount;
	int fAfterEntryCount;
	int fSligingOffset = 0;
	TTF_Font* fFont = nullptr;
};


class AudioControlButton : public MultiImage
{
public:
	AudioControlButton(fs::path aImagePath, SDL_Rect aPosition)
		: MultiImage(aImagePath, { "button-48.png", "volume190.png" }, aPosition)
	{
		activeImage = 0;
	}
public:
	virtual std::string elementName() override { return "AutoControlButton"; }
	int virtual onTouchUp(TouchState& aTouchState, Config& aConfig) override
	{
		std::scoped_lock guard(aConfig.lock);
		if (fVolumeSlider && fVolumeSlider->visible)
		{
			fVolumeSlider->visible = false;
			fVolumeText->visible = false;
			fPlayButton->visible = false;
			fAudioTitleScroller->visible = false;
			fAudioTitleSmall->visible = true;
		}
		else
		{
			if (!fVolumeSlider)
			{
				auto rect = position;
				auto parentRect = parent->position;
				
				SDL_Rect volumeTextRect{ rect.x + rect.w + 6,rect.y,rect.w,rect.h / 2 };
				fVolumeText = std::make_shared<Text>(std::to_string(int(aConfig.audioVolume * 100)), "arlrdbd.ttf", rect.h / 2, volumeTextRect);
				parent->addChild(fVolumeText);
				
				SDL_Rect volumeSliderRect{ volumeTextRect.x + 20,rect.y,parentRect.w / 3,rect.h };
				fVolumeSlider = std::make_shared<VolumeSlider>(fImagePath, aConfig.audioVolume, volumeSliderRect);
				fVolumeSlider->onSliding = [this](Config& aConfig, double aValue) -> int
				{
					aConfig.audioSetVolume(aValue);
					fVolumeText->updateText(std::to_string(int(aConfig.audioVolume * 100)), aConfig);
					return RERENDER_ALL; 
				};
				parent->addChild(fVolumeSlider);

				SDL_Rect playRect{ parentRect.x + parentRect.w - (rect.x - parentRect.x) - rect.w,rect.y,rect.w,rect.h };
				fPlayButton = std::make_shared<Button>(fImagePath, std::vector<std::string>{ "button-48.png", "play160.png", "stop160.png" }, 1, "", playRect);
				fPlayButton->onClick = [this](Config& aConfig) -> int
				{
					if (this->fPlayButton->activeIconImage == 1)
					{ 
						this->fPlayButton->activeIconImage = 2;
						// play
						aConfig.audioPlay(fImagePath / ".." / "sounds", 1000, 1);
					}
					else
					{
						this->fPlayButton->activeIconImage = 1;
						// stop
						aConfig.audioStop(1000);
					}
					return RERENDER_ALL;
				};
				parent->addChild(fPlayButton);

				std::vector<std::string> entries;
				for (const auto& entry : fs::directory_iterator(fImagePath / ".." / "sounds"))
					entries.push_back(entry.path().filename().u8string());
				std::sort(entries.begin(), entries.end());
				auto& selectedName = aConfig.audioAlarmSoundFileName;
				int selectedEntry = int(entries.size()) - 1;
				while (selectedEntry >= 0 && selectedName.compare(entries[selectedEntry]) != 0)
					selectedEntry--;
				if (selectedEntry < 0)
					selectedEntry = 0;
				SDL_Rect titleScrollerRect{ volumeSliderRect.x + volumeSliderRect.w + 12,0,playRect.x - (volumeSliderRect.x + volumeSliderRect.w + 24),20 };
				titleScrollerRect = Element::rectCenterY(rect, titleScrollerRect);
				fAudioTitleScroller = std::make_shared<AudioTitleScroller>(entries, selectedEntry, 3, 2, titleScrollerRect);
				fAudioTitleScroller->onSelectionChanged = [this, entries](AudioTitleScroller* aAudioTitleScroller, Config& aConfig) -> int
				{
					{
						std::scoped_lock guard(aConfig.lock);
						if (0 <= aAudioTitleScroller->selectedEntry && aAudioTitleScroller->selectedEntry < int(entries.size()))
						{
							aConfig.audioAlarmSoundFileName = entries[aAudioTitleScroller->selectedEntry];
							aConfig.save();
							if (aConfig.audioIsPlaying())
								aConfig.audioPlay(fImagePath / ".." / "sounds", 500);
						}
					}
					fAudioTitleSmall->updateText(fs::path(aConfig.audioAlarmSoundFileName).replace_extension().string(), aConfig);
					return RERENDER_ALL;
				};
				parent->addChild(fAudioTitleScroller);

				auto audioTitleSmall = rect;
				audioTitleSmall.x += audioTitleSmall.w + 12;
				audioTitleSmall.w = int(parent->position.w - 2 * audioTitleSmall.w);
				audioTitleSmall.y += audioTitleSmall.h - 12;
				audioTitleSmall.h = 12;
				fAudioTitleSmall = std::make_shared<Text>(fs::path(aConfig.audioAlarmSoundFileName).replace_extension().string(), "seguisb.ttf", 12, audioTitleSmall);
				fAudioTitleSmall->visible = false;
				parent->addChild(fAudioTitleSmall);
				
			}
			else
			{
				fVolumeSlider->visible = true;
				fVolumeText->visible = true;
				fPlayButton->visible = true;
				fAudioTitleScroller->visible = true;
				fAudioTitleSmall->visible = false;
			}
		}
		return RERENDER_ALL;
	}

	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			MultiImage::renderThis(aConfig, x, y);

			auto rect = relativeDisplayPosition(x, y);

			auto iconRect = Element::rectOffset(rect, int(-rect.w * 0.1), 0);
			iconRect = Element::rectInflate(iconRect, int(-iconRect.w * 0.4), int(-iconRect.h * 0.4));
			auto iconTexture = getOrLoadTexture(aConfig.renderer, 1);
			aConfig.renderer->drawTextureScale(iconTexture, &iconRect);
			
			auto volumeBarRect = Element::rectOffset(rect, int(rect.w * 0.3), 0);
			volumeBarRect = Element::rectInflate(volumeBarRect, int(-volumeBarRect.w * 0.9), int(-volumeBarRect.h * 0.2));
			aConfig.renderer->drawRectFilled(volumeBarRect, 0x20ffffff);
			for (int y = 0; y < volumeBarRect.h; y++)
			{
				auto iy = volumeBarRect.h - y - 1;
				auto v = double(iy) / volumeBarRect.h;
				auto p = y % 4;
				if (v <= aConfig.audioVolume && (p == 0 || p == 1))
					aConfig.renderer->drawHLineColor(volumeBarRect.x, volumeBarRect.x + volumeBarRect.w, volumeBarRect.y + y, 0x60ffffff);
			}
			return true;
		}
		else
			return false;
	}
protected:
	std::shared_ptr<Text> fAudioTitleSmall;
	std::shared_ptr<VolumeSlider> fVolumeSlider;
	std::shared_ptr<Text> fVolumeText;
	std::shared_ptr<Button> fPlayButton;
	std::shared_ptr<AudioTitleScroller> fAudioTitleScroller;
};
