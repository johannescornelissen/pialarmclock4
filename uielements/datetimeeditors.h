// modal dialog to select a (alarm) time

#pragma once

#pragma once

#include <sstream>
#include <iomanip>

#include "../sdl_eventloop.h"
#include "images.h"


#define colorDateTimeEditorFlipBackground 0xE0303030 
#define colorDateTimeEditorFlipSplitLine 0xFF202020
#define colorDateTimeEditorFlipText 0xE0909090


class DateTimeEditor : public Element
{
public:
	DateTimeEditor(int aHour, int aMinutes, SDL_Rect aPosition)
		: Element(aPosition, true, true)
	{
		hour = aHour;
		minutes = aMinutes;
	}
public:
	virtual std::string elementName() override { return "DateTimeEditor"; }
	int hour = 0;
	int minutes = 0;
	std::function<int(DateTimeEditor* aDateTimeEditor, Config& aConfig)> onEditorDone;
public:
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, true))
		{
			auto rect = relativeDisplayPosition(x, y);

			// gray down rest of screen, editor works in modal mode
			aConfig.renderer->drawRectFilled(rect, aConfig.renderer->alphaColor(0xFF000000, 0.8));

			if (!fFont)
				fFont = aConfig.renderer->getFont(int(position.w / 3.5), "seguisb.ttf");

			auto rectHour = rect;
			rectHour.w = int(rectHour.w / 2.5);
			rectHour.h = int(rectHour.h / 1.6);
			rectHour.x += int(rectHour.w / 5.0);
			rectHour = rectCenterY(rect, rectHour);

			auto rectMinutes = rectHour;
			rectMinutes.x = rect.x + rect.w - rectMinutes.w - int(rectMinutes.w / 5.0);

			int line_width = std::max(rect.h / 70, 1);
			int rr_radius = std::max(rect.h / 10, 2);

			aConfig.renderer->drawRoundedRectFilled(rectHour, rr_radius, colorDateTimeEditorFlipBackground);
			aConfig.renderer->drawRoundedRectFilled(rectMinutes, rr_radius, colorDateTimeEditorFlipBackground);

			renderFlipDigits(aConfig.renderer, rectHour, std::to_string(hour), 0.06);

			std::stringstream ss;
			ss << std::setw(2) << std::setfill('0') << minutes;
			renderFlipDigits(aConfig.renderer, rectMinutes, ss.str(), 0.06);

			renderSplitLine(aConfig.renderer, rectHour, line_width);
			renderSplitLine(aConfig.renderer, rectMinutes, line_width);

			// define touch arreas

			fHourPlus = rectHour;
			fHourPlus.h = fHourPlus.h / 2;
			fHourMin = fHourPlus;
			fHourMin.y += fHourMin.h;

			fMinutesPlus = rectMinutes;
			fMinutesPlus.h = fMinutesPlus.h / 2;
			fMinutesMin = fMinutesPlus;
			fMinutesMin.y += fMinutesMin.h;

			return true;
		}
		else
			return false;
	}
	void renderFlipDigits(SDLRenderer* aRenderer, const SDL_Rect& aRect, const std::string& aText, double aVerticalAdjustFactor)
	{
		SDL_Rect renderQuad{ aRect.x,aRect.y,0,0 };
		int w, h;
		TTF_SizeText(fFont, aText.c_str(), &w, &h);
		renderQuad.w = w;
		renderQuad.h = h;
		renderQuad = rectCenter(aRect, renderQuad);
		renderQuad = rectOffset(renderQuad, 0, int(-renderQuad.h * aVerticalAdjustFactor));// adjust text
		aRenderer->drawText(aText, fFont, colorDateTimeEditorFlipText, &renderQuad);
	}
	void renderSplitLine(SDLRenderer* aRenderer, const SDL_Rect& aRect, int aLineWidth)
	{
		// render horizontal line as cut-out
		int y1 = aRect.y + aRect.h / 2 - aLineWidth / 2;
		int y2 = y1 + aLineWidth;
		for (int y = y1; y < y2; y++)
			aRenderer->drawHLineColor(aRect.x, aRect.x + aRect.w, y, colorDateTimeEditorFlipSplitLine);
	}
	int virtual onTouchUp(TouchState& aTouchState, Config& aConfig)
	{
		if (aTouchState.touchDownInRect(fHourPlus))
		{
			hour++;
			hour %= 24;
			return RERENDER_ALL;
		}
		else if (aTouchState.touchDownInRect(fHourMin))
		{
			hour--;
			if (hour < 0)
				hour += 24;
			return RERENDER_ALL;
		}
		else if (aTouchState.touchDownInRect(fMinutesPlus))
		{
			minutes += 5;
			minutes %= 60;
			return RERENDER_ALL;
		}
		else if (aTouchState.touchDownInRect(fMinutesMin))
		{
			minutes -= 5;
			if (minutes < 0)
				minutes += 60;
			return RERENDER_ALL;
		}
		else
		{
			if (onEditorDone)
				return onEditorDone(this, aConfig);
			else
				return RERENDER_NONE;
		}
	}
	int virtual onTouchUpLong(TouchState& aTouchState, Config& aConfig)
	{
		return RERENDER_NONE;
	}
protected:
	TTF_Font* fFont = nullptr;
	SDL_Rect fYearPlus{ 0,0,0,0 };
	SDL_Rect fYearMin{ 0,0,0,0 };
	SDL_Rect fMonthPlus{ 0,0,0,0 };
	SDL_Rect fMonthMin{ 0,0,0,0 };
	SDL_Rect fDayPlus{ 0,0,0,0 };
	SDL_Rect fDayMin{ 0,0,0,0 };
	SDL_Rect fHourPlus{ 0,0,0,0 };
	SDL_Rect fHourMin{ 0,0,0,0 };
	SDL_Rect fMinutesPlus{ 0,0,0,0 };
	SDL_Rect fMinutesMin{ 0,0,0,0 };
};
