#pragma once

#include <sstream>

#include "../sdl_eventloop.h"
#include "slidingpanels.h"

struct ClockTimerInfo
{
	Element* clock = nullptr;
	Config* config = nullptr;
};

inline Uint32 timerUpdateCLock(Uint32 aInterval, void* aParam)
{
	auto clockTimerInfo = (ClockTimerInfo*)aParam;
	std::scoped_lock guard(clockTimerInfo->config->lock);
	// manually get the time
	clockTimerInfo->config->currentTime = LocalDateTime::now();
	sendRerenderEvent();
	return aInterval;
}


class Clock : public Element
{
public:
	static constexpr double pi = 3.14159265358979323846;
public:
	Clock(fs::path aImagePath, SDL_Rect aPosition, int aClockType) 
		: Element(aPosition, false, true)
	{
		fImagePath = aImagePath;
		fClockType = aClockType;
	}
	virtual ~Clock() override
	{
		timerStop();
	}
public:
	virtual int clockType() { return fClockType; }
	virtual void onExit(Config& aConfig) override
	{
		std::scoped_lock guard(aConfig.lock);
		timerStop();
	}
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			// just draw red rectangle to show unconfigured clock is selected
			auto rect = relativeDisplayPosition(x, y);
			if (fDrawBackground)
				aConfig.renderer->drawRectFilled(rect, 0xff000000);
			return true;
		}
		else
			return false;
	}
	virtual void activate(Config& aConfig, bool aFull) override
	{
		if (!isTimerRunning())
			timerStart(fTimerInterval, aConfig);
		Element::activate(aConfig, aFull);
	}
	virtual void deactivate(Config& aConfig) override
	{
		timerStop();
		Element::deactivate(aConfig);
	}
protected:
	bool isTimerRunning() { return fTimerID != 0; }
	void timerStart(int aInterval, Config& aConfig)
	{
		timerStop();
		fClockTimerInfo.clock = this;
		fClockTimerInfo.config = &aConfig;
		fTimerID = SDL_AddTimer(aInterval, &timerUpdateCLock, &fClockTimerInfo);
	}
	void timerStop()
	{
		if (isTimerRunning())
		{
			SDL_RemoveTimer(fTimerID);
			fTimerID = 0;
		}
	}
protected:
	static void calculateArmAngles(int aHour, int aMinutes, int aSeconds, double* aHourAngle = nullptr, double* aMinutesAngle = nullptr, double* aSecondsAngle = nullptr)
	{
		double secondsFraction = (double)aSeconds / 60.0;
		double minutesFraction = ((double)aMinutes + secondsFraction) / 60.0;
		double hourFraction = ((double)(aHour % 12) + minutesFraction) / 12.0;
		if (aHourAngle)
			*aHourAngle = (2.0 * pi * hourFraction) - 0.5 * pi;
		if (aMinutesAngle)
			*aMinutesAngle = 2.0 * pi * minutesFraction - 0.5 * pi;
		if (aSecondsAngle)
			*aSecondsAngle = 2.0 * pi * secondsFraction - 0.5 * pi;
	}
	static SDL_Point time_location(SDL_Point aCenter, double aAngle, double aRadius)
	{
		return SDL_Point
		{
			aCenter.x + int(std::round(aRadius * std::cos(aAngle))),
			aCenter.y + int(std::round(aRadius * std::sin(aAngle)))
		};
	}
	void drawAlarmState(Config& aConfig, int x, int y, uint32_t aColor = 0xc0ffffff, bool aSmall = false)
	{
		auto alarmDelta = aConfig.calculateAlarmDelta();
		if (alarmDelta)
		{
			auto rect = relativeDisplayPosition(x, y);

			if (aConfig.showNextAlarmWithDay)
			{
				// alarm day and time
				auto text = LocalDateTime::clockNextAlarmStr(aConfig.alarms[0]);
				if (!fFontAlarmState)
					fFontAlarmState = aConfig.renderer->getFont(rect.h / ((aSmall && aConfig.bigScreen()) ? 24 : 14), "segoeuisl.ttf");
				
				if (aConfig.bigScreen())
				{
					if (aSmall)
					{
						auto textRect = aConfig.renderer->textRect(text, fFontAlarmState, rect.x + 24, rect.y + parent->position.y + parent->position.h - (rect.h / 24) - 12);
						aConfig.renderer->drawText(text, fFontAlarmState, aColor, &textRect);
					}
					else
					{
						auto textRect = aConfig.renderer->textRect(text, fFontAlarmState, rect.x + 48, rect.y + parent->position.y + parent->position.h - (rect.h / 14) - 16);
						aConfig.renderer->drawText(text, fFontAlarmState, aColor, &textRect);
					}
				}
				else
				{
					auto textRect = aConfig.renderer->textRect(text, fFontAlarmState, rect.x + 24, rect.y + parent->position.y + parent->position.h - (rect.h / 14) - 12);
					aConfig.renderer->drawText(text, fFontAlarmState, aColor, &textRect);
				}
			}

			// orb with state of alarm
			auto size = (aConfig.bigScreen() && !aSmall) ? 24 : 12;
			SDL_Rect orbRect{ rect.x + 12,rect.y + rect.h - (12 + 24),size,size };
			if ((!aConfig.bigScreen()) || aSmall)
			{
				orbRect.x -= 8;
				orbRect.y += 14;
				orbRect.w = 12;
				orbRect.h = 12;
			}
			if (alarmDelta <= NEXT_ALARM_RED_TIME)
			{
				// red orb
				if (!fRedOrbTexture)
					fRedOrbTexture = aConfig.renderer->loadTexture(fImagePath / ".." / "images"  / ((aConfig.bigScreen() && !aSmall) ? "orb-24-red.png" : "orb-red.png"));
				aConfig.renderer->drawTexture(fRedOrbTexture, &orbRect);
			}
			else
			{
				// green orb
				if (!fGreenOrbTexture)
					fGreenOrbTexture = aConfig.renderer->loadTexture(fImagePath / ".." / "images" / ((aConfig.bigScreen() && !aSmall) ? "orb-24-green.png" : "orb-green.png"));
				aConfig.renderer->drawTexture(fGreenOrbTexture, &orbRect);
				
			}
		}
	}
	void drawCurrentDate(Config& aConfig, int x, int y, uint32_t aColor = 0xc0ffffff)
	{
		if (fShowCurrentDate && aConfig.showCurrentDate)
		{
			auto rect = relativeDisplayPosition(x, y);
			auto text = LocalDateTime::clockDateStr(aConfig.currentTime);
			if (!fFontDate)
				fFontDate = aConfig.renderer->getFont(rect.h / 14, "segoeuisl.ttf");
			if (aConfig.bigScreen())
			{
				auto textRect = aConfig.renderer->textRect(text, fFontDate, rect.x + 48, rect.y + 12);
				aConfig.renderer->drawText(text, fFontDate, aColor, &textRect);
			}
			else
			{
				auto textRect = aConfig.renderer->textRect(text, fFontDate, rect.x + 48 - 34, rect.y + 6);
				aConfig.renderer->drawText(text, fFontDate, aColor, &textRect);
			}
		}
	}
protected:
	SDL_Texture* fGreenOrbTexture = nullptr;
	SDL_Texture* fRedOrbTexture = nullptr;
	TTF_Font* fFontAlarmState = nullptr;
	TTF_Font* fFontDate = nullptr;
protected:
	bool fDrawBackground = true;
	bool fShowCurrentDate = true;
	SDL_TimerID fTimerID = 0;
	int fTimerInterval = 15000; // default
	ClockTimerInfo fClockTimerInfo;
	fs::path fImagePath;
	int fClockType = 0;
};




