// whole schedule of week based alarms

#pragma once

#include <string>
#include <vector>
#include <map>

#include "../sdl_eventloop.h"

#include "timeselectors.h"
#include "timeeditors.h"
#include "dayselectors.h"


class ScheduleRow : public Element
{
public:
	ScheduleRow(fs::path aImagePath, int aHour, int aMinutes, int aDaysInSchedule, const std::vector<std::string>& aDaysOfWeek, const SDL_Rect& aPosition, Config& aConfig)
		: Element(aPosition, false, true)
	{
		fImagePath = aImagePath;
		// time selector - day selectors - next active
		timeSelector = aConfig.bigScreen() 
			? std::make_shared<TimeSelector>(fImagePath, aHour, aMinutes, SDL_Rect{ aPosition.x,aPosition.y + aPosition.h - 64,171,64 })
			: std::make_shared<TimeSelector>(fImagePath, aHour, aMinutes, SDL_Rect{ aPosition.x,aPosition.y + aPosition.h - 32,52,32 });
		timeSelector->onShortTouch = [this](TimeSelector* aTimeSelector, Config& aConfig) -> int
		{
			if (aTimeSelector->isActive())
			{
				if (onEditTime)
				{
					onEditTime(this, aConfig);
					if (onDaysChanged)
						onDaysChanged(this, aConfig);
					return RERENDER_ALL;
				}
				else
					return RERENDER_ALL;
			}
			else
			{
				for (auto& day : fDays)
				{
					if (day->activeImage == 2)
						day->activeImage = 0;
				}
				aTimeSelector->setActive(true);
				if (onDaysChanged)
					return onDaysChanged(this, aConfig);
				else
					return RERENDER_ALL;
			}
		};
		timeSelector->onLongTouch = [this](TimeSelector* aTimeSelector, Config& aConfig) -> int
		{
			if (aTimeSelector->isActive())
			{
		        for (auto& day : fDays)
				{
					if (day->activeImage == 0)
						day->activeImage = 2;
				}
				aTimeSelector->setActive(false);
				if (onDaysChanged)
					return onDaysChanged(this, aConfig);
				else
					return RERENDER_ALL;
			}
			else
			{
				auto atLeast1DayActive = false;
				for (auto& day : fDays)
				{
					if (day->activeImage == 2)
						day->activeImage = 0;
					if (day->activeImage == 0)
						atLeast1DayActive = true;
				}
				if (atLeast1DayActive)
				{
					aTimeSelector->setActive(true);
					if (onDaysChanged)
						return onDaysChanged(this, aConfig);
				}
				else
				{
					// remove this entry as a whole
					if (onRemoveEntry)
						return onRemoveEntry(this, aConfig);
				}
				// fall through
				return RERENDER_ALL;
			}
		};
		addChild(timeSelector);
		auto left = aPosition.x + (aConfig.bigScreen() ? 180 : 54);
		auto right = aPosition.x + aPosition.w - (aConfig.bigScreen() ? 24 : 6);
		auto stride = (right - left) / (aDaysInSchedule);
		for (int day = 0; day < aDaysInSchedule; day++)
		{
			auto centerX = int(left + double(stride) * double(day + 0.5));
			auto orb = std::make_shared<DaySelectorOrb>(fImagePath, SDL_Rect{ centerX - 6,aPosition.y-2,12,12 });
			fOrbs.push_back(orb);
			addChild(orb);
			auto daySelector =  aConfig.bigScreen() 
				? std::make_shared<DaySelector>(fImagePath, false, 0, aDaysOfWeek[day], SDL_Rect{ centerX - 32,aPosition.y + aPosition.h - 64,64,64 }, aConfig)
				: std::make_shared<DaySelector>(fImagePath, false, 0, aDaysOfWeek[day], SDL_Rect{ centerX - 16,aPosition.y + aPosition.h - 32,32,32 }, aConfig);
			daySelector->onChanged = [this](DaySelector* aDaySelector, Config& aConfig) -> int
			{
				// test if we have to fixup the time selector state
				if (aDaySelector->isActive() && !timeSelector->isActive())
				{
					// time was disabled -> enable all postponed entries and enabled time selector
					timeSelector->setActive(true);
					for (auto& day : fDays)
					{
						if (day->activeImage == 2)
							day->activeImage = 0;
					}
					if (onDaysChanged)
						return onDaysChanged(this, aConfig);
					else
						return RERENDER_ALL;
				}
				else
				{
					if (onDaysChanged)
						return onDaysChanged(this, aConfig);
					else
						return RERENDER_ALL;
				}
			};
			fDays.push_back(daySelector);
			addChild(daySelector);
		}
	}
public:
	virtual std::string elementName() override { return "ScheduleRow"; }
	std::shared_ptr<TimeSelector> timeSelector;
	std::function<int(ScheduleRow* aScheduleRow, Config& aConfig)> onEditTime;
	std::function<int(ScheduleRow* aScheduleRow, Config& aConfig)> onDaysChanged;
	std::function<int(ScheduleRow* aScheduleRow, Config& aConfig)> onRemoveEntry;
public:
	void activateSchedule(int aDayOfWeek, int aDelay, bool aFirst, bool aRed)
	{
		fDays[aDayOfWeek]->activeImage = aDelay < 1000 ? 0 : 2;
		if (aDelay < 1000)
			fDays[aDayOfWeek]->setDelay(aDelay);
		if (aFirst && aDelay < 1000)
		{
			if (aRed)
				fOrbs[aDayOfWeek]->setRed();
			else
				fOrbs[aDayOfWeek]->setGreen();
		}
		else
			fOrbs[aDayOfWeek]->setInvisble();
		if (fDays[aDayOfWeek]->activeImage == 0)
			timeSelector->setActive(true);
	}
	void deactivateAllSchedules()
	{
		timeSelector->setActive(false);
		for (auto& day : fDays)
		{
			day->setActive(false);
			day->resetDelay();
		}
		for (auto& orb : fOrbs)
			orb->visible = false;
	}
	std::map<int, int> getActiveDaysOfWeek()
	{
		std::map<int, int> res;
		for (int dayOfWeek = 0; dayOfWeek < int(fDays.size()); dayOfWeek++)
		{
			if (fDays[dayOfWeek]->activeImage != 1)
			{
				res[dayOfWeek] = fDays[dayOfWeek]->activeImage == 0 ? fDays[dayOfWeek]->delay : 1000;
			}
		}
		return res;
	}
protected:
	fs::path fImagePath;
	std::vector<std::shared_ptr<DaySelectorOrb>> fOrbs;
	std::vector<std::shared_ptr<DaySelector>> fDays;
};

class Schedule : public Element
{
public:
	Schedule(fs::path aImagePath, const std::vector<std::string>& aDaysOfWeek, const SDL_Rect& aPosition)
		: Element(aPosition, false, true)
	{
		fImagePath = aImagePath;
		fDaysOfWeek = aDaysOfWeek;
	}
public:
	virtual std::string elementName() override { return "Schedule"; }
	int virtual onInit(Config& aConfig) override
	{ 
		// start clean
		clearChildren(aConfig);
		fRows.clear();
		// rebuild rows, generate ScheduleRow elements for all unique times in aConfig.alarms
		for (auto& alarm : aConfig.alarms)
		{
			auto intAlarm = LocalDateTime::timePart(alarm);
			auto it = fRows.find(intAlarm);
			if (it == fRows.end())
				fRows.insert({ intAlarm, nullptr });
		}
		auto p = aConfig.bigScreen() 
			? SDL_Rect{ position.x + 16,position.y + 16,position.w - 32,64 + 20 }
			: SDL_Rect{ position.x + 8,position.y + 8,position.w - 8,32 + 12 };
		for(auto& row : fRows)
		{
			int hour, minutes;
			LocalDateTime::timeParts(row.first, &hour, &minutes);
			row.second = std::make_shared<ScheduleRow>(fImagePath, hour, minutes, aConfig.daysInSchedule, fDaysOfWeek, p, aConfig);
			row.second->onEditTime = [this](ScheduleRow* aScheduleRow, Config& aConfig) -> int
			{
				// start editor
				auto timeEditor = std::make_shared<TimeEditor>(aScheduleRow->timeSelector->hour, aScheduleRow->timeSelector->minutes, parent->position);
				timeEditor->onEditorDone = [this, aScheduleRow](TimeEditor* aTimeEditor, Config& aConfig) -> int
				{
					auto newHour = aTimeEditor->hour;
					auto newMinutes = aTimeEditor->minutes;
					auto oldIntTime = LocalDateTime::fromTimeParts(aScheduleRow->timeSelector->hour, aScheduleRow->timeSelector->minutes);
					auto newIntTime = LocalDateTime::fromTimeParts(newHour, newMinutes);
					if (oldIntTime != newIntTime)
					{
						std::scoped_lock guard(aConfig.lock);
						// save time to correct row, remove and re-insert row with correct int-time
						std::shared_ptr<ScheduleRow> row = fRows[oldIntTime];
						fRows.erase(oldIntTime);
						fRows.insert({ newIntTime, row });
						// re-order children by moving position on screen (NOT in children)
						SDL_Point p = { position.x + 16,position.y + 16 };
						for (auto& row : fRows)
						{
							row.second->move(0, p.y - row.second->position.y);
							p.y += 64 + 20 + 10;
						}
						// adjust time in time selector
						aScheduleRow->timeSelector->hour = newHour;
						aScheduleRow->timeSelector->minutes = newMinutes;
						// save alarms, re-order and load again
						saveAlarms(aConfig);
						loadAlarms(aConfig); // to update orbs
						// disable editor
						parent->removeChild(aTimeEditor);
						return RERENDER_ALL;
					}
					else
					{
						// disable editor
						parent->removeChild(aTimeEditor);
						return RERENDER_ALL;
					}
				};
				// enable editor
				std::scoped_lock guard(aConfig.lock);
				parent->addChild(timeEditor);
				return RERENDER_ALL;
			};
			row.second->onDaysChanged = [this](ScheduleRow* aScheduleRow, Config& aConfig) -> int
			{
				saveAlarms(aConfig);
				loadAlarms(aConfig);
				return RERENDER_ALL;
			};
			row.second->onRemoveEntry = [this](ScheduleRow* aScheduleRow, Config& aConfig) -> int
			{
				std::scoped_lock guard(aConfig.lock);
				//auto oldIntTime = LocalDateTime::fromTimeParts(aScheduleRow->timeSelector->hour, aScheduleRow->timeSelector->minutes);
				// save time to correct row, remove and re-insert row with correct int-time
				//std::shared_ptr<ScheduleRow> row = fRows[oldIntTime];
				//fRows.erase(oldIntTime);
				// save alarms, re-order and load again
				saveAlarms(aConfig);
				//removeChild(aScheduleRow);
				dispatch.push_back({ [this, &aConfig]() -> void { this->onInit(aConfig); } });
				return RERENDER_ALL; //RERENDER_ALL_REMOVE_PARENT_FROM_PARENT;
			};
			addChild(row.second);
			p.y += p.h + (aConfig.bigScreen() ? 10 : 4);
		}
		loadAlarms(aConfig);
		// add new-row button
		p.y += aConfig.bigScreen() ? 16 : 8;
		p.w = aConfig.bigScreen() ? 48 : 32;
		p.h = aConfig.bigScreen() ? 48 : 32;
		auto plusButton = std::make_shared<Button>(fImagePath, std::vector<std::string>{ "button-48.png" }, -1, "+", p);
		plusButton->onClick = [this](Config& aConfig) -> int 
		{
			std::scoped_lock guard(aConfig.lock);
			saveAlarms(aConfig);
			// add extra alarm with new time
			aConfig.addNewTime();
			aConfig.recalculateAlarms(aConfig.currentTime);
			return onInit(aConfig);
		};
		addChild(plusButton);

		return RERENDER_ALL; 
	}
	void virtual onExit(Config& aConfig) override
	{
		saveAlarms(aConfig);
	}
	void loadAlarms(Config& aConfig)
	{
		for (auto& row : fRows)
			row.second->deactivateAllSchedules();
		// add single alarms to rows
		auto first = true;
		for (auto& alarm : aConfig.alarms)
		{
			auto intAlarm = LocalDateTime::timePart(alarm);
			auto it = fRows.find(intAlarm);
			if (it != fRows.end())
			{
				auto dayOfWeek = LocalDateTime::dayOfWeek(alarm);
				auto delay = int((alarm - aConfig.currentTime) / (SECONDS_PER_DAY * aConfig.daysInSchedule));
				it->second->activateSchedule(dayOfWeek, delay, first, alarm - aConfig.currentTime <= NEXT_ALARM_RED_TIME);
				first = false;
			}
			// else error
		}
	}
	void saveAlarms(Config& aConfig)
	{
		// build list of alarms and set list of alarms in config
		aConfig.alarms.clear();
		std::cout << "      alarms: ";
		for (auto& row : fRows)
		{
			auto activeDays = row.second->getActiveDaysOfWeek();
			for (auto& dow : activeDays)
			{
				auto time = aConfig.currentTime;
				auto dayOfWeek = LocalDateTime::dayOfWeek(time);
				while (dayOfWeek != dow.first)
				{
					time += SECONDS_PER_DAY;
					dayOfWeek = LocalDateTime::dayOfWeek(time);
				}
				time = LocalDateTime::datePart(time) + row.first;
				// correct if alarm is today but already in the past
				if (time < aConfig.currentTime)
					time += SECONDS_PER_DAY * aConfig.daysInSchedule;
				time += SECONDS_PER_DAY* aConfig.daysInSchedule*dow.second; // add delay time (number of weeks)
				time = ((time + time_t(5) * 30) / (time_t(5) * 60)) * time_t(5) * 60; // round to 5 minutes
				std::cout << LocalDateTime::ISO(time) << ", ";
				aConfig.alarms.push_back(time);
			}
		}
		std::cout << std::endl;
		aConfig.recalculateAlarms(aConfig.currentTime);
	}
protected:
	fs::path fImagePath;
	std::vector<std::string> fDaysOfWeek;
	std::map<time_t, std::shared_ptr<ScheduleRow>> fRows;
};
