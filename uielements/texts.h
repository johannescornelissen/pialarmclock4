// generic text

#pragma once

#include <sstream>

#include "../sdl_eventloop.h"


class Text : public Element
{
public:
	static std::string doubleToText(double aValue, int aDigits)
	{
		std::stringstream ss;
		ss << std::setprecision(aDigits) << aValue;
		return ss.str();
	}
public:
	Text(const std::string& aText, const std::string& aFontName, int aFontSize, SDL_Rect aPosition) 
		: Element(aPosition, true, true)
	{
		fText = aText;
		fFontName = aFontName;
		fFontSize = aFontSize;
	}
public:
	virtual std::string elementName() override { return "Text"; }
	void updateText(const std::string& aText, Config& aConfig)
	{
		std::scoped_lock guard(aConfig.lock);
		fText = aText;
	}
	void updateText(int aValue, Config& aConfig)
	{
		std::scoped_lock guard(aConfig.lock);
		fText = std::to_string(aValue);
	}
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			auto rect = relativeDisplayPosition(x, y);
			// text
			if (!fFont)
				fFont = aConfig.renderer->getFont(fFontSize, fFontName);
			auto textRect = aConfig.renderer->textRect(fText, fFont, rect.x, rect.y);
			aConfig.renderer->drawText(fText, fFont, 0x80ffffff, &textRect);
			return true;
		}
		else
			return false;
	}
protected:
	std::string fText;
	std::string fFontName;
	int fFontSize;
	TTF_Font* fFont = nullptr;
};

