#pragma once

#include "clocks.h"

#define INIT_ANIM_STEPS 20
#define MICKEY_ANIMATION_INTERVAL 50

class ClockMickey : public Clock
{
public:
	ClockMickey(fs::path aImagePath, SDL_Rect aPosition, int aClockType)
		: Clock(aImagePath, aPosition, aClockType)
	{
		fColorCipherText = 0xff909090;
		fColorCipherBackground = 0xff202020;
		fColorBackground = 0xff000000;

		fTimerInterval = 1000;// 1000 / 10; //1000 / 15;

	}
public:
	virtual void activate(Config& aConfig, bool aFull) override
	{
		Clock::activate(aConfig, aFull);
	}
	virtual void deactivate(Config& aConfig) override
	{
		stopAnimTimer();
		Clock::deactivate(aConfig);
	}
protected:
	virtual std::string elementName() override { return "ClockMickey"; }

	struct MickeyAnimInfo
	{
		ClockMickey* clock;
		Config* config;
		SDL_Rect rect;
		SDL_Point center;
	};
	
	class MickeyTextures
	{
	public:
		//SDL_Texture* background = nullptr;
		//SDL_Texture* scale = nullptr;
		SDL_Texture* hour = nullptr;
		SDL_Texture* body = nullptr;

		std::vector<SDL_Texture*> sequenceTapShoe;

		SDL_Texture* head = nullptr;

		std::vector<SDL_Texture*> sequenceBlinkEyes;

		SDL_Texture* minuteA = nullptr;
		SDL_Texture* minuteB = nullptr;
		double minuteAStartAngle = 0;
		double minuteAEndAngle = 0;

		std::vector<SDL_Texture*> sequenceYawnEyesAndMouth; // mouth is in eyes
		std::vector<SDL_Texture*> sequenceYawnHead;

		void render(
			Config& aConfig, SDL_Rect* aRect, SDL_Point* aCenter,
			double aHourAngle, double aMinuteAngle,
			SDL_Rect* aBodyRect, SDL_Rect* aFaceRect,
			size_t& blink, size_t& yawn, size_t& tap, size_t& dance)
		{
			//aConfig.renderer->drawTextureScale(background, aRect);
			//aConfig.renderer->drawTextureScale(scale, aRect);
			aConfig.renderer->drawTextureRotated(hour, aRect, aHourAngle, aCenter);
			aConfig.renderer->drawTextureScale(body, aBodyRect);
			// shoe (and sequence)
			aConfig.renderer->drawTextureScale(sequenceTapShoe[tap], aRect);
			if (tap > 0) { tap++; if (tap >= sequenceTapShoe.size()) { tap = 0; if (dance > 0) dance++; } }
			// head and eyes
			if (yawn == 0)
			{
				// optional blink
				aConfig.renderer->drawTextureScale(head, aFaceRect);
				aConfig.renderer->drawTextureScale(sequenceBlinkEyes[blink], aFaceRect);
				if (blink > 0) { blink++; if (blink >= sequenceBlinkEyes.size()) blink = 0; }
			}
			else
			{
				// yawn (sequence)
				size_t yawnDiv3 = (size_t)(yawn / 3);
				aConfig.renderer->drawTextureScale(sequenceYawnHead[yawnDiv3], aRect);
				aConfig.renderer->drawTextureScale(sequenceYawnEyesAndMouth[yawnDiv3], aRect);
				if (yawn > 0)
				{
					yawn++;
					if ((size_t)(yawn / 3) >= sequenceYawnHead.size())
						yawn = 0;
				}
			}
			// minute arm
			if (minuteAStartAngle <= aMinuteAngle && aMinuteAngle < minuteAEndAngle)
			{
				if (minuteA)
					aConfig.renderer->drawTextureRotated(minuteA, aRect, aMinuteAngle, aCenter);
			}
			else
			{
				if (minuteB)
					aConfig.renderer->drawTextureRotated(minuteB, aRect, aMinuteAngle, aCenter);
			}
		}

		void renderBlink(Config& aConfig, SDL_Rect* aRect, SDL_Point* aCenter, double aMinuteAngle, size_t& blink)
		{
			aConfig.renderer->drawTextureScale(head, aRect);
			aConfig.renderer->drawTextureScale(sequenceBlinkEyes[blink], aRect);
			if (blink > 0) { blink++; if (blink >= sequenceBlinkEyes.size()) blink = 0; }
			// minute arm
			if (minuteAStartAngle <= aMinuteAngle && aMinuteAngle < minuteAEndAngle)
			{
				if (minuteA)
					aConfig.renderer->drawTextureRotated(minuteA, aRect, aMinuteAngle, aCenter);
			}
			else
			{
				if (minuteB)
					aConfig.renderer->drawTextureRotated(minuteB, aRect, aMinuteAngle, aCenter);
			}
		}
	};

	void loadTextures(Config& aConfig)
	{
		auto textureBack = aConfig.renderer->loadTexture(fImagePath / "back.png");
		auto textureScale = aConfig.renderer->loadTexture(fImagePath / "scale1.png");
		{
			//texturesA.background = textureBack;
			//texturesA.scale = textureScale;
			texturesA.hour = aConfig.renderer->loadTexture(fImagePath / "hour1.png");
			texturesA.body = aConfig.renderer->loadTexture(fImagePath / "body1.png");

			auto shoeA = aConfig.renderer->loadTexture(fImagePath / "shoe1.png");
			auto shoeB = aConfig.renderer->loadTexture(fImagePath / "shoe1b.png");
			auto shoeC = aConfig.renderer->loadTexture(fImagePath / "shoe1c.png");
			auto shoeD = aConfig.renderer->loadTexture(fImagePath / "shoe1d.png");
			auto shoeE = aConfig.renderer->loadTexture(fImagePath / "shoe1e.png");
			texturesA.sequenceTapShoe = std::vector<SDL_Texture*>{ shoeA, shoeB, shoeC, shoeD, shoeE, shoeD, shoeC, shoeB, shoeA };

			texturesA.head = aConfig.renderer->loadTexture(fImagePath / "head1.png");

			auto eyesA = aConfig.renderer->loadTexture(fImagePath / "eyes1.png");
			auto eyesB = aConfig.renderer->loadTexture(fImagePath / "eyes1b.png");
			auto eyesC = aConfig.renderer->loadTexture(fImagePath / "eyes1c.png");
			texturesA.sequenceBlinkEyes = std::vector<SDL_Texture*>{ eyesA, eyesB, eyesC, eyesB, eyesA };

			texturesA.minuteA = aConfig.renderer->loadTexture(fImagePath / "minute1a.png");
			texturesA.minuteB = aConfig.renderer->loadTexture(fImagePath / "minute1b.png");
			texturesA.minuteAStartAngle = 0;
			texturesA.minuteAEndAngle = 90;

			auto mouthYawnA = aConfig.renderer->loadTexture(fImagePath / "mouthyawn1a.png");
			auto mouthYawnB = aConfig.renderer->loadTexture(fImagePath / "mouthyawn1b.png");
			texturesA.sequenceYawnEyesAndMouth = std::vector<SDL_Texture*>{
				eyesA, eyesB, eyesC,
				mouthYawnA, mouthYawnA, mouthYawnB, mouthYawnB, mouthYawnB, mouthYawnB, mouthYawnA,
				eyesC, eyesB, eyesA
			};

			auto headA = texturesA.head;
			auto headYawnA = aConfig.renderer->loadTexture(fImagePath / "headyawn1a.png");
			auto headYawnB = aConfig.renderer->loadTexture(fImagePath / "headyawn1b.png");
			texturesA.sequenceYawnHead = std::vector<SDL_Texture*>{
				headA, headA, headA,
				headYawnA, headYawnA, headYawnB, headYawnB, headYawnB, headYawnB, headYawnA,
				headA, headA, headA
			};
		}
		{
			//texturesB.background = textureBack;
			//texturesB.scale = textureScale;
			texturesB.hour = aConfig.renderer->loadTexture(fImagePath / "hour2.png");
			texturesB.body = aConfig.renderer->loadTexture(fImagePath / "body2.png");

			auto shoeA = aConfig.renderer->loadTexture(fImagePath / "shoe2.png");
			auto shoeB = aConfig.renderer->loadTexture(fImagePath / "shoe2b.png");
			auto shoeC = aConfig.renderer->loadTexture(fImagePath / "shoe2c.png");
			auto shoeD = aConfig.renderer->loadTexture(fImagePath / "shoe2d.png");
			auto shoeE = aConfig.renderer->loadTexture(fImagePath / "shoe2e.png");
			texturesB.sequenceTapShoe = std::vector<SDL_Texture*>{ shoeA, shoeB, shoeC, shoeD, shoeE, shoeD, shoeC, shoeB, shoeA };

			texturesB.head = aConfig.renderer->loadTexture(fImagePath / "head2.png");

			auto eyesA = aConfig.renderer->loadTexture(fImagePath / "eyes2.png");
			auto eyesB = aConfig.renderer->loadTexture(fImagePath / "eyes2b.png");
			auto eyesC = aConfig.renderer->loadTexture(fImagePath / "eyes2c.png");
			texturesB.sequenceBlinkEyes = std::vector<SDL_Texture*>{ eyesA, eyesB, eyesC, eyesB, eyesA };

			texturesB.minuteA = aConfig.renderer->loadTexture(fImagePath / "minute2a.png");
			texturesB.minuteB = aConfig.renderer->loadTexture(fImagePath / "minute2b.png");
			texturesB.minuteAStartAngle = 180;
			texturesB.minuteAEndAngle = 225;

			auto mouthYawnA = aConfig.renderer->loadTexture(fImagePath / "mouthyawn2a.png");
			auto mouthYawnB = aConfig.renderer->loadTexture(fImagePath / "mouthyawn2b.png");
			texturesB.sequenceYawnEyesAndMouth = std::vector<SDL_Texture*>{
				eyesA, eyesB, eyesC,
				mouthYawnA, mouthYawnA, mouthYawnB, mouthYawnB, mouthYawnB, mouthYawnB, mouthYawnA,
				eyesC, eyesB, eyesA
			};

			auto headA = texturesB.head;
			auto headYawnA = aConfig.renderer->loadTexture(fImagePath / "headyawn2a.png");
			auto headYawnB = aConfig.renderer->loadTexture(fImagePath / "headyawn2b.png");
			texturesB.sequenceYawnHead = std::vector<SDL_Texture*>{
				headA, headA, headA,
				headYawnA, headYawnA, headYawnB, headYawnB, headYawnB, headYawnB, headYawnA,
				headA, headA, headA
			};
		}
		{
			// textures for wave animation, base is texturesA
			//texturesWave.background = textureBack;
			//texturesWave.scale = textureScale;
			texturesWave.hour = aConfig.renderer->loadTexture(fImagePath / "arms3.png");
			texturesWave.body = texturesA.body;

			texturesWave.sequenceTapShoe = texturesA.sequenceTapShoe;

			texturesWave.head = aConfig.renderer->loadTexture(fImagePath / "head3.png");

			auto eyesA = aConfig.renderer->loadTexture(fImagePath / "eyes3.png");
			auto eyesB = aConfig.renderer->loadTexture(fImagePath / "eyes3b.png");
			auto eyesC = aConfig.renderer->loadTexture(fImagePath / "eyes3c.png");
			texturesWave.sequenceBlinkEyes = std::vector<SDL_Texture*>{ eyesA, eyesB, eyesC, eyesB, eyesA };

			// no minute arm
			texturesWave.minuteA = nullptr;
			texturesWave.minuteB = nullptr;
			texturesA.minuteAStartAngle = 0;
			texturesA.minuteAEndAngle = 90;

			texturesWave.sequenceYawnEyesAndMouth = std::vector<SDL_Texture*>{
				eyesA
			};

			auto headA = texturesWave.head;
			texturesWave.sequenceYawnHead = std::vector<SDL_Texture*>{
				headA,
			};

			auto handsA = aConfig.renderer->loadTexture(fImagePath / "hands3.png");
			auto handsB = aConfig.renderer->loadTexture(fImagePath / "hands3b.png");
			auto handsC = aConfig.renderer->loadTexture(fImagePath / "hands3c.png");
			fSequenceWaveHands = std::vector<SDL_Texture*>{
				handsA, handsB, handsA, handsC,
				handsA, handsB, handsA, handsC,
				handsA, handsB, handsA, handsC,
				handsA, handsB, handsA, handsC
			};
		}

		fSequenceBody = std::vector<SDL_Point>{ {0,0}, {1,4}, {3,8}, {6,12}, {3,16}, {0,20}, {-2,16},{-6,12}, {-3,8},{-1,4}, {0,0} };
	}

	MickeyTextures* getTextures(double aHourAngle)
	{
		double hourAngle = std::fmod(aHourAngle, 360.0);
		return (0.0 <= hourAngle) && (hourAngle < 180.0) ? &texturesB : &texturesA;
	}
	
	void startNextAnimation(Config& aConfig, SDL_Rect* aRect, SDL_Point* aCenter)
	{
#ifndef NDEBUG
		std::cout << "starting animation " << nextAnimation << std::endl;
#endif
		// reset all animations
		blink = 0; tap = 0; body = 0; dance = 0; yawn = 0; rotate = 0; wave = 0;
		switch (nextAnimation)
		{
		case 0:  blink = 1; nextAnimation++; break;
		case 1:  tap = 1; nextAnimation++; break;
		case 2:  body = 1; nextAnimation++; break;
		case 3:  dance = 1; nextAnimation++; break;
		case 4:  yawn = 1; nextAnimation++; break;
		case 5:  rotate = 1; nextAnimation++; break;
		default: wave = 1; nextAnimation = 0; break;
		}
		checkAnimAtrionTimer(aConfig, aRect, aCenter);
	}

	SDL_TimerID fAnimationTimerID = 0;
	MickeyAnimInfo fMickeyAnimInfo;

	void checkAnimAtrionTimer(Config& aConfig, SDL_Rect* aRect, SDL_Point* aCenter)
	{
		if (fAnimationTimerID == 0)
		{
			fMickeyAnimInfo.clock = this;
			fMickeyAnimInfo.config = &aConfig;
			fMickeyAnimInfo.rect = *aRect;
			fMickeyAnimInfo.center = *aCenter;
			fAnimationTimerID = SDL_AddTimer(MICKEY_ANIMATION_INTERVAL, [](Uint32 aInterval, void* aParam) ->Uint32
				{
					auto mickeyAnimInfo = (MickeyAnimInfo*)aParam;
					if (mickeyAnimInfo->clock->blink)
					{
						// render only head
						int hour, minutes, seconds;
						LocalDateTime::timeParts(mickeyAnimInfo->config->currentTime, &hour, &minutes, &seconds);
						double hourAngle, minutesAngle;
						calculateArmAngles(hour, minutes, seconds, &hourAngle, &minutesAngle);
						double hourAngleDeg = 360.0 * hourAngle / (2.0 * pi) + 90.0;
						double minuteAngleDef = 360.0 * minutesAngle / (2.0 * pi) + 90.0;
						std::scoped_lock guard(mickeyAnimInfo->config->lock);
						mickeyAnimInfo->clock->getTextures(hourAngleDeg)->renderBlink(
							*mickeyAnimInfo->config, 
							&mickeyAnimInfo->rect,
							&mickeyAnimInfo->center,
							minuteAngleDef,
							mickeyAnimInfo->clock->blink);
						mickeyAnimInfo->config->renderer->update();
					}
					else
					{
						sendRerenderEvent();
					}
					if (mickeyAnimInfo->clock->blink |
						mickeyAnimInfo->clock->tap |
						mickeyAnimInfo->clock->body |
						mickeyAnimInfo->clock->dance |
						mickeyAnimInfo->clock->yawn |
						mickeyAnimInfo->clock->rotate |
						mickeyAnimInfo->clock->wave)
					{
						return aInterval;
					}
					else
					{
						mickeyAnimInfo->clock->stopAnimTimer();
						return 0;
					}
				}, &fMickeyAnimInfo);
		}
	}

	void stopAnimTimer()
	{
		if (fAnimationTimerID != 0)
		{
			SDL_RemoveTimer(fAnimationTimerID);
			fAnimationTimerID = 0;
		}
	}
	
	void renderDial(Config& aConfig, SDL_Rect* aRect, SDL_Point* aCenter, double aHourAngle, double aMinuteAngle, int aHour, int aMinute, int aSecond, bool aAnimations)
	{
		if (rotate != 0)
		{
			rotate = 0;
			rotateAnimation(aConfig, aRect, aCenter, aHourAngle, aMinuteAngle, aHour, aMinute, aSecond);
			// redraw for the last time to re-enter the normal render sequence
			size_t dummy = 0;
			getTextures(aHourAngle)->render(aConfig, aRect, aCenter, aHourAngle, aMinuteAngle, aRect, aRect, dummy, dummy, dummy, dummy);
		}
		else if (wave != 0)
		{
			wave = 0;
			// show animation of mickey waving
			// arms go to 50 deg and 310 deg
			rotateSequence(aConfig, aRect, aCenter, aHourAngle, aHourAngle < 180 ? 50 : 310, aMinuteAngle, aHourAngle < 180 ? 310 : 50, aHour, aMinute, aSecond, 1000);
			SDL_Delay(100);
			// wave hands
			size_t blink = 0;
			for (size_t sequence = 0; sequence < fSequenceWaveHands.size(); sequence++)
			{
				//SDL_RenderClear(aRenderer);
				aConfig.renderer->clear();
				size_t dummy = 0;
				// blink half way during wave
				if (sequence == (size_t)(fSequenceWaveHands.size() / 2))
					blink = 1;
				texturesWave.render(aConfig, aRect, aCenter, 0, 0, aRect, aRect, blink, dummy, dummy, dummy);
				//SDL_RenderCopy(aRenderer, fSequenceWaveHands[sequence], NULL, aRect);
				aConfig.renderer->drawTextureScale(fSequenceWaveHands[sequence], aRect);
				//SDL_RenderPresent(aRenderer);
				aConfig.renderer->update();
				SDL_Delay(sequence == 0 ? 200 : 100);
			}
			SDL_Delay(100);
			// rotate arms back to original positions
			rotateSequence(aConfig, aRect, aCenter, aHourAngle < 180 ? 50 : 310, aHourAngle, aHourAngle < 180 ? 310 : 50, aMinuteAngle, aHour, aMinute, aSecond, 1000);
			SDL_Delay(100);
			// redraw for the last time to re-enter the normal render sequence
			{
				size_t dummy = 0;
				getTextures(aHourAngle)->render(aConfig, aRect, aCenter, aHourAngle, aMinuteAngle, aRect, aRect, dummy, dummy, dummy, dummy);
			}
		}
		else
		{
			// pre-calc
			// dance sequence
			if (dance > 0 && body == 0 && tap == 0)
			{
				switch (dance)
				{
				case 1: body = 1; tap = 1; break;
				case 2: body = 1; break;
				case 3: body = 1; tap = 1; break;
				case 4: body = 1; break;
				case 5: tap = 1; break;
				case 6: tap = 1; break;
				default: dance = 0; break;
				}
			}
			SDL_Rect stretchBodyRect = *aRect;
			SDL_Rect stretchFaceRect = *aRect;
			if (body > 0)
			{
				SDL_Point bodyDelta = fSequenceBody[body];
				// adjust body rect
				stretchBodyRect.x += bodyDelta.x;
				stretchBodyRect.w -= bodyDelta.x;
				stretchBodyRect.y += bodyDelta.y;
				stretchBodyRect.h -= bodyDelta.y;
				// adjust face rect
				stretchFaceRect.y += bodyDelta.y / 2;
				stretchFaceRect.h -= bodyDelta.y / 2;
				// next in sequence
				body++;
				if (body >= fSequenceBody.size()) { body = 0; if (dance > 0) dance++; }
			}
			// check to start animations
			if (aAnimations)
			{
				if (prev_second != aSecond)
				{
					auto minuteOfDay = aHour * 60 + aMinute;
					// blink every 7 seconds
					if ((yawn == 0) && (blink == 0) && (aSecond % 7 == 0))
						blink = 1;
					// dance every hour for 20 seconds
					if ((yawn == 0) && (dance == 0) && (aMinute == 0) && ((0 <= aSecond) && (aSecond < 20)))
						dance = 1;
					// tap every 15 minute exception on the hour (dance)
					if ((yawn == 0) && (dance == 0) && (aMinute % 15 == 0) && (0 == aSecond))
						tap = 1;
					// yawn every 13 minutes between 20:00 and 8:00
					if ((dance == 0) && (blink == 0) && ((20 <= aHour) || (aHour < 8)) && (aMinute % 13 == 0) && ((aSecond == 0) || (aSecond == 4)))
						yawn = 1;
					// wave every 43 minutes
					if ((blink == 0) && (dance == 0) && (yawn == 0) && (minuteOfDay % 43 == 0) && (aSecond == 0))
						wave = 1;
					checkAnimAtrionTimer(aConfig, aRect, aCenter);
					prev_second = aSecond;
				}
			}
			// render
			getTextures(aHourAngle)->render(aConfig, aRect, aCenter, aHourAngle, aMinuteAngle, &stretchBodyRect, &stretchFaceRect, blink, yawn, tap, dance);
		}
	}
	
	void rotateSequence(Config& aConfig, SDL_Rect* aRect, SDL_Point* aCenter,
		double aHourAngleFrom, double aHourAngleTo,
		double aMinuteAngleFrom, double aMinuteAngleTo,
		int aHour, int aMinute, int aSecond,
		int aAnimationTimeMS)
	{
		try
		{
			double currentHourAngle = aHourAngleFrom;
			double currentMinuteAngle = aMinuteAngleFrom;
			double deltaHourAngle = (aHourAngleTo - currentHourAngle) / (double)INIT_ANIM_STEPS;
			double deltaMinuteAngle = (aMinuteAngleTo - currentMinuteAngle) / (double)INIT_ANIM_STEPS;
			// animate
			int step = 0;
			while (step < INIT_ANIM_STEPS)
			{
				// render watch face
				aConfig.renderer->clear();
				renderDial(aConfig, aRect, aCenter, currentHourAngle, currentMinuteAngle, aHour, aMinute, aSecond, false);
				aConfig.renderer->update();
				SDL_Delay(aAnimationTimeMS / INIT_ANIM_STEPS);
				step++;
				currentHourAngle += deltaHourAngle;
				currentMinuteAngle += deltaMinuteAngle;
			}
			// precise last step to avoid rounding errors
			aConfig.renderer->clear();
			renderDial(aConfig, aRect, aCenter, aHourAngleTo, aMinuteAngleTo, aHour, aMinute, aSecond, false);
			aConfig.renderer->update();
		}
		catch (std::exception& e)
		{
			std::cout << "## exception in rotation animation: " << e.what() << std::endl;
		}
	}

	void rotateAnimation(Config& aConfig, SDL_Rect* aRect, SDL_Point* aCenter, double aHourAngle, double aMinuteAngle, int aHour, int aMinute, int aSecond)
	{
		bool hourClockWise = ((0 <= aHourAngle) && (aHourAngle < 90)) || ((180 <= aHourAngle) && (aHourAngle < 270));
		double destHourAngle = hourClockWise ? aHourAngle + 360 : aHourAngle - 360;
		double destMinuteAngle = hourClockWise ? aMinuteAngle - 360 : aMinuteAngle + 360;
		// do animation from given state to both arms at 6-o-clock and then back to current
		rotateSequence(aConfig, aRect, aCenter, aHourAngle, destHourAngle, aMinuteAngle, destMinuteAngle, aHour, aMinute, aSecond, 2000);
		SDL_Delay(500);
		rotateSequence(aConfig, aRect, aCenter, destHourAngle, aHourAngle, destMinuteAngle, aMinuteAngle, aHour, aMinute, aSecond, 2000);
	}

	int prev_second = -1;
	// animation states
	size_t nextAnimation = 0;
	size_t blink = 0;
	size_t tap = 0;
	size_t body = 0;
	size_t dance = 0;
	size_t yawn = 0;
	size_t rotate = 0;
	size_t wave = 0;
	std::vector<SDL_Point> fSequenceBody;
	std::vector<SDL_Texture*> fSequenceWaveHands;
	// textures, references
	MickeyTextures texturesA; // 1 looks to his right (6-12 o'clock)
	MickeyTextures texturesB; // 2 looks to his left (12-6 o'clock)
	MickeyTextures texturesWave; // 3 looks to the front (wave, body etc. is from 1)
protected:
	SDL_Texture* fBackgroundTexture = nullptr;
public:
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			Clock::renderThis(aConfig, x, y);

			auto rect = relativeDisplayPosition(x, y);

			auto dialRect = rect;
			if (aConfig.bigScreen())
			{
#ifdef BIG_MICKEY
				auto s = std::min(aConfig.screenSize.x, aConfig.screenSize.y);
				dialRect.w = s;
				dialRect.h = s;
#else
				// use native (max) resolution of images
				dialRect.w = 320;
				dialRect.h = 320;
#endif
			}
			else
			{
				auto s = std::min(aConfig.screenSize.x, aConfig.screenSize.y);
				dialRect.w = s;
				dialRect.h = s;
			}
			dialRect = rectCenter(rect, dialRect);

			if (!fBackgroundTexture)
			{
				auto backgroundTexture = aConfig.renderer->loadTexture(fImagePath / "back.png");
				auto scaleTexture = aConfig.renderer->loadTexture(fImagePath / "scale1.png");
				fBackgroundTexture = aConfig.renderer->createTargetTexture(fImagePath / "combinedBackAndScale", backgroundTexture);
				aConfig.renderer->drawTextureOnTexture(backgroundTexture, fBackgroundTexture);
				aConfig.renderer->drawTextureOnTexture(scaleTexture, fBackgroundTexture);
			}
			aConfig.renderer->drawTextureScale(fBackgroundTexture, &dialRect);
			
			if (!texturesA.body)
				loadTextures(aConfig);

			int hour, minutes, seconds;
			LocalDateTime::timeParts(aConfig.currentTime, &hour, &minutes, &seconds);

			double hourAngle, minutesAngle;
			calculateArmAngles(hour, minutes, seconds, &hourAngle, &minutesAngle);
			SDL_Point dialCenter{ /*dialRect.x + */(dialRect.w / 2),/*dialRect.y +*/(dialRect.h / 2)};

			renderDial(aConfig, &dialRect, &dialCenter, 360.0 * hourAngle / (2.0 * pi) + 90.0, 360.0 * minutesAngle / (2.0 * pi) + 90.0, hour, minutes, seconds, true);

			// date
			//drawCurrentDate(aConfig, x, y);

			// draw time in corners
			if (!fFontTime)
				fFontTime = aConfig.renderer->getFont(int(double(rect.h) / 5.0), "segoeuisl.ttf");

			auto time = std::to_string(hour);
			auto timeRect = aConfig.renderer->textRect(time, fFontTime, rect.x, rect.y);
			timeRect = rectRight(rect, timeRect);
			timeRect.x -= rect.w / 50;
			//timeRect.y -= timeRect.h / 7;
			aConfig.renderer->drawText(time, fFontTime, 0xffaaaaaa, &timeRect);

			time = std::to_string(minutes);
			timeRect = aConfig.renderer->textRect(time, fFontTime, rect.x, rect.y);
			timeRect = rectRight(rect, timeRect);
			timeRect.x -= rect.w / 50;
			timeRect.y += int(timeRect.h * 2.5);
			aConfig.renderer->drawText(time, fFontTime, 0xffaaaaaa, &timeRect);

			// next alarm
			drawAlarmState(aConfig, x, y);
			
			// make sure we are active
			activate(aConfig, false);

			return true;
		}
		else
			return false;
	}
protected:
	TTF_Font* fFontTime = nullptr;
	Uint32 fColorCipherText;
	Uint32 fColorCipherBackground;
	Uint32 fColorBackground;
};
