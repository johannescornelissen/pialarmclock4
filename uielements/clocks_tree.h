#pragma once

#include "clocks.h"


class ClockTree : public Clock
{
public:
	ClockTree(fs::path aImagePath, SDL_Rect aPosition, Config& aConfig, int aClockType)
		: Clock(aImagePath, aPosition, aClockType)
	{
		fClockBackground = std::make_shared<Animation>(aConfig.renderer, fImagePath, "", "", this->position);
		fClockBackground->load(aConfig.renderer, false);
	}
public:
	virtual std::string elementName() override { return "ClockTree"; }
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			fClockBackground->renderThis(aConfig, x, y);

			auto rect = relativeDisplayPosition(x, y);

			int hour, minutes;
			LocalDateTime::timeParts(aConfig.currentTime, &hour, &minutes);

			// hour+minutes
			if (!fFontTime)
				fFontTime = aConfig.renderer->getFont(int(double(rect.h) / 2.5), "segoeuisl.ttf");

			auto time = std::to_string(hour);
			auto timeRect = aConfig.renderer->textRect(time, fFontTime, rect.x, rect.y);
			timeRect = rectLeft(rect, timeRect);
			timeRect.x += rect.w / 50;
			timeRect.y -= timeRect.h / 7;
			aConfig.renderer->drawText(time, fFontTime, 0x60000000, &timeRect);

			time = std::to_string(minutes);
			timeRect = aConfig.renderer->textRect(time, fFontTime, rect.x, rect.y);
			timeRect = rectLeft(rect, timeRect);
			timeRect.x += rect.w / 50;
			timeRect.y += int(timeRect.h * 0.86);
			aConfig.renderer->drawText(time, fFontTime, 0x60000000, &timeRect);

			// next alarm
			drawAlarmState(aConfig, x, y, 0x60000000, true);

			return true;
		}
		else
			return false;
	}
public:
	virtual void activate(Config& aConfig, bool aFull) override
	{
		Clock::activate(aConfig, aFull);
		if (fClockBackground)
			fClockBackground->activate(aConfig, aFull);
	}
	virtual void deactivate(Config& aConfig) override
	{
		Clock::deactivate(aConfig);
		if (fClockBackground)
			fClockBackground->deactivate(aConfig);
	}
protected:
	TTF_Font* fFontTime = nullptr;
	std::shared_ptr<Animation> fClockBackground;
};
