// panel with pages, children are always absolutely positioned on panel, 
// pages position dependent on move state, 
// a move snaps to a new active page which is positioned absolute again

#pragma once

#include "../sdl_eventloop.h"

#define spiNone -1
#define spiTop 0
#define spiRight 1
#define spiBottom 2
#define spiLeft 3


class SlidingPanel; //forward


class SlidingPanelPage
{
public:
	static int oppositePosition(int aPosition)
	{
		switch (aPosition)
		{
		case spiTop:	return spiBottom;
		case spiRight:	return spiLeft;
		case spiBottom:	return spiTop;
		case spiLeft:	return spiRight;
		default:		return spiNone;
		}
	}
public:
	SlidingPanelPage(std::shared_ptr<Element> aElement)
	{
		element = aElement;
	}
public:
	std::shared_ptr<Element> element;
	SlidingPanelPage* neighbours[4]{ nullptr,nullptr,nullptr,nullptr };
public:
	void setNeighbour(int aPosition, SlidingPanelPage* aNeighbour)
	{
		// link
		neighbours[aPosition] = aNeighbour;
		// link back, if not linked back yet
		auto opposite = oppositePosition(aPosition);
		if (!aNeighbour->neighbours[opposite])
			aNeighbour->neighbours[opposite] = this;
	}
	void setNeighbours(SlidingPanelPage* aBottom, SlidingPanelPage* aRight, SlidingPanelPage* aTop = nullptr, SlidingPanelPage* aLeft = nullptr)
	{
		// link
		if (aBottom)
		{
			neighbours[spiBottom] = aBottom;
			// link back
			if (!aBottom->neighbours[spiTop])
				aBottom->neighbours[spiTop] = this;
		}
		if (aRight)
		{
			neighbours[spiRight] = aRight;
			// link back
			if (!aRight->neighbours[spiLeft])
				aRight->neighbours[spiLeft] = this;
		}
		if (aTop)
		{
			neighbours[spiTop] = aTop;
			// link back
			if (!aTop->neighbours[spiBottom])
				aTop->neighbours[spiBottom] = this;
		}
		if (aLeft)
		{
			neighbours[spiLeft] = aLeft;
			// link back
			if (!aLeft->neighbours[spiRight])
				aLeft->neighbours[spiRight] = this;
		}
	}
	void activateElement(Config& aConfig, bool aFull)
	{
		if (element)
			element->activate(aConfig, aFull);
	}
	void deactivateElement(Config& aConfig)
	{
		if (element)
			element->deactivate(aConfig);
	}
	void activateNeighbour(Config& aConfig, int aNeighbour, bool aFull)
	{
		auto neighbour = neighbours[aNeighbour];
		if (neighbour)
			neighbour->activateElement(aConfig, aFull);
	}
	void activateNeighbours(Config& aConfig, bool aFull)
	{
		activateNeighbour(aConfig, spiLeft, aFull);
		activateNeighbour(aConfig, spiRight, aFull);
		activateNeighbour(aConfig, spiTop, aFull);
		activateNeighbour(aConfig, spiBottom, aFull);
	}
	void deactivateNeighbour(Config& aConfig, int aNeighbour, int aExcept = spiNone)
	{
		if (aNeighbour != aExcept)
		{
			auto neighbour = neighbours[aNeighbour];
			if (neighbour)
				neighbour->deactivateElement(aConfig);
		}
	}
	void deactivateNeighbours(Config& aConfig, int aExcept = spiNone)
	{
		deactivateNeighbour(aConfig, spiLeft, aExcept);
		deactivateNeighbour(aConfig, spiRight, aExcept);
		deactivateNeighbour(aConfig, spiTop, aExcept);
		deactivateNeighbour(aConfig, spiBottom, aExcept);
	}
};


class SlidingPanel : public Element
{
public:
	SlidingPanel(const SDL_Rect& aPosition)
		: Element(aPosition, true, true)
	{
	}
public:
	std::vector<std::shared_ptr<SlidingPanelPage>> pages;
	SlidingPanelPage* activePage = nullptr;
	SlidingPanelPage* addPage(std::shared_ptr<Element> aElement)
	{
		auto newPage = std::make_shared<SlidingPanelPage>(aElement);
		pages.push_back(newPage);
		aElement->parent = this;
		return newPage.get();
	}
public:
	std::function<void(SlidingPanel*/*, int sliding orientation*/)> onSlideStart;
	std::function<void(bool aPageStillActive)> onSlideEnd;
	virtual std::string elementName() override { return "SlidingPanel"; }
	virtual bool renderThis(Config& aConfig, int x, int y) override 
	{ 
		if (activePage)
		{
			if (fMoving)
			{
				// draw all neighbours that are in view
				int posElement;
				SlidingPanelPage* slidingPage;
				posElement = fDisplayOffset.y >= 0 ? spiTop : spiBottom;
				slidingPage = activePage->neighbours[posElement];
				if (slidingPage)
				{
					std::shared_ptr<Element> slidingElement = slidingPage->element;
					if (slidingElement)
					{
						if (posElement == spiTop)
							slidingElement->render(aConfig, x/* + fDisplayOffset.x*/, y + fDisplayOffset.y - aConfig.screenSize.y);
						else
							slidingElement->render(aConfig, x/* + fDisplayOffset.x*/, y + fDisplayOffset.y + aConfig.screenSize.y);
					}
				}
				posElement = fDisplayOffset.x >= 0 ? spiLeft : spiRight;
				slidingPage = activePage->neighbours[posElement];
				if (slidingPage)
				{
					std::shared_ptr<Element> slidingElement = slidingPage->element;
					if (slidingElement)
					{
						if (posElement == spiLeft)
							slidingElement->render(aConfig, x + fDisplayOffset.x - aConfig.screenSize.x, y); // +fDisplayOffset.y);
						else
							slidingElement->render(aConfig, x + fDisplayOffset.x + aConfig.screenSize.x, y); // +fDisplayOffset.y);
					}
				}
			}
			// draw active panel on top
			activePage->element->render(aConfig, x + fDisplayOffset.x, y + fDisplayOffset.y);
			return true;
		}
		else
			return false;
	}
	virtual Element* findElementRev(int x, int y, bool aOnlyVisible, bool aOnlyEnabled) override
	{
		Element* res = nullptr;
		// pages are relative
		if (activePage)
			res = activePage->element->findElementRev(x - fDisplayOffset.x, y - fDisplayOffset.y, aOnlyVisible, aOnlyEnabled);
		// children are absolute
		if (!res)
			res = Element::findElementRev(x, y, aOnlyVisible, aOnlyEnabled);
		return res;
	}
	int virtual onTouchMoveStart(TouchState& aTouchState, Config& aConfig) override
	{
		if (activePage)
		{
			activePage->activateNeighbours(aConfig, false);
			fMoving = true;
		}
		if (onSlideStart)
			onSlideStart(this);
		std::cout << "sliding page start " << fDisplayOffset.x << " - " << fDisplayOffset.y << std::endl;
		return RERENDER_ALL_CLEAR_FIRST;
	}
	int virtual onTouchMove(TouchState& aTouchState, Config& aConfig) override
	{
		if (fMoving)
		{
			fDisplayOffset.x = aTouchState.currentX - aTouchState.touchDownX;
			fDisplayOffset.y = aTouchState.currentY - aTouchState.touchDownY;
			std::cout << "sliding page move " << fDisplayOffset.x << " - " << fDisplayOffset.y << std::endl;
		}
		return RERENDER_ALL_CLEAR_FIRST;
	}
	int virtual onTouchMoveEnd(TouchState& aTouchState, Config& aConfig) override
	{
		if (activePage)
		{
			// determine specific direction
			int direction = getDirection(position.w / 5);
			fDisplayOffset.x = 0;
			fDisplayOffset.y = 0;
			fMoving = false;
			// determine new active panel
			SlidingPanelPage* newPanel = direction != spiNone ? activePage->neighbours[direction] : nullptr;
			if (newPanel)
			{
				// record path how we came to the new panel, for sliding back
				auto oppositeDirection = SlidingPanelPage::oppositePosition(direction);
				newPanel->neighbours[oppositeDirection] = activePage;
				// deactivate pages
				activePage->deactivateElement(aConfig);
				activePage->deactivateNeighbour(aConfig, spiTop, direction);
				activePage->deactivateNeighbour(aConfig, spiBottom, direction);
				activePage->deactivateNeighbour(aConfig, spiLeft, direction);
				activePage->deactivateNeighbour(aConfig, spiRight, direction);
				
				// switch to new panel, which is already active
				activePage = newPanel;
				activePage->activateElement(aConfig, true);
				
				// todo: is this is the place to do this and shouldn't we also save the config to recover from a crash?
				// set active page clock type in config
				if (activePage && activePage->element)
					aConfig.clockType = activePage->element->clockType();
			}
			if (onSlideEnd)
				onSlideEnd(newPanel);
			std::cout << "sliding page end" << std::endl;
		}
		return RERENDER_ALL_CLEAR_FIRST;
	}
	int getDirection(int aMinimalMovement)
	{
		int res = spiNone;
		if (std::abs(fDisplayOffset.x) >= std::abs(fDisplayOffset.y))
		{
			if (std::abs(fDisplayOffset.x) >= aMinimalMovement) // position.w / 5)
				res = fDisplayOffset.x > 0 ? spiLeft : spiRight;
		}
		else
		{
			if (std::abs(fDisplayOffset.y) >= aMinimalMovement) // position.w / 5)
				res = fDisplayOffset.y > 0 ? spiTop : spiBottom;
		}
		return res;
	}
	virtual int handleInit(Config& aConfig) override
	{
		auto res = Element::handleInit(aConfig);
		for (std::shared_ptr<SlidingPanelPage> page : pages)
		{
			if (page->element)
			{
				auto resPage = page->element->handleInit(aConfig);
				if (res < resPage)
					res = resPage;
			}
		}
		return res;
	}
	virtual void handleExit(Config& aConfig) override
	{
		Element::handleExit(aConfig);
		for (std::shared_ptr<SlidingPanelPage> page : pages)
		{
			if (page->element)
				page->element->handleExit(aConfig);
		}
	}
	virtual void activate(Config& aConfig, bool aFull) override
	{
		if (activePage)
			activePage->activateElement(aConfig, aFull);
		Element::activate(aConfig, aFull);
	}
	virtual void deactivate(Config& aConfig) override
	{
		if (activePage)
			activePage->deactivateElement(aConfig);
		Element::deactivate(aConfig);
	}
protected:
	SDL_Point fDisplayOffset{ 0,0 };
	bool fMoving = false;
};
