// element to show an alarm time in a schedule row

#pragma once

#include <sstream>
#include <iomanip>

#include "../sdl_eventloop.h"
#include "images.h"

#define colorTimeSelectorFlipBackground 0x60101010
#define colorTimeSelectorFlipSplitLine 0x80202020
#define colorTimeSelectorFlipText 0x80C0C0C0


class TimeSelector : public MultiImage
{
public:
	TimeSelector(fs::path aImagePath, int aHour, int aMinutes, SDL_Rect aPosition)
		: MultiImage(aImagePath, { "timeselector-enabled.png","timeselector-disabled.png" }, aPosition) 
	{
		hour = aHour;
		minutes = aMinutes;
		onTouchDownLong = [this](TouchState& aTouchState, Config& aConfig) -> int
		{
			if (onLongTouch)
				return onLongTouch(this, aConfig);
			else
				return RERENDER_NONE;
		};
	}
public:
	virtual std::string elementName() override { return "TimeSelector"; }
	int hour = 0;
	int minutes = 0;
	std::function<int(TimeSelector* aTimeSelector, Config& aConfig)> onShortTouch;
	std::function<int(TimeSelector* aTimeSelector, Config& aConfig)> onLongTouch;
	bool isActive() { return activeImage == 0; }
	bool setActive(bool aActive) 
	{ 
		if (isActive() ^ aActive)
		{
			activeImage = aActive ? 0 : 1;
			return true;
		}
		else
			return false;
	}
	int virtual onTouchUp(TouchState& aTouchState, Config& aConfig) override
	{
		if (onShortTouch)
			return onShortTouch(this, aConfig);
		else
			return RERENDER_NONE;
	}
public:
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			MultiImage::renderThis(aConfig, x, y);
			auto rect = relativeDisplayPosition(x, y);

			if (!fFont)
				fFont = aConfig.renderer->getFont(int(position.h / 1.2), "seguisb.ttf");

			auto rectHour = rect;
			rectHour.w /= 3;
			rectHour.h = int(rectHour.h / 1.4);
			rectHour.x += int(rectHour.w / 2.5);

			rectHour = rectCenterY(rect, rectHour);

			auto rectMinutes = rectHour;
			rectMinutes.x = rect.x + rect.w - rectMinutes.w - (rectHour.x - rect.x);

			int line_width = std::max(rect.h / 40, 1);
			int rr_radius = std::max(rect.h / 10, 2);
			aConfig.renderer->drawRoundedRectFilled(rectHour, rr_radius, colorTimeSelectorFlipBackground);
			aConfig.renderer->drawRoundedRectFilled(rectMinutes, rr_radius, colorTimeSelectorFlipBackground);

			renderFlipDigits(aConfig.renderer, rectHour, std::to_string(hour));

			std::stringstream ss;
			ss << std::setw(2) << std::setfill('0') << minutes;
			renderFlipDigits(aConfig.renderer, rectMinutes, ss.str());

			renderSplitLine(aConfig.renderer, rectHour, line_width);
			renderSplitLine(aConfig.renderer, rectMinutes, line_width);

			return true;
		}
		else
			return false;
	}
	void renderFlipDigits(SDLRenderer* aRenderer, const SDL_Rect& aRect, const std::string& aText)
	{
		SDL_Rect renderQuad{ aRect.x,aRect.y,0,0 };
		int w, h;
		TTF_SizeText(fFont, aText.c_str(), &w, &h);
		renderQuad.w = w;
		renderQuad.h = h;
		renderQuad = rectCenter(aRect, renderQuad);
		renderQuad = rectOffset(renderQuad, 0, int(-renderQuad.h * 0.07)); // adjust text
		aRenderer->drawText(aText, fFont, colorTimeSelectorFlipText, &renderQuad);
	}
	void renderSplitLine(SDLRenderer* aRenderer, const SDL_Rect& aRect, int aLineWidth)
	{
		// render horizontal line as cut-out
		int y1 = aRect.y + aRect.h / 2 - aLineWidth / 2;
		int y2 = y1 + aLineWidth;
		for (int y = y1; y < y2; y++)
			aRenderer->drawHLineColor(aRect.x, aRect.x + aRect.w, y, colorTimeSelectorFlipSplitLine);
	}
protected:
	TTF_Font* fFont = nullptr;
};
