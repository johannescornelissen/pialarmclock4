// button with a day of the week, enabled/disabled and third state, orb above it when selected and +x to signal alarm is postponed for the number of weeks
// used in schedule row

#pragma once

#include "images.h"


class DaySelectorOrb : public MultiImage
{
public:
	DaySelectorOrb(fs::path aImagePath, SDL_Rect aPosition)
		: MultiImage(aImagePath, { "orb-green.png","orb-red.png" }, aPosition)
	{
		visible = false; // default not visible
	}
public:
	virtual std::string elementName() override { return "DaySelectOrb"; }
	void setInvisble() { visible = false; }
	void setGreen() { visible = true; activeImage = 0; }
	void setRed() { visible = true; activeImage = 1; }
};

class DaySelector : public MultiImage
{
public:
	DaySelector(fs::path aImagePath, bool aActive, int aDelay, std::string aText, SDL_Rect aPosition, Config &aConfig) 
		: MultiImage(aImagePath, 
			{
				(aConfig.bigScreen() ? "button-enabled.png" : "button-enabled-48.png"), 
				(aConfig.bigScreen() ? "button-disabled.png" : "button-disabled-48.png"),
				(aConfig.bigScreen() ? "button-postponed.png" : "button-postponed-48.png")
			}, aPosition)
	{
		fText = aText;
		setActive(aActive);
		delay = aDelay;
		onTouchDownLong = [this](TouchState& aTouchState, Config& aConfig)->int 
		{
			setActive(true);
			addDelay();
			if (onChanged)
				return onChanged(this, aConfig);
			else
				return RERENDER_ALL;
		};
	}
public:
	virtual std::string elementName() override { return "DaySelector"; }
	std::function<int(DaySelector* aDaySelector, Config& aConfig)> onChanged;
	int delay = 0;
	bool isActive() { return activeImage == 0; }
	void setActive(bool aActive) { activeImage = aActive ? 0 : 1; }
	void setDelay(int aValue) { delay = aValue; }
	void resetDelay() { delay = 0; }
	void addDelay() { delay++; }
	int virtual onTouchUp(TouchState& aTouchState, Config& aConfig) override
	{ 
		setActive(!isActive());
		resetDelay(); 
		if (onChanged)
			return onChanged(this, aConfig);
		else
			return RERENDER_ALL;
	}
public:
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			MultiImage::renderThis(aConfig, x, y);
			auto rect = relativeDisplayPosition(x, y);
			// day-of-week
			if (!fDowFont)
				fDowFont = aConfig.renderer->getFont(int(position.h / 2), "arlrdbd.ttf");
			auto textColor = aConfig.renderer->rgbaToColor(0xff, 0xff, 0xff, activeImage ? 0x40 : 0x80);
			auto dowRect = aConfig.renderer->textRect(fText, fDowFont, rect.x,rect.y);
			dowRect = rectCenter(rect, dowRect);
			aConfig.renderer->drawText(fText, fDowFont, textColor, &dowRect);
			// delay
			if (delay > 0)
			{
				auto delayText = "+" + std::to_string(delay);
				if (!fDelayFont)
					fDelayFont = aConfig.renderer->getFont(int(position.h / 5), "segoeuib.ttf");
				
				auto delayRect = aConfig.renderer->textRect(fText, fDowFont, rect.x, rect.y);
				delayRect = rectMoveSize(rectBottom(rect, rectRight(rect, delayRect)), -1, -0.2);
				aConfig.renderer->drawText(delayText, fDelayFont, textColor, &delayRect);
			}
			return true;
		}
		else
			return false;
	}
protected:
	std::string fText;
	TTF_Font* fDowFont = nullptr;
	TTF_Font* fDelayFont = nullptr;
};
