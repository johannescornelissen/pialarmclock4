#pragma once

#include "clocks.h"

#define ABGR_PETROL 0xff6a5f00


class ClockJustNumbers : public Clock
{
public:
	ClockJustNumbers(fs::path aImagePath, SDL_Rect aPosition, int aClockType)
		: Clock(aImagePath, aPosition, aClockType)
	{
	}
public:
	virtual std::string elementName() override { return "ClickJustNumbers"; }
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			Clock::renderThis(aConfig, x, y);

			auto rect = relativeDisplayPosition(x, y);

			int hour, minutes;
			LocalDateTime::timeParts(aConfig.currentTime, &hour, &minutes);

			// date
			drawCurrentDate(aConfig, x, y, ABGR_PETROL);

			// hour+minutes
			std::stringstream ss;
			ss << std::setfill('0') << hour << ":" << std::setw(2) << minutes;
			auto time = ss.str();
			if (!fFontTime)
				fFontTime = aConfig.renderer->getFont(rect.h / 2, "segoeuisl.ttf");

			auto timeRect = aConfig.renderer->textRect(time, fFontTime, rect.x, rect.y);
			timeRect = rectCenter(rect, timeRect);
			aConfig.renderer->drawText(time, fFontTime, ABGR_PETROL, &timeRect);

			// next alarm
			drawAlarmState(aConfig, x, y, ABGR_PETROL);

			// make sure we are active
			activate(aConfig, false);

			return true;
		}
		else
			return false;
	}
protected:
	TTF_Font* fFontTime = nullptr;
};



