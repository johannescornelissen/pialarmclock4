#pragma once

#include "clocks_analogdial.h"
#include "clocks_chimneys.h"
#include "clocks_effectsandriscurves.h"
#include "clocks_flip.h"
#include "clocks_justnumbers.h"
#include "clocks_mickey.h"
#include "clocks_tree.h"


// 0 = config page
#define CT_Chimneys			1
#define CT_Tree				2
#define CT_EffectsAndRC		3
#define CT_JUST_NUMBERS		4
#define CT_FLIP				5
#define CT_ANALOG			6
#define CT_MICKEY			7


inline std::shared_ptr<Clock> createClock(fs::path aImagePath, const SDL_Rect aRect, int aClockType, Config& aConfig)
{
	switch (aClockType)
	{
	case CT_Chimneys:			return std::make_shared<ClockChimneys>(aImagePath / (aConfig.bigScreen() ? "chimneys.large" : "chimneys.small"), aRect, aConfig, aClockType);
	case CT_EffectsAndRC:		return std::make_shared<ClockEffectsAndRC>(aImagePath / "images", aRect, aClockType);
	case CT_Tree:				return std::make_shared<ClockTree>(aImagePath / (aConfig.bigScreen() ? "tree.large" : "tree.small"), aRect, aConfig, aClockType);
	case CT_ANALOG:				return std::make_shared<ClockAnalogDial>(aImagePath / "images", aRect, aClockType);
	case CT_FLIP:				return std::make_shared<ClockFlip>(aImagePath / "images", aRect, aClockType);
	case CT_MICKEY:				return std::make_shared<ClockMickey>(aImagePath / "mickey2", aRect, aClockType);
		// case CT_JUST_NUMBERS:
	default:					return std::make_shared<ClockJustNumbers>(aImagePath / "images", aRect, aClockType);
	}
}

inline std::shared_ptr<Clock> createClock(fs::path aImagePath, const SDL_Rect aRect, Config& aConfig)
{
	return createClock(aImagePath, aRect, aConfig.clockType, aConfig);
}

inline std::shared_ptr<Panel> createAllClockPages(std::shared_ptr<SlidingPanel> aSlidingPanel, const std::string aPathResources, Config& aConfig)
{
	// create generic panel for config elements, this will be returned from this function
	auto configPanel = std::make_shared<Panel>(aSlidingPanel->position);

	// create page per clock type
	std::vector<SlidingPanelPage*> clockPages;
	clockPages.push_back(aSlidingPanel->addPage(configPanel));
	clockPages.push_back(aSlidingPanel->addPage(createClock(fs::path(aPathResources), aSlidingPanel->position, CT_Chimneys, aConfig)));
	clockPages.push_back(aSlidingPanel->addPage(createClock(fs::path(aPathResources), aSlidingPanel->position, CT_Tree, aConfig)));
	clockPages.push_back(aSlidingPanel->addPage(createClock(fs::path(aPathResources), aSlidingPanel->position, CT_EffectsAndRC, aConfig)));
	clockPages.push_back(aSlidingPanel->addPage(createClock(fs::path(aPathResources), aSlidingPanel->position, CT_ANALOG, aConfig)));
	clockPages.push_back(aSlidingPanel->addPage(createClock(fs::path(aPathResources), aSlidingPanel->position, CT_FLIP, aConfig)));
	clockPages.push_back(aSlidingPanel->addPage(createClock(fs::path(aPathResources), aSlidingPanel->position, CT_JUST_NUMBERS, aConfig)));
	clockPages.push_back(aSlidingPanel->addPage(createClock(fs::path(aPathResources), aSlidingPanel->position, CT_MICKEY, aConfig)));

	// link all clock pages
	if (clockPages.size() > 1)
	{
		for (auto p = 1; p < clockPages.size() - 1; p++)
			clockPages[p]->setNeighbours(clockPages[size_t(p) + 1], clockPages[0]);
		clockPages[clockPages.size() - 1]->setNeighbours(clockPages[1], clockPages[0]);
	}

	aConfig.clockType = 7; // todo: for debugging, setting clock type directly

	if (clockPages.size() > 0)
	{
		// active page
		if (0 <= aConfig.clockType && aConfig.clockType < clockPages.size())
			aSlidingPanel->activePage = clockPages[aConfig.clockType];
		else
			aSlidingPanel->activePage = clockPages[0];

		if (aSlidingPanel->activePage != clockPages[0])
			clockPages[0]->setNeighbour(spiLeft, aSlidingPanel->activePage);
		else
			clockPages[0]->setNeighbour(spiLeft, clockPages[1]);
	}

	// return the generated generic config panel
	return configPanel;
}
