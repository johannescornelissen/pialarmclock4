#pragma once

#include "clocks.h"


class ClockFlip : public Clock
{
public:
	ClockFlip(fs::path aImagePath, SDL_Rect aPosition, int aClockType)
		: Clock(aImagePath, aPosition, aClockType)
	{
		fColorCipherText = 0xff909090;
		fColorCipherBackground = 0xff202020;
		fColorBackground = 0xff000000;
	}
public:
	virtual std::string elementName() override { return "ClockFlip"; }
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			Clock::renderThis(aConfig, x, y);

			auto rect = relativeDisplayPosition(x, y);

			int hour, minutes;
			LocalDateTime::timeParts(aConfig.currentTime, &hour, &minutes);

			// date
			drawCurrentDate(aConfig, x, y);

			// hour+minutes
			{
				auto hourRect = rect;
				hourRect.x = rect.x + rect.w / 20;
				hourRect.w = int(hourRect.w / 2.4);
				hourRect.h = int(hourRect.h / 1.8);
				hourRect = rectCenterY(rect, hourRect);
				renderCipher(hourRect, std::to_string(hour), aConfig);

				auto minutesRect = hourRect;
				minutesRect.x = rect.x + rect.w - hourRect.w - rect.w / 20;
				auto minutesText = std::to_string(minutes);
				while (minutesText.size() < 2)
					minutesText = '0' + minutesText;
				renderCipher(minutesRect, minutesText, aConfig);
			}

			// next alarm
			drawAlarmState(aConfig, x, y);

			// make sure we are active
			activate(aConfig, false);

			return true;
		}
		else
			return false;
	}
protected:
	void renderCipher(const SDL_Rect& rect, const std::string& aText, Config& aConfig)
	{
		if (!fFontTime)
		{
			if (aConfig.bigScreen())
				fFontTime = aConfig.renderer->getFont(int(position.h / 1.6), "seguisb.ttf");
			else
				fFontTime = aConfig.renderer->getFont(int(position.h / 2.0), "seguisb.ttf");
		}

		int line_width = std::max(rect.h / 50, 1);
		int rr_radius = std::max(rect.h / 10, 2);
		aConfig.renderer->drawRoundedRectFilled(rect, rr_radius, fColorCipherBackground);

		SDL_Rect renderQuad{ rect.x,rect.y,0,0 };
		int w, h;
		TTF_SizeText(fFontTime, aText.c_str(), &w, &h);
		renderQuad.w = w;
		renderQuad.h = h;
		renderQuad = rectCenter(rect, renderQuad);
		if (aConfig.bigScreen())
			renderQuad = rectOffset(renderQuad, 0, int(-renderQuad.h * 0.07)); // adjust text
		else
			renderQuad = rectOffset(renderQuad, 0, int(-renderQuad.h * 0.08)); // adjust text

		renderQuad = aConfig.renderer->textRect(aText, fFontTime, renderQuad.x, renderQuad.y);
		renderQuad.y += renderQuad.h / 30; // adjust vertical position of text a little bit for flip-breaking-line to fit
		aConfig.renderer->drawText(aText, fFontTime, fColorCipherText, &renderQuad);

		// render horizontal line as cut-out
		int y1 = rect.y + rect.h / 2 - line_width / 2;
		int y2 = y1 + line_width;
		for (int y = y1; y < y2; y++)
			//hlineColor(aConfig.renderer->getRenderer(), (Sint16)rect.x, (Sint16)(rect.x + rect.w), (Sint16)y, fColorBackground);
			aConfig.renderer->drawHLineColor(rect.x, rect.x + rect.w, y, fColorBackground);
	}
protected:
	TTF_Font* fFontTime = nullptr;
	Uint32 fColorCipherText;
	Uint32 fColorCipherBackground;
	Uint32 fColorBackground;
};


