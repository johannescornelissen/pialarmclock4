#pragma once

#include "clocks.h"
#include "clocks_analogdial.h"


class ClockEffectsAndRC : public ClockAnalogDial
{
public:
	ClockEffectsAndRC(fs::path aImagePath, SDL_Rect aPosition, int aClockType)
		: ClockAnalogDial(aImagePath, aPosition, aClockType)
	{
		fDrawBackground = false;
		fShowCurrentDate = false;
		fColor = 0xffffffff;
	}
public:
	virtual std::string elementName() override { return "ClockEffectsAndRC"; }
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			if (!fBackground)
				fBackground = aConfig.renderer->loadTexture(fImagePath / "effects_splash.png");
			auto rect = relativeDisplayPosition(x, y);
			aConfig.renderer->drawTexture(fBackground, &rect);
			ClockAnalogDial::renderThis(aConfig, x, y);
			return true;
		}
		else
			return false;
	}
protected:
	SDL_Texture* fBackground = nullptr;
};


