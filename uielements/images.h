// generic image

#pragma once

#include "../sdl_eventloop.h"


class Image : public Element
{
public:
	Image(fs::path aImagePath, SDL_Rect aPosition) 
		: Element(aPosition, true, true)
	{
		fImagePath = aImagePath;
	}
public:
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			auto rect = relativeDisplayPosition(x, y);
			if (!fImageTexture)
				fImageTexture = aConfig.renderer->loadTexture(fImagePath.string());
			aConfig.renderer->drawTexture(fImageTexture, &rect);
			return true;
		}
		else
			return false;
	}
protected:
	fs::path fImagePath;
	SDL_Texture* fImageTexture = nullptr;
};

class MultiImage : public Element
{
public:
	MultiImage(fs::path aImagePath, std::vector<std::string> aImageNames, SDL_Rect aPosition)
		: Element(aPosition, true, true)
	{
		fImagePath = aImagePath;
		fImageNames = aImageNames;
	}
public:
	int activeImage = 0;
	int imageCount() { return (int)fImageNames.size(); }
public:
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false) && activeImage >= 0)
		{
			auto rect = relativeDisplayPosition(x, y);
			auto texture = getOrLoadTexture(aConfig.renderer, activeImage);
			aConfig.renderer->drawTextureScale(texture, &rect);
			return true;
		}
		else
			return false;
	}
protected:
	SDL_Texture* getOrLoadTexture(SDLRenderer* aRenderer, int aIndex)
	{
		if (aIndex >= int(fImageTextures.size()) || !fImageTextures[aIndex])
		{
			while (aIndex >= int(fImageTextures.size()))
				fImageTextures.push_back(nullptr);
			fImageTextures[aIndex] = aRenderer->loadTexture(fImagePath / fImageNames[aIndex]);
		}
		return fImageTextures[aIndex];
	}
protected:
	fs::path fImagePath;
	std::vector<std::string> fImageNames;
	std::vector<SDL_Texture*> fImageTextures;
};
