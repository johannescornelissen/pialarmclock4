#pragma once

#include "clocks.h"


class ClockAnalogDial : public Clock
{
public:
	ClockAnalogDial(fs::path aImagePath, SDL_Rect aPosition, int aClockType)
		: Clock(aImagePath, aPosition, aClockType)
	{
		fTimerInterval = 10000;
	}
public:
	virtual std::string elementName() override { return "ClockAnalogDial"; }
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		Clock::renderThis(aConfig, x, y);

		if (checkState(true, false))
		{
			auto rect = relativeDisplayPosition(x, y);

			drawCurrentDate(aConfig, x, y);

			double angle;
			auto center = centerOfRect(rect);

			// dial minutes
			for (int minute = 0; minute < 60; minute++)
			{
				calculateArmAngles(0, minute, 0, nullptr, &angle, nullptr);
				auto p1 = time_location(center, angle, int(std::round(0.425 * rect.h)));
				auto p2 = time_location(center, angle, int(std::round(0.450 * rect.h)));
				aConfig.renderer->drawAALine(p1.x, p1.y, p2.x, p2.y, fColor);
			}
			// dial hours
			for (int hour = 1; hour <= 12; hour++)
			{
				for (int i = -4; i <= 4; i++)
				{
					if (i != 0)
					{
						calculateArmAngles(hour, 0, i * 10, &angle, nullptr, nullptr);
						auto p1 = time_location(center, angle, int(std::round(0.40 * rect.h)));
						auto p2 = time_location(center, angle, int(std::round(0.45 * rect.h)));
						aConfig.renderer->drawAALine(p1.x, p1.y, p2.x, p2.y, fColor);
					}
				}
				if (!fFontTime)
					fFontTime = aConfig.renderer->getFont(rect.h / 17, "segoeuisl.ttf" /*"seguisb.ttf"*/);
				calculateArmAngles(hour, 0, 0, &angle, nullptr, nullptr);
				auto p3 = time_location(center, angle, int(std::round(0.35 * rect.h)));
				auto text = std::to_string(hour);

				auto textRect = aConfig.renderer->textRect(text, fFontTime, p3.x, p3.y);
				textRect = rectOffset(textRect, -textRect.w / 2, -textRect.h / 2);
				aConfig.renderer->drawText(text, fFontTime, 0xffffffff, &textRect);
			}

			// arms
			int hour, minutes, seconds;
			LocalDateTime::timeParts(aConfig.currentTime, &hour, &minutes, &seconds);
			double hourArmAngle, minutesArmAngle;
			calculateArmAngles(hour, minutes, seconds, &hourArmAngle, &minutesArmAngle, nullptr);
			{
				// hour arm
				auto p1 = time_location(center, hourArmAngle - 5.0 / 8.0 * pi, int(std::round(0.01 * rect.h)));
				auto p2 = time_location(center, hourArmAngle, int(std::round(0.27 * rect.h)));
				auto p3 = time_location(center, hourArmAngle + 5.0 / 8.0 * pi, int(std::round(0.01 * rect.h)));
				aConfig.renderer->drawAAFilledTrigon(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, fColor);
			}
			{
				// minutes arm
				auto p1 = time_location(center, minutesArmAngle - 6.0 / 8.0 * pi, int(std::round(0.015 * rect.h)));
				auto p2 = time_location(center, minutesArmAngle, int(std::round(0.43 * rect.h)));
				auto p3 = time_location(center, minutesArmAngle + 6.0 / 8.0 * pi, int(std::round(0.015 * rect.h)));
				aConfig.renderer->drawAAFilledTrigon(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, fColor);
			}

			// next alarm
			drawAlarmState(aConfig, x, y);

			// make sure we are active
			activate(aConfig, false);

			return true;
		}
		else
			return false;
	}
protected:
	TTF_Font* fFontTime = nullptr;
	Uint32 fColor = 0xaaffffff;
};
