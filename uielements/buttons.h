// gereric button, with text and/or icon

#pragma once

#include "../sdl_eventloop.h"
#include "images.h"


class Button : public MultiImage
{
public:
	Button(fs::path aImagePath, std::vector<std::string> aImageNames, int aActiveIconImage, const std::string& aText, SDL_Rect aPosition)
		: MultiImage(aImagePath, aImageNames, aPosition)
	{
		activeIconImage = aActiveIconImage;
		fText = aText;
	}
public:
	virtual std::string elementName() override { return "Button"; }
	int activeIconImage = 1;
	std::function<int(Config& aConfig)> onClick;

	int virtual onTouchUp(TouchState& aTouchState, Config& aConfig) override
	{
		return onClick ? onClick(aConfig) : RERENDER_NONE;
	}

	virtual bool renderThis(Config& aConfig, int x, int y) override
	{
		if (checkState(true, false))
		{
			MultiImage::renderThis(aConfig, x, y);
			auto rect = relativeDisplayPosition(x, y);
			// icon
			if (activeIconImage >= 0)
			{
				auto icon = getOrLoadTexture(aConfig.renderer, activeIconImage);
				auto iconRect = aConfig.renderer->textureRect(icon);
				iconRect = rectCenter(rect, iconRect);
				aConfig.renderer->drawTexture(icon, &iconRect);
			}
			// text
			if (!fText.empty())
			{
				if (!fFont)
					fFont = aConfig.renderer->getFont(int(position.h / 2), "arlrdbd.ttf");
				auto dowRect = aConfig.renderer->textRect(fText, fFont, rect.x, rect.y);
				dowRect = rectCenter(rect, dowRect);
				aConfig.renderer->drawText(fText, fFont, 0x80ffffff, &dowRect);
			}
			return true;
		}
		else
			return false;
	}
protected:
	std::string fText;
	TTF_Font* fFont = nullptr;
};
