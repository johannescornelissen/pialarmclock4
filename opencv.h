#pragma once

#include <filesystem>
namespace fs = std::filesystem;

#include <opencv2/core.hpp>	
#include <opencv2/core/types_c.h>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "sdl_init.h"

class OpenCVAnimation
{
public:
	OpenCVAnimation() {}
	OpenCVAnimation(fs::path aPath)
	{
		fVideo = cv::VideoCapture(aPath.string());
		fIsOpen = fVideo.isOpened();
		if (fIsOpen)
			fFrameCount = size_t(fVideo.get(cv::CAP_PROP_FRAME_COUNT));
		else
			fFrameCount = 0;
	}
public:
	size_t getFrameCount() { return fFrameCount; }
	size_t getIsOpen() { return fIsOpen; }
	void gotoFrameNr(size_t aFrame)
	{
		fVideo.set(cv::CAP_PROP_POS_FRAMES, double(aFrame));
	}
	size_t getFrameNr()
	{
		return size_t(fVideo.get(cv::CAP_PROP_POS_FRAMES));
	}
	cv::Mat* getFrame()
	{
		fVideo >> fFrame;
		return &fFrame;
	}
	cv::Mat* getResizedFrame(int aWidth, int aHeight)
	{
		cv::resize(fFrame, fResizedFrame, cv::Size(aWidth, aHeight), 0, 0, cv::INTER_CUBIC);
		return &fResizedFrame;
	}
	SDL_Surface* createFrameSurface(int aWidth, int aHeight)
	{
		getFrame();
		if (fFrame.data)
		{
			getResizedFrame(aWidth, aHeight);
			int d = 0;
			switch (fResizedFrame.depth())
			{
			case CV_8U: d = 8; break;
			case CV_8S: d = 8; break;
			case CV_16U: d = 8; break;
			case CV_16S: d = 16; break;
			case CV_32S: d = 32; break;
			case CV_32F: d = 32; break;
			case CV_64F: d = 64; break;
			case CV_16F: d = 16; break;
			}
			auto surface = SDL_CreateRGBSurfaceFrom(fResizedFrame.data, aWidth, aHeight, d * fResizedFrame.channels(), int(fResizedFrame.step), 0xff0000, 0x00ff00, 0x0000ff, 0);
			return surface;
		}
		else
			return nullptr;
	}
protected:
	cv::VideoCapture fVideo;
	bool fIsOpen = false;
	size_t fFrameCount = 0;
	cv::Mat fFrame;
	cv::Mat fResizedFrame;
};