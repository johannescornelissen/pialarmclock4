#include <iostream>
#include <vector>
#include <string>
#include <functional>
#include <fstream>
#include <csignal>
#include <thread>
#include <ctime>

#define SMALL_SCREEN
//#define BIG_MICKEY

#ifdef SDL1
#include "sdl_renderer1.h"
#include "tslibevents.h" // use tslib with own handler, not using TSLIB from SDL
#else
#include "sdl_renderer2.h"
#endif
#include "sdl_eventloop.h"

#include "uielements/buttons.h"
#include "uielements/images.h"
#include "uielements/dayselectors.h"
#include "uielements/timeselectors.h"
#include "uielements/timeeditors.h"
#include "uielements/schedules.h"
#include "uielements/texts.h"
#include "uielements/audio.h"
#include "uielements/slidingpanels.h"
#include "uielements/clocks_analogdialbutton.h"

#include "uielements/clocks_all.h"


#ifdef __linux__
#include <tslib.h>
#endif

// TTF uses SDL_Color (struct, use colorToSDLColor)
// GFX and rest of SDL uses 0xAABBGGRR

#define colorScreenBackground 0xFF000000
#define colorFlipBackground 0xFF404040 // 0xFF202020 // 0xFF101010
#define colorFlipCipher 0xFF909090 // 0xFFC8C8C8
#define colorFlipCipherLight 0xFFCCCCCC

#define colorHourDotOverlay 0xFF44FF00
#define colorMinuteDotOverlay 0x00DDDDDD // transparent for now
#define colorSecondDotOverlay 0xFFFF4400

#define BM_AUTO 0
#define BM_ALWAYS_ON 1
#define BM_ALWAYS_OFF 2
#define BM_NOTHING 3

#define SENSOR_LOOP_MS 200.0
#define PID_LUX_MAX_DELTA 10.0

#define DEFAULT_SENSOR_LUX_MAX 50.0
#define DEFAULT_SENSOR_LUX_MIN 1.0
#define DEFAULT_BACKLIGHT_MAX 255
#define DEFAULT_BACKLIGHT_MIN 10


double SENSOR_LUX_MAX = DEFAULT_SENSOR_LUX_MAX;
double SENSOR_LUX_MIN = DEFAULT_SENSOR_LUX_MIN;
int BACKLIGHT_MAX = DEFAULT_BACKLIGHT_MAX;
int BACKLIGHT_MIN = DEFAULT_BACKLIGHT_MIN;

int WATCH_FACE = 0;

#ifdef __linux__
#define pathResources "../../../"
#else
#ifndef NDEBUG
// debug
#define pathResources "."
#define SHOW_PID_ON_CONSOLE
#else
// release
//#define pathResources ""
//#define pathResources "../../../"
#define pathResources "."
#endif
#endif

Config config;

int main(int argc, char* args[])
{

#ifdef __linux__

#ifdef SDL1
	setenv("SDL_VIDEODRIVER", "fbcon", 0);
	setenv("SDL_FBDEV", "/dev/fb1", 0);
	setenv("SDL_NOMOUSE", "1", 0);
	setenv("TSLIB_FBDEVICE", "/dev/fb1", 0);
	SDL12 sdl(false);
#else
	setenv("SDL_MOUSEDRV", "TSLIB", 0);
	setenv("SDL_MOUSEDEV", "/dev/input/event1", 0);
	SDL2 sdl(false);
#endif

#else
	SDL2 sdl(true);
#endif
	try
	{
#ifdef SMALL_SCREEN
		SDLRenderer renderer(fs::path(pathResources) / "fonts", true, 320, 240, 16);
#else
		SDLRenderer renderer(fs::path(pathResources) / "fonts", true, 800, 480);
#endif

		// black background
		renderer.clear();

		config.screenSize = SDL_Point{ renderer.width,renderer.height };

		config.renderer = &renderer;
		config.load("config.json");

		// handler for sig term
		std::signal(SIGTERM, [](int signal) -> void
			{
				std::cout << "## SIGTERM: " << signal << std::endl;
				config.quit = true;
			});

		SDLEventLoop eventLoop(&config);

		//ffmpeg - i "D:\Users\johan\Downloads\Pexels Videos 2782.mp4" -q:v 1 -qmin 1 -qmax 1 -vf scale=800 : -1 e:\temp\img%04d.jpg
		//E:\ffmpeg-2023-02-04-git-bdc76f467f-full_build\bin\ffmpeg -i "D:\Users\johan\Sources\rpi\pialarmclock4\background\background-dark2.mp4" -q:v 1 -qmin 1 -qmax 1 -vf scale=800:-1 D:\Users\johan\Sources\rpi\pialarmclock4\background.3\img%04d.jpg
		//E:\ffmpeg-2023-02-04-git-bdc76f467f-full_build\bin\ffmpeg -i "D:\Users\johan\Sources\rpi\pialarmclock4\background\background-dark2.mp4" -q:v 1 -qmin 1 -qmax 1 -vf "scale=800:-1,minterpolate=mi_mode=2" D:\Users\johan\Sources\rpi\pialarmclock4\background.3\img%04d.jpg
		//E:\ffmpeg-2023-02-04-git-bdc76f467f-full_build\bin\ffmpeg -r 1 -s 800x480 -i img%04d.jpg -filter:v fps=25 output2.mpg

		// touch: https://www.eevblog.com/forum/programming/sdl-touchscreen-on-linux-why-do-i-get-rotated-coordinates-(x-y)/

		std::cout << "building/loading UI" << std::endl;

		auto slidingPanel = std::make_shared<SlidingPanel>(SDL_Rect{ 0,0,Uint16(renderer.width),Uint16(renderer.height) });
		eventLoop.addElement(slidingPanel);

		// override for testing to set config page as default
		auto configPanel = createAllClockPages(slidingPanel, pathResources, config);

		auto backgroundAnimation = std::make_shared<Animation>(&renderer, fs::path(pathResources) / "background", ".day", ".night", configPanel->position);
		backgroundAnimation->load(config.renderer);
		configPanel->addChild(backgroundAnimation);

		auto schedule = std::make_shared<Schedule>(fs::path(pathResources) / "images", LocalDateTime::DAY_OF_WEEK_NAMES, backgroundAnimation->position);
		backgroundAnimation->addChild(schedule);

		auto setTimeControlRect = Element::rectOffset(Element::rectBottom(backgroundAnimation->position, 
			SDL_Rect{ backgroundAnimation->position.x,backgroundAnimation->position.y,config.bigScreen() ? 48 : 32,config.bigScreen() ? 48 : 32 }), 
			config.bigScreen() ? 16 : 8, config.bigScreen() ? -64 - 24 : -8 - 48);
		auto setTimeControl = std::make_shared<ClockAnalogDialButton>(fs::path(pathResources) / "images", std::vector<std::string>{"button-48.png"}, setTimeControlRect);
		backgroundAnimation->addChild(setTimeControl);

		auto audioControlRect = Element::rectOffset(Element::rectBottom(backgroundAnimation->position, 
			SDL_Rect{ backgroundAnimation->position.x,backgroundAnimation->position.y,config.bigScreen() ? 48 : 32,config.bigScreen() ? 48 : 32 }), 
			config.bigScreen() ? 16 : 8, config.bigScreen() ? -16 : -8);
		auto audioControl = std::make_shared<AudioControlButton>(fs::path(pathResources) / "images", audioControlRect);
		backgroundAnimation->addChild(audioControl);

		slidingPanel->onTouchDownLong = [slidingPanel, schedule](TouchState& aTouchState, Config& aConfig) -> int
		{
			if (slidingPanel->activePage && slidingPanel->activePage->element && slidingPanel->activePage->element->clockType() >= 0)
			{
				// deactivate next alarm if set
				aConfig.cancelNextAlarm();
				schedule->onInit(aConfig);
				return RERENDER_ALL;
			}
			else
				return RERENDER_NONE;
		};

#ifdef SDL1
		TSLibEvents ts;
		auto tsLibThread = std::thread([&ts]() -> void { ts.loop(); });
#endif

		eventLoop.loop();

#ifdef SDL1
		ts.active = false;
#else
		config.audioStop(300);
#endif
		config.cancelAllAnimations();
		renderer.stop();
		config.save();
		sdl.stop();
		return 0;
	}
	catch (std::exception e)
	{
		sdl.stop();
		std::cout << "## exception in main: " << e.what() << std::endl;
		return 1;
	}
}
