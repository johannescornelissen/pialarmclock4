#pragma once

#include <string>
#include <iostream>
#include <algorithm>
#include <thread>
#include <mutex>
#include <functional>

#include "i2c.h"


class TSL2561 : I2CDevice
{
public:
	TSL2561(int aDeltaMilliSeconds, int aAddress = 0x39, int aSMBUS = 1) : I2CDevice(aAddress, aSMBUS)
	{
		fThreadRunning = false;
		fDeltaMiliiSeconds = aDeltaMilliSeconds;
	}
	virtual ~TSL2561()
	{
		stop();
	}
public:
	int getMaxChannelValue()
	{
		std::lock_guard<std::recursive_mutex> guard(fLock);
		return std::max(fChannel_0, fChannel_1);
	}
	std::string getStatus()
	{
		std::lock_guard<std::recursive_mutex> guard(fLock);
		return std::to_string(fChannel_0) + '/' + std::to_string(fChannel_1);
	}
	double lux()
	{
		std::lock_guard<std::recursive_mutex> guard(fLock);
		if (fChannel_0 > 0)
		{
			auto ch0 = fChannel_0;
			auto ch1 = fChannel_1;
			if (ch1 / ch0 <= 0.50)
				return 0.0304 * ch0 - 0.062 * ch0 * pow(ch1 / ch0, 1.4);
			else if (ch1 / ch0 <= 0.61)
				return 0.0224 * ch0 - 0.031 * ch1;
			else if (ch1 / ch0 <= 0.80)
				return 0.0128 * ch0 - 0.0153 * ch1;
			else if (ch1 / ch0 <= 1.30)
				return 0.00146 * ch0 - 0.00112 * ch1;
			else
				return 0;
		}
		else
			return 0;
	}
	void enable() { i2c_write(0x80, 0x03); }
	void disable() { i2c_write(0x80, 0x00); }
	void low_gain() { i2c_write(0x81, 0x02); }
	void high_gain() { i2c_write(0x81, 0x12); }
	void start_measurement_high_gain() { i2c_write(0x81, 0x1B); }
	void stop_measurement_high_gain() { i2c_write(0x81, 0x13); }
public:
	bool sensor_open()
	{
		if (i2c_open(getDevice()))
		{
			enable();
			high_gain();
			return true;
		}
		else
			return false;
	}
	void start()
	{
		if (!i2c_is_open())
			sensor_open();
		if (!fThreadRunning)
		{
			fThreadRunning = true;
			fThread = std::thread(&TSL2561::run, this);
		}
	}
	void stop()
	{
		if (fThreadRunning)
		{
			fThreadRunning = false;
			if (fThread.joinable())
				fThread.join();
		}
		if (i2c_is_open())
		{
			disable();
			i2c_close();
		}
	}
	std::function<void(double aLux)> onValue;
private:
	void run()
	{
#ifndef NDEBUG
		std::cout << "TSL2561 now running" << std::endl;
#endif
		while (fThreadRunning)
		{
			uint8_t buffer[4];
			if (i2c_read(0x8C, buffer, 4))
			{
				std::lock_guard<std::recursive_mutex> guard(fLock);
				fChannel_0 = ((buffer[1] << 8) + buffer[0]); // photo diode 650nm
				fChannel_1 = ((buffer[3] << 8) + buffer[2]); // photo diode 810nm
				if (onValue)
					onValue(lux());
			}
#ifndef NDEBUG
			else
				std::cout << "## could not read TSL2561 values" << std::endl;
#endif
			usleep(fDeltaMiliiSeconds*1000);
		}
#ifndef NDEBUG
		std::cout << "TSL2561 now stopped" << std::endl;
#endif
	}
private:
	bool fThreadRunning;
	int fDeltaMiliiSeconds;
	std::thread fThread;
	int fChannel_0 = 0;
	int fChannel_1 = -0;
	std::recursive_mutex fLock;
};
