#pragma once

#include <string>
#include <iostream>
#include <algorithm>
#include <thread>
#include <chrono>
#include <mutex>
#include <functional>
#include <cmath>

#include "i2c.h"

using namespace std::literals::chrono_literals;


//from https://github.com/adafruit/Adafruit_TSL2591_Library/blob/master/Adafruit_TSL2591.h

#define TSL2591_VISIBLE 2						// (channel 0) - (channel 1)
#define TSL2591_INFRARED 1						// channel 1
#define TSL2591_FULLSPECTRUM 0					// channel 0

#define TSL2591_COMMAND_BIT 0xA0				// 1010 0000: bits 7 and 5 for 'command normal'

#define TSL2591_CLEAR_INT 0xE7					// Special Function Command for "Clear ALS and no persist ALS interrupt"

#define TSL2591_TEST_INT 0xE4					// Special Function Command for "Interrupt set - forces an interrupt"

#define TSL2591_WORD_BIT 0x20					// 1 = read/write word (rather than byte)
#define TSL2591_BLOCK_BIT 0x10					// 1 = using block read/write

#define TSL2591_ENABLE_POWEROFF 0x00			// Flag for ENABLE register to disable
#define TSL2591_ENABLE_POWERON 0x01				// Flag for ENABLE register to enable
#define TSL2591_ENABLE_AEN 0x02					// ALS Enable. This field activates ALS function. Writing a one
												// activates the ALS. Writing a zero disables the ALS.

#define TSL2591_ENABLE_AIEN 0x10				// ALS Interrupt Enable. When asserted permits ALS interrupts to be
												// generated, subject to the persist filter.
#define TSL2591_ENABLE_NPIEN 0x80				// No Persist Interrupt Enable. When asserted NP Threshold conditions
												// will generate an interrupt, bypassing the persist filter

#define TSL2591_LUX_DF 408.0					// Lux cooefficient
//#define TSL2591_LUX_COEFB 1.64					// CH0 coefficient
//#define TSL2591_LUX_COEFC 0.59					// CH1 coefficient A
//#define TSL2591_LUX_COEFD 0.86					// CH2 coefficient B

/// TSL2591 Register map
enum {
	TSL2591_REGISTER_ENABLE = 0x00,				// Enable register
	TSL2591_REGISTER_CONTROL = 0x01,			// Control register
	TSL2591_REGISTER_THRESHOLD_AILTL = 0x04,	// ALS low threshold lower byte
	TSL2591_REGISTER_THRESHOLD_AILTH = 0x05,	// ALS low threshold upper byte
	TSL2591_REGISTER_THRESHOLD_AIHTL = 0x06,	// ALS high threshold lower byte
	TSL2591_REGISTER_THRESHOLD_AIHTH = 0x07,	// ALS high threshold upper byte
	TSL2591_REGISTER_THRESHOLD_NPAILTL = 0x08,	// No Persist ALS low threshold lower byte
	TSL2591_REGISTER_THRESHOLD_NPAILTH = 0x09,	// No Persist ALS low threshold higher byte
	TSL2591_REGISTER_THRESHOLD_NPAIHTL = 0x0A,	// No Persist ALS high threshold lower byte
	TSL2591_REGISTER_THRESHOLD_NPAIHTH = 0x0B,	// No Persist ALS high threshold higher byte
	TSL2591_REGISTER_PERSIST_FILTER = 0x0C,		// Interrupt persistence filter
	TSL2591_REGISTER_PACKAGE_PID = 0x11,		// Package Identification
	TSL2591_REGISTER_DEVICE_ID = 0x12,			// Device Identification
	TSL2591_REGISTER_DEVICE_STATUS = 0x13,		// Internal Status
	TSL2591_REGISTER_CHAN0_LOW = 0x14,			// Channel 0 data, low byte
	TSL2591_REGISTER_CHAN0_HIGH = 0x15,			// Channel 0 data, high byte
	TSL2591_REGISTER_CHAN1_LOW = 0x16,			// Channel 1 data, low byte
	TSL2591_REGISTER_CHAN1_HIGH = 0x17,			// Channel 1 data, high byte
};

/// Enumeration for the sensor integration timing
typedef enum {
	TSL2591_INTEGRATIONTIME_100MS = 0x00,		// 100 millis
	TSL2591_INTEGRATIONTIME_200MS = 0x01,		// 200 millis
	TSL2591_INTEGRATIONTIME_300MS = 0x02,		// 300 millis
	TSL2591_INTEGRATIONTIME_400MS = 0x03,		// 400 millis
	TSL2591_INTEGRATIONTIME_500MS = 0x04,		// 500 millis
	TSL2591_INTEGRATIONTIME_600MS = 0x05,		// 600 millis
} tsl2591IntegrationTime_t;

/// Enumeration for the persistance filter (for interrupts)
typedef enum {
	//  bit 7:4: 0
	TSL2591_PERSIST_EVERY = 0x00,				// Every ALS cycle generates an interrupt
	TSL2591_PERSIST_ANY = 0x01,					// Any value outside of threshold range
	TSL2591_PERSIST_2 = 0x02,					// 2 consecutive values out of range
	TSL2591_PERSIST_3 = 0x03,					// 3 consecutive values out of range
	TSL2591_PERSIST_5 = 0x04,					// 5 consecutive values out of range
	TSL2591_PERSIST_10 = 0x05,					// 10 consecutive values out of range
	TSL2591_PERSIST_15 = 0x06,					// 15 consecutive values out of range
	TSL2591_PERSIST_20 = 0x07,					// 20 consecutive values out of range
	TSL2591_PERSIST_25 = 0x08,					// 25 consecutive values out of range
	TSL2591_PERSIST_30 = 0x09,					// 30 consecutive values out of range
	TSL2591_PERSIST_35 = 0x0A,					// 35 consecutive values out of range
	TSL2591_PERSIST_40 = 0x0B,					// 40 consecutive values out of range
	TSL2591_PERSIST_45 = 0x0C,					// 45 consecutive values out of range
	TSL2591_PERSIST_50 = 0x0D,					// 50 consecutive values out of range
	TSL2591_PERSIST_55 = 0x0E,					// 55 consecutive values out of range
	TSL2591_PERSIST_60 = 0x0F,					// 60 consecutive values out of range
} tsl2591Persist_t;

/// Enumeration for the sensor gain
typedef enum {
	TSL2591_GAIN_LOW = 0x00,					// low gain (1x)
	TSL2591_GAIN_MED = 0x10,					// medium gain (25x)
	TSL2591_GAIN_HIGH = 0x20,					// medium gain (428x)
	TSL2591_GAIN_MAX = 0x30,					// max gain (9876x)
} tsl2591Gain_t;


// end from https://github.com/adafruit/Adafruit_TSL2591_Library/blob/master/Adafruit_TSL2591.h


class TSL2591 : I2CDevice
{
public:
	TSL2591(int aDeltaMilliSeconds, int aAddress = 0x29, int aSMBUS = 1) : I2CDevice(aAddress, aSMBUS)
	{
		//fThreadRunning = false;
		//fDeltaMiliiSeconds = aDeltaMilliSeconds;
		_gain = TSL2591_GAIN_MED;
	}
	virtual ~TSL2591()
	{
		stop();
	}
private:
	tsl2591IntegrationTime_t _integration = TSL2591_INTEGRATIONTIME_100MS;
	tsl2591Gain_t _gain = TSL2591_GAIN_MED;
public:
	bool exists()
	{
		uint8_t id;
		if (i2c_read(TSL2591_COMMAND_BIT | TSL2591_REGISTER_DEVICE_ID, &id, sizeof(id)))
			return id == 0x50;
		else
			return false;
	}
	/*
	int getMaxChannelValue()
	{
		std::lock_guard<std::recursive_mutex> guard(fLock);
		return std::max(fChannel_0, fChannel_1);
	}
	std::string getStatus()
	{
		std::lock_guard<std::recursive_mutex> guard(fLock);
		return std::to_string(fChannel_0) + '/' + std::to_string(fChannel_1);
	}
	*/
	void enable() 
	{ 
		i2c_write(
			TSL2591_COMMAND_BIT | TSL2591_REGISTER_ENABLE, 
			TSL2591_ENABLE_POWERON | TSL2591_ENABLE_AEN | TSL2591_ENABLE_AIEN | TSL2591_ENABLE_NPIEN); 
	}
	void disable() 
	{ 
		i2c_write(
			TSL2591_COMMAND_BIT | TSL2591_REGISTER_ENABLE,
			TSL2591_ENABLE_POWEROFF);
	}
	void setGain(tsl2591Gain_t aGain)
	{
		enable();
		_gain = aGain;
		i2c_write(
			TSL2591_COMMAND_BIT | TSL2591_REGISTER_CONTROL, 
			_integration | _gain);
		disable();
	}
	tsl2591Gain_t getGain() 
	{ 
		return _gain; 
	}
	void setTiming(tsl2591IntegrationTime_t aIntegration) {
		enable();
		_integration = aIntegration;
		i2c_write(
			TSL2591_COMMAND_BIT | TSL2591_REGISTER_CONTROL, 
			_integration | _gain);
		disable();
	}
	tsl2591IntegrationTime_t getTiming() 
	{ 
		return _integration; 
	}
	double lux(uint16_t ch0, uint16_t ch1) {
		double atime, again;
		double cpl, lux;

		// Check for overflow conditions first
		if ((ch0 != 0xFFFF) && (ch1 != 0xFFFF))
		{
			// Note: This algorithm is based on preliminary coefficients
			// provided by AMS and may need to be updated in the future
			switch (_integration) 
			{
			case TSL2591_INTEGRATIONTIME_100MS:
				atime = 100.0;
				break;
			case TSL2591_INTEGRATIONTIME_200MS:
				atime = 200.0;
				break;
			case TSL2591_INTEGRATIONTIME_300MS:
				atime = 300.0;
				break;
			case TSL2591_INTEGRATIONTIME_400MS:
				atime = 400.0;
				break;
			case TSL2591_INTEGRATIONTIME_500MS:
				atime = 500.0;
				break;
			case TSL2591_INTEGRATIONTIME_600MS:
				atime = 600.0;
				break;
			default: // 100ms
				atime = 100.0;
				break;
			}

			switch (_gain) 
			{
			case TSL2591_GAIN_LOW:
				again = 1.0;
				break;
			case TSL2591_GAIN_MED:
				again = 25.0;
				break;
			case TSL2591_GAIN_HIGH:
				again = 428.0;
				break;
			case TSL2591_GAIN_MAX:
				again = 9876.0;
				break;
			default:
				again = 1.0;
				break;
			}

			cpl = (atime * again) / TSL2591_LUX_DF;
			// Alternate lux calculation 1
			// See: https://github.com/adafruit/Adafruit_TSL2591_Library/issues/14
			lux = (((double)ch0 - (double)ch1)) * (1.0F - ((double)ch1 / (double)ch0)) / cpl;
			return lux;
		}
		else
			return -1; // overflow
	}
	uint32_t getFullLuminosity(void)
	{
		enable();
		// Wait x ms for ADC to complete
		for (uint8_t d = 0; d <= _integration; d++) 
		{
			std::this_thread::sleep_for(120ms); // sleep(120);
		}
		// CHAN0 must be read before CHAN1
		// See: https://forums.adafruit.com/viewtopic.php?f=19&t=124176
		uint16_t x;
		uint16_t y;
		i2c_read(TSL2591_COMMAND_BIT | TSL2591_REGISTER_CHAN0_LOW, &y, sizeof(y));
		i2c_read(TSL2591_COMMAND_BIT | TSL2591_REGISTER_CHAN1_LOW, &x, sizeof(x));
		disable();
		return x << 16 | y;
	}
	uint16_t getLuminosity(uint8_t channel) {
		uint32_t x = getFullLuminosity();
		if (channel == TSL2591_FULLSPECTRUM) 
			// Reads two byte value from channel 0 (visible + infrared)
			return (x & 0xFFFF);
		else if (channel == TSL2591_INFRARED) 
			// Reads two byte value from channel 1 (infrared)
			return (x >> 16);
		else if (channel == TSL2591_VISIBLE) 
			// Reads all and subtracts out just the visible!
			return ((x & 0xFFFF) - (x >> 16));
		else
			// unknown channel!
			return 0; 
	}
public:
	bool sensor_open()
	{
		if (i2c_open(getDevice()))
		{
			setTiming(_integration);
			setGain(_gain);
			enable();
			return true;
		}
		else
			return false;
	}
	/*
	void start()
	{
		if (!i2c_is_open())
			sensor_open();
		if (!fThreadRunning)
		{
			fThreadRunning = true;
			fThread = std::thread(&TSL2591::run, this);
		}
	}
	*/
	void stop()
	{
		/*
		if (fThreadRunning)
		{
			fThreadRunning = false;
			if (fThread.joinable())
				fThread.join();
		}
		*/
		if (i2c_is_open())
		{
			disable();
			i2c_close();
		}
	}
	/*
	std::function<void(double aLux)> onValue;
	bool sensor_read()
	{
		uint8_t buffer[4];
		if (i2c_read(0x8C, buffer, 4))
		{
			std::lock_guard<std::recursive_mutex> guard(fLock);
			fChannel_0 = ((buffer[1] << 8) + buffer[0]); // photo diode 650nm
			fChannel_1 = ((buffer[3] << 8) + buffer[2]); // photo diode 810nm
			return true;
		}
		else
			return false;
	}
	*/
/*
private:
	void run()
	{
#ifndef NDEBUG
		std::cout << "TSL2561 now running" << std::endl;
#endif
		while (fThreadRunning)
		{
			uint8_t buffer[4];
			if (i2c_read(0x8C, buffer, 4))
			{
				std::lock_guard<std::recursive_mutex> guard(fLock);
				fChannel_0 = ((buffer[1] << 8) + buffer[0]); // photo diode 650nm
				fChannel_1 = ((buffer[3] << 8) + buffer[2]); // photo diode 810nm
				if (onValue)
					onValue(lux());
			}
#ifndef NDEBUG
			else
				std::cout << "## could not read TSL2561 values" << std::endl;
#endif
			usleep(fDeltaMiliiSeconds * 1000);
		}
#ifndef NDEBUG
		std::cout << "TSL2561 now stopped" << std::endl;
#endif
	}
private:
	//bool fThreadRunning;
	//int fDeltaMiliiSeconds;
	//std::thread fThread;
	//int fChannel_0 = 0;
	//int fChannel_1 = -0;
	std::recursive_mutex fLock;
*/
};
