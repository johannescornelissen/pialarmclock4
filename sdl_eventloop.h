#pragma once

#include <map>
#include <mutex>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <cmath>
#include <fstream>
#include <set>
#include <cmath>
#include <sstream>

#include <nlohmann/json.hpp>

#ifdef SDL1
#include "sdl_renderer1.h"
#else
#include "sdl_renderer2.h"
#endif

#define TOUCH_SHORT_TOUCH_TIMESTAMP_LIMIT 1000
#define TOUCH_MOVED_MIN_DISTANCE 16.0f

#define ANIMATION_INTERVAL 70 // ms

#define RERENDER_NONE 0
//#define RERENDER_THIS 1
#define RERENDER_ALL 2
#define RERENDER_ALL_CLEAR_FIRST 3

#define CONFIG_FILE_NAME "config.json"

#define SECONDS_PER_DAY			((time_t)24 * 60 * 60)
#define NEXT_ALARM_RED_TIME		(SECONDS_PER_DAY / 2)

#define ANIMATION_INITIAL_FRAMES 1 // 50


#ifdef SDL1
typedef Uint8 SDLConstEventType;
#else
typedef int SDLConstEventType;
#endif

#ifdef SDL1
inline SDLConstEventType longTouchDownEvent = SDL_USEREVENT + 0; //  SDL_RegisterEvents(1);
inline SDLConstEventType rerenderEvent = SDL_USEREVENT + 1; //  SDL_RegisterEvents(1);
inline SDLConstEventType touchDNEvent = SDL_USEREVENT + 2;
inline SDLConstEventType touchUPEvent = SDL_USEREVENT + 3;
inline SDLConstEventType touchMVEvent = SDL_USEREVENT + 4;
#else
inline Uint32 longTouchDownEvent = SDL_RegisterEvents(1);
inline Uint32 rerenderEvent = SDL_RegisterEvents(1);
#endif

inline std::vector<std::function<void()>> dispatch;

Uint32 sendRerenderEvent()
{
	SDL_Event sdl_event;
	SDL_memset(&sdl_event, 0, sizeof(sdl_event));
	sdl_event.type = rerenderEvent;
	SDL_PushEvent(&sdl_event);
	return 0;
}

Uint32 sendQuitEvent()
{
	SDL_Event sdl_event;
	SDL_memset(&sdl_event, 0, sizeof(sdl_event));
	sdl_event.type = SDL_QUIT;
	SDL_PushEvent(&sdl_event);
	return 0;
}


class Element; // forward

class Animation; //forward

class LocalDateTime
{
protected:
	static int calculate_local_time_offset()
	{
#ifdef __linux__
		time_t gmt, rawtime = time(NULL);
		struct tm* ptm;
		struct tm gbuf;
		ptm = gmtime_r(&rawtime, &gbuf);
		// Request that mktime() looksup dst in timezone database
		ptm->tm_isdst = -1;
		gmt = mktime(ptm);
#else
		time_t gmt, rawtime;
		time(&rawtime);
		tm tm;
		gmtime_s(&tm, &rawtime);
		tm.tm_isdst = -1;
		gmt = std::mktime(&tm);
#endif
		return (int)difftime(rawtime, gmt);
	}
	static inline int local_time_offset = calculate_local_time_offset();
public:
	static std::vector<std::string> buildDayOfWeekNames(int aMaxLength)
	{
		std::vector<std::string> res;
		for (int dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++)
		{
			tm decodedDateTime{ 0,0,0,0,0,0,dayOfWeek,0,0 };
			char timeString[50];
			std::strftime(timeString, std::size(timeString), "%a", &decodedDateTime);
			res.push_back(std::string(timeString).substr(0, aMaxLength));
		}
		return res;
	}

	static std::vector<std::string> buildMonthNames()
	{
		std::vector<std::string> res;
		for (int month = 0; month < 12; month++)
		{
			tm decodedDateTime{ 0,0,0,0,month,0,0,0,0 };
			char timeString[50];
			std::strftime(timeString, std::size(timeString), "%b", &decodedDateTime);
			res.push_back(timeString);
		}
		return res;
	}

	inline static std::vector<std::string> DAY_OF_WEEK_NAMES = buildDayOfWeekNames(2);
	inline static std::vector<std::string> MONTH_NAMES = buildMonthNames();

	static time_t now()
	{
		time_t t;
		time(&t);
		return t + local_time_offset;
	}

	static time_t fromParts(int aYear, int aMonth, int aDay, int aHour, int aMinutes, int aSeconds)
	{
		tm decodedTime{ 0 };
		decodedTime.tm_year = aYear - 1900;
		decodedTime.tm_mon = aMonth - 1;
		decodedTime.tm_mday = aDay;
		decodedTime.tm_hour = aHour;
		decodedTime.tm_min = aMinutes;
		decodedTime.tm_sec = aSeconds;
		decodedTime.tm_isdst = 0;
		return std::mktime(&decodedTime) + local_time_offset;
	}

	static time_t datePart(time_t aDateTime)
	{
		return (aDateTime / SECONDS_PER_DAY) * SECONDS_PER_DAY;
	}

	static time_t timePart(time_t aDateTime)
	{
		return aDateTime % SECONDS_PER_DAY;
	}

	static void timeParts(time_t aDateTime, int* aHour = nullptr, int* aMinutes = nullptr, int* aSeconds = nullptr)
	{
		if (aHour)
			*aHour = int((aDateTime / ((time_t)60 * 60)) % 24);
		if (aMinutes)
			*aMinutes = int((aDateTime / ((time_t)60)) % 60);
		if (aSeconds)
			*aSeconds = int(aDateTime % (time_t)60);
	}

	static time_t fromTimeParts(time_t aHour, time_t aMinutes, time_t aSeconds = 0)
	{
		return aHour * 60 * 60 + aMinutes * 60 + aSeconds;
	}

	static int dayOfWeek(time_t aDateTime)
	{
		return int(((aDateTime / SECONDS_PER_DAY) + 4) % 7);
	}

	static void parts(time_t aDateTime,
		int* aYear, int* aMonth, int* aDay,
		int* aHour = nullptr, int* aMinutes = nullptr, int* aSeconds = nullptr,
		int* aDayOfWeek = nullptr, int* aDayOfYear = nullptr)
	{
		tm decodedTime;
#ifdef __linux__
		gmtime_r(&aDateTime, &decodedTime); // we handle aDateTime as local time so conversion should be straight -> gmt //localtime_s(&decodedTime, &aDateTime);
#else
		gmtime_s(&decodedTime, &aDateTime); // we handle aDateTime as local time so conversion should be straight -> gmt //localtime_s(&decodedTime, &aDateTime);
#endif
		if (aYear)
			*aYear = decodedTime.tm_year + 1900;
		if (aMonth)
			*aMonth = decodedTime.tm_mon + 1;
		if (aDay)
			*aDay = decodedTime.tm_mday;
		if (aHour)
			*aHour = decodedTime.tm_hour;
		if (aMinutes)
			*aMinutes = decodedTime.tm_min;
		if (aSeconds)
			*aSeconds = decodedTime.tm_sec;
		if (aDayOfWeek)
			*aDayOfWeek = decodedTime.tm_wday;
		if (aDayOfYear)
			*aDayOfYear = decodedTime.tm_yday;
	}

	static std::string ISO(time_t aDateTime, bool aAddExtraFields = false)
	{
		int year, month, day, hour, minutes, seconds, dayOfWeek, dayOfYear;
		parts(aDateTime, &year, &month, &day, &hour, &minutes, &seconds, &dayOfWeek, &dayOfYear);
		std::stringstream ss;
		ss << std::setfill('0') << std::setw(4) << year << "-" << std::setw(2) << month << "-" << std::setw(2) << day << " " << std::setw(2) << hour << ":" << std::setw(2) << minutes << ":" << std::setw(2) << seconds;
		if (aAddExtraFields)
			ss << " (" << dayOfWeek << ":" + DAY_OF_WEEK_NAMES[dayOfWeek] + "," << dayOfYear << ")";
		return ss.str();
	}

	static std::string clockDateStr(time_t aDateTime)
	{
		std::stringstream ss;
		int month, day, dayOfWeek;
		parts(aDateTime, nullptr, &month, &day, nullptr, nullptr, nullptr, &dayOfWeek);
		ss << DAY_OF_WEEK_NAMES[dayOfWeek] << " " << day << " " << MONTH_NAMES[size_t(month) - 1];
		return ss.str();
	}

	static std::string clockNextAlarmStr(time_t aDateTime)
	{
		std::stringstream ss;
		int month, day, dayOfWeek, hour, minutes;
		parts(aDateTime, nullptr, &month, &day, &hour, &minutes, nullptr, &dayOfWeek);
		ss << DAY_OF_WEEK_NAMES[dayOfWeek] << " " << std::setfill('0') << hour << ":" << std::setw(2) << minutes;
		return ss.str();
	}

	static void show(time_t aDateTime, bool aAddExtraFields = false)
	{
		std::cout << ISO(aDateTime, aAddExtraFields) << std::endl;
	}
};

Uint32 handleNextAnimationFrame(Uint32 interval, void* param); // forward

class Config
{
public:
	bool quit = false;
	SDLRenderer* renderer = nullptr;
	SDL_Point screenSize{ 0,0 };
	std::recursive_mutex lock;
	time_t currentTime = 0; 
	std::vector<time_t> alarms;
	int daysInSchedule = 7;
	int clockType = 0;
	bool showNextAlarmWithDay = true;
	bool showCurrentDate = false; // true;

	bool bigScreen() { return screenSize.x >= 400; }

	void load(std::string aConfigFileName = CONFIG_FILE_NAME)
	{
		// read config
		auto config = fs::exists(aConfigFileName) ? nlohmann::json::parse(std::ifstream(aConfigFileName)) : nlohmann::json();
		if (config.contains("alarms"))
			json2alarms(config["alarms"]);
		if (config.contains("daysInSchedule"))
			daysInSchedule = config["daysInSchedule"];
		if (config.contains("volume"))
			audioVolume = config["volume"];
		if (config.contains("soundFileName"))
			audioAlarmSoundFileName = config["soundFileName"];
		if (config.contains("clockType"))
			clockType = config["clockType"];
		if (config.contains("showNextAlarmWithDay"))
			showNextAlarmWithDay = config["showNextAlarmWithDay"];
		if (config.contains("showCurrentDate"))
			showCurrentDate = config["showCurrentDate"];
	}

	void save(std::string aConfigFileName = CONFIG_FILE_NAME)
	{
		// read config
		auto config = fs::exists(aConfigFileName) ? nlohmann::json::parse(std::ifstream(aConfigFileName)) : nlohmann::json();
		// update alarms etc. in config
		config["alarms"] = alarms2json();
		config["daysInSchedule"] = daysInSchedule;
		config["volume"] = audioVolume;
		config["soundFileName"] = audioAlarmSoundFileName;
		config["clockType"] = clockType;
		config["showNextAlarmWithDay"] = showNextAlarmWithDay;
		config["showCurrentDate"] = showCurrentDate;
		// open config as output
		std::ofstream ofs(aConfigFileName);
		// save new config
		ofs << config;
	}

	void recalculateAlarms(time_t aNow)
	{
		for (auto& alarm : alarms)
		{
			while (alarm <= aNow)
				alarm += SECONDS_PER_DAY * daysInSchedule;
		}
		// sort alarms on time, low to high
		std::sort(alarms.begin(), alarms.end());
	}

	void addNewTime()
	{
		if (alarms.size() > 0)
		{
			std::set<time_t> times;
			for (auto alarm : alarms)
			{
				int hour, minutes;
				LocalDateTime::timeParts(alarm, &hour, &minutes);
				times.insert({ LocalDateTime::timePart(alarm) });
			}
			// add alarm based on first active alarm, add 1 day and 10 minutes
			auto newAlarm = alarms[0] + SECONDS_PER_DAY + time_t(10) * 60;
			auto newTime = LocalDateTime::timePart(newAlarm);
			// find if new time is unique, else keep adding 10 minutes until it is unique
			while (times.find(newTime) != times.end())
			{
				newAlarm += time_t(10) * 60;
				newTime = LocalDateTime::timePart(newAlarm);
			}
			alarms.push_back(newAlarm);
		}
		else
		{
			// add a new alarm based on current date/time and then add 1 day and 10 minutes
			auto newAlarm = currentTime + SECONDS_PER_DAY + time_t(10) * 60;
			// round alarm to 5 minutes
			newAlarm = (newAlarm / (time_t(5) * 60)) * time_t(5) * 60;
			alarms.push_back(newAlarm);
		}
	}

	void cancelNextAlarm()
	{
		if (alarms.size() > 0)
		{
			auto now = LocalDateTime::now();
			if (alarms[0] - now < NEXT_ALARM_RED_TIME)
			{
				alarms[0] += SECONDS_PER_DAY * daysInSchedule;
				recalculateAlarms(now);
				save();
			}
		}
	}

	time_t calculateAlarmDelta()
	{
		if (alarms.size() > 0 && alarms[0] < currentTime + 100 * SECONDS_PER_DAY)
			return alarms[0] - currentTime;
		else
			return 0;
	}
public:
	std::set<Animation*> animations;
	SDL_TimerID animationTimerID = 0;
	
	void startAnimation(Animation* aAnimation)
	{
		std::scoped_lock guard(lock);
		animations.insert({ aAnimation });
		if (!animationTimerID)
			animationTimerID = SDL_AddTimer(ANIMATION_INTERVAL, &handleNextAnimationFrame, this);
	}

	void cancelAnimation(Animation* aAnimation)
	{
		std::scoped_lock guard(lock);
		animations.erase(aAnimation);
		if (animations.size() == 0 && animationTimerID)
		{
			// no more animations running -> stop timer
			SDL_RemoveTimer(animationTimerID);
			animationTimerID = 0;
		}
	}

	void cancelAllAnimations()
	{
		std::scoped_lock guard(lock);
		animations.clear();
		if (animationTimerID)
		{
			// no more animations running -> stop timer
			SDL_RemoveTimer(animationTimerID);
			animationTimerID = 0;
		}
	}
public:
	double audioVolume = 0.5;
	std::string audioAlarmSoundFileName = "default.mp3";
	void audioPlay(fs::path aSoundsBasePath, int aFadeInMS = 5000, int aLoops = 3)
	{
		audioStop(1000);
		fActiveMusic = Mix_LoadMUS((aSoundsBasePath / audioAlarmSoundFileName).string().c_str());
		//fActiveMusic = (int*)1;
		audioSetVolume();
		Mix_FadeInMusic(fActiveMusic, aLoops, aFadeInMS);
	}

	void audioStop(int aFadeOutMS = 2000)
	{
		if (fActiveMusic)
		{
			Mix_FadeOutMusic(aFadeOutMS);
			Mix_FreeMusic(fActiveMusic);
			fActiveMusic = nullptr;
		}
	}
	bool audioIsPlaying() 
	{ 
		return fActiveMusic != nullptr;
	}
	
	void audioSetVolume(double aVolume=NAN)
	{
		if (!std::isnan(aVolume))
			audioVolume = aVolume;
		Mix_VolumeMusic(int(audioVolume * double(MIX_MAX_VOLUME)));
	}
private:
	nlohmann::json alarms2json()
	{
		auto res = nlohmann::json::array();
		for (auto alarm : alarms)
			res.push_back(alarm);
		return res;
	}

	void json2alarms(nlohmann::json aAlarms)
	{
		for (auto& alarm : aAlarms)
		{
			auto a = alarm.get<time_t>();
			alarms.push_back(a);
		}
	}

	Mix_Music* fActiveMusic = nullptr;
	//int* fActiveMusic = nullptr;
};

class TouchState
{
public:
#ifdef __linux__
	time_t currentTime = 0x7FFFFFFFFFFFFFFF; // initialize high so we trigger recalculation on first use
#else
	time_t currentTime = 0x7FFFFFFF; // initialize high so we trigger recalculation on first use
#endif
	Uint32 currentTimeStamp = 0;
	bool moving = false;
	Uint32 touchDownTimeStamp = 0;
	SDL_TimerID touchDownTimerID = 0;
	int touchDownX = 0;
	int touchDownY = 0;
	Element* touchDownElement = nullptr;
	int currentX = 0;
	int currentY = 0;
	float moved() 
	{ 
		auto dx = float(currentX - touchDownX);
		auto dy = float(currentY - touchDownY);
#ifdef __linux__
		return std::sqrt(dx * dx + dy * dy);
#else
		return std::sqrtf((float)(dx * dx + dy * dy));
#endif
	}
	bool touchDownInRect(const SDL_Rect& aRect, const SDL_Point& aOffset)
	{
		auto x = touchDownX + aOffset.x;
		auto y = touchDownY + aOffset.y;
		return
			aRect.x <= x && x < aRect.x + aRect.w &&
			aRect.y <= y && y < aRect.y + aRect.h;
	}
	bool touchDownInRect(const SDL_Rect& aRect)
	{
		return touchDownInRect(aRect, SDL_Point{ 0,0 });
	}
};

class Element
{
public:
	Element(const SDL_Rect& aPosition, bool aTouchable, bool aEnabled)
	{
		position = aPosition;
		touchable = aTouchable;
		enabled = aEnabled;
	}
	virtual ~Element() {}
public:
	SDL_Rect position;
	bool touchable = false;
	bool enabled = true;
	Element* parent = nullptr;
	std::vector<std::shared_ptr<Element>> children;
	bool visible = true;
	virtual std::string elementName() = 0;
	virtual int clockType() { return -1; }
public:
	virtual SDL_Point relativeXY()
	{
		return relativeXY(0, 0);
	}
	virtual SDL_Point relativeXY(int x, int y)
	{
		return this->parent ? this->parent->relativeXY(x,y) : SDL_Point{ x,y };
	}
	virtual SDL_Rect relativeDisplayPosition(const SDL_Point& aPoint)
	{ 
		return rectOffset(position, aPoint.x, aPoint.y);
	}
	virtual SDL_Rect relativeDisplayPosition(int x, int y)
	{
		return rectOffset(position, x, y);
	}
	static SDL_Point pointOffset(const SDL_Point& aPoint, int x, int y)
	{
		return SDL_Point{ aPoint.x + x, aPoint.y + y };
	}
	static SDL_Point pointOffset(const SDL_Point& aPoint, const SDL_Point& aOffset)
	{
		return SDL_Point{ aPoint.x + aOffset.x, aPoint.y + aOffset.y };
	}
	static SDL_Point centerOfRect(const SDL_Rect& aRect)
	{
		return SDL_Point{ aRect.x + aRect.w / 2,aRect.y + aRect.h / 2 };
	}
	static SDL_Rect rectOffset(const SDL_Rect& aRect, int dx, int dy)
	{
		return SDL_Rect{ aRect.x + dx, aRect.y + dy, aRect.w, aRect.h };
	}
	static SDL_Rect rectOffset(const SDL_Rect& aRect, const SDL_Point& aOffset)
	{
		return SDL_Rect{ aRect.x + aOffset.x, aRect.y + aOffset.y, aRect.w, aRect.h };
	}
	static SDL_Rect rectMoveSize(const SDL_Rect& aRect, double dx, double dy)
	{
		return SDL_Rect{ aRect.x + int(dx * aRect.w), aRect.y + int(dy * aRect.h), aRect.w, aRect.h };
	}
	static SDL_Rect rectInflate(const SDL_Rect& aRect, int dx, int dy)
	{
		return SDL_Rect{ aRect.x - dx / 2, aRect.y - dy / 2, aRect.w + dx, aRect.h + dy };
	}
	static SDL_Rect rectCenter(const SDL_Rect& aOuterRect, const SDL_Rect& aInnerRect)
	{
		return SDL_Rect{ aOuterRect.x + (aOuterRect.w - aInnerRect.w) / 2, aOuterRect.y + (aOuterRect.h - aInnerRect.h) / 2, aInnerRect.w, aInnerRect.h };
	}
	static SDL_Rect rectCenterX(const SDL_Rect& aOuterRect, const SDL_Rect& aInnerRect)
	{
		return SDL_Rect{ aOuterRect.x + (aOuterRect.w - aInnerRect.w) / 2, aInnerRect.y, aInnerRect.w, aInnerRect.h };
	}
	static SDL_Rect rectCenterY(const SDL_Rect& aOuterRect, const SDL_Rect& aInnerRect)
	{
		return SDL_Rect{ aInnerRect.x, aOuterRect.y + (aOuterRect.h - aInnerRect.h) / 2, aInnerRect.w, aInnerRect.h };
	}
	static SDL_Rect rectCenterAt(const SDL_Rect& aRect, int x, int y)
	{
		return SDL_Rect{ x - aRect.w / 2, y - aRect.h / 2, aRect.w, aRect.h };
	}
	static SDL_Rect rectLeft(const SDL_Rect& aOuterRect, const SDL_Rect& aInnerRect)
	{
		return SDL_Rect{ aOuterRect.x, aInnerRect.y, aInnerRect.w, aInnerRect.h };
	}
	static SDL_Rect rectTop(const SDL_Rect& aOuterRect, const SDL_Rect& aInnerRect)
	{
		return SDL_Rect{ aInnerRect.x, aOuterRect.y, aInnerRect.w, aInnerRect.h };
	}
	static SDL_Rect rectRight(const SDL_Rect& aOuterRect, const SDL_Rect& aInnerRect)
	{
		return SDL_Rect{ aOuterRect.x + aOuterRect.w - aInnerRect.w, aInnerRect.y, aInnerRect.w, aInnerRect.h };
	}
	static SDL_Rect rectBottom(const SDL_Rect& aOuterRect, const SDL_Rect& aInnerRect)
	{
		return SDL_Rect{ aInnerRect.x, aOuterRect.y + aOuterRect.h - aInnerRect.h, aInnerRect.w, aInnerRect.h };
	}
public:
	int virtual onInit(Config& aConfig) { return RERENDER_NONE; }
	void virtual onExit(Config& aConfig) { }
	int virtual onTouchDown(TouchState& aTouchState, Config& aConfig) { return RERENDER_NONE; }
	int virtual onTouchUp(TouchState& aTouchState, Config& aConfig) { return RERENDER_NONE; }
	int virtual onTouchUpLong(TouchState& aTouchState, Config& aConfig) { return RERENDER_NONE; }
	int virtual onTouchMoveStart(TouchState& aTouchState, Config& aConfig) { return RERENDER_NONE; }
	int virtual onTouchMove(TouchState& aTouchState, Config& aConfig) { return RERENDER_NONE; }
	int virtual onTouchMoveEnd(TouchState& aTouchState, Config& aConfig) { return RERENDER_NONE; }
	std::function<int(TouchState& aTouchState, Config& aConfig)> onTouchDownLong;
public:
	virtual void move(int dx, int dy)
	{
		position = rectOffset(position, dx, dy);
		for (auto& child : children)
			child->move(dx, dy);
	}
public:
	virtual bool render(Config& aConfig, int x, int y)
	{
		auto res = renderThis(aConfig, x, y);
		if (renderChildren(aConfig, x, y))
			res = true;
		return res;
	}
	virtual bool renderThis(Config& aConfig, int x, int y) { return false; }
	virtual bool renderChildren(Config& aConfig, int x, int y)
	{ 
		auto res = false;
		for (auto& c : children)
		{
			if (c->visible && c->render(aConfig, x, y))
				res = true;
		}
		return res; 
	}
	virtual int handleInit(Config& aConfig)
	{
		auto res = onInit(aConfig);
		for (auto& c : children)
		{
			auto resChild = c->handleInit(aConfig);
			if (res < resChild)
				res = resChild;
		}
		return res;
	}
	virtual void handleExit(Config& aConfig)
	{
		onExit(aConfig);
		for (auto& c : children)
			c->handleExit(aConfig);
	}
	virtual void activate(Config& aConfig, bool aFull)  
	{ 
		for (auto& child : children)
			child->activate(aConfig, aFull);
	}
	virtual void deactivate(Config& aConfig) 
	{ 
		for (auto& child : children)
			child->deactivate(aConfig);
	}
public:
	void addChild(std::shared_ptr<Element> aChild)
	{
		children.push_back(aChild);
		aChild->parent = this;
	}
	void removeChild(Element* aChild)
	{
		children.erase(std::remove_if(children.begin(), children.end(), [aChild](auto child) -> bool { return child.get() == aChild; }), children.end());
	}
	void removeFromParent()
	{
		if (parent)
			parent->removeChild(this);
	}
	void removeParentFromParent()
	{
		if (parent && parent->parent)
			parent->parent->removeChild(parent);
	}
	void clearChildren(Config& aConfig)
	{
		for (auto& child : children)
		{
			child->handleExit(aConfig);
			child->parent = nullptr;
		}
		children.clear();
	}
	virtual bool inElement(int x, int y)
	{
		return (position.x <= x) && (x - position.x <= position.w) && (position.y <= y) && (y - position.y <= position.h);
	}
	virtual Element* findElementRev(int x, int y, bool aOnlyVisible, bool aOnlyEnabled)
	{
		if (inElement(x, y) && checkState(aOnlyVisible, aOnlyEnabled))
		{
			for (int i = (int)children.size() - 1; i >= 0; i--)
			{
				auto& c = children[i];
				auto s = c->findElementRev(x, y, aOnlyVisible, aOnlyEnabled);
				if (s)
					return s;
			}
			// fall through to this because not in any child of us
			return touchable ? this : nullptr;
		}
		// fall through, not in this element so not in any child of us
		return nullptr;
	}
	bool checkState(bool aVisible = false, bool aEnabled = false)
	{
		return (!aVisible || visible) && (!aEnabled || enabled);
	}
};

class Panel : public Element
{
public:
	Panel(const SDL_Rect& aPosition)
		: Element(aPosition, false, true)
	{
	}
public:
	virtual std::string elementName() override { return "Panel"; }
};

class Animation : public Element
{
public:
	Animation(SDLRenderer* aRenderer, fs::path aDirectory, const std::string& aDayPostfix, const std::string& aNightPostfix, const SDL_Rect& aPosition)
		: Element(aPosition, false, true)
	{
		fDirectory = aDirectory;
		fDayPostfix = aDayPostfix;
		fNightPostfix = aNightPostfix;
	}
public:
	virtual std::string elementName() override { return "Animation"; }
	virtual bool renderThis(Config& aConfig, int x, int y) override
	{ 
		// render current frame
		if (checkState(true, false) && 0 <= currentFrameIndex && currentFrameIndex < int(frames.size()))
		{
			std::scoped_lock guard(fLock);
			auto frame = frames[currentFrameIndex];
			auto rect = relativeDisplayPosition(x, y);
			aConfig.renderer->drawTexture(frame, &rect);
			return true;
		}
		else
			return false; 
	}
	virtual void activate(Config& aConfig, bool aFull) override
	{ 
		aConfig.startAnimation(this);
		Element::activate(aConfig, aFull);
		if (aFull && !allFramesLoaded)
		{
			// load all frames via thread
			auto t = std::thread([this, &aConfig]()-> void { load(aConfig.renderer); });
			// immediate detach can fail start of thread? set allFramesLoaded=true in thread so we can re-trigger if thread was not started, seems to work
			t.detach();
		}
	}
	virtual void deactivate(Config& aConfig) override
	{ 
		aConfig.cancelAnimation(this);
		Element::deactivate(aConfig);
	}
public:
	void load(SDLRenderer* aRenderer, bool aAll = true)
	{
		allFramesLoaded = aAll;
		int hour = 0;
		LocalDateTime::timeParts(LocalDateTime::now(), &hour);
		// add post fix depending on time of day (or nothing if postfixes are empty)
		auto path = 7 <= hour && hour <= 19 ? fs::path(fDirectory.string() + fDayPostfix) : fs::path(fDirectory.string() + fNightPostfix); // todo: less hardcoded
		std::cout << "loading (" << aAll << ") " << path << "\r";
		std::cout.flush();
		try
		{
			std::map<std::string, fs::path> sortedImagePaths;
			for (const auto& entry : fs::directory_iterator(path))
			{
				if (fs::is_regular_file(entry.path()))
					sortedImagePaths[entry.path().filename().string()] = entry.path();
			}
			std::vector<SDL_Surface*> localFrames;
			int i = 0;
			for (auto& entry : sortedImagePaths)
			{
				if (aAll || i < ANIMATION_INITIAL_FRAMES)
				{
					if (i < frames.size())
					{
						if (i % 25 == 0)
						{
							std::cout << "loading (copy) " << i << "\r";
							std::cout.flush();
						}
						localFrames.push_back(frames[i]);
					}
					else
					{
						if (i % 25 == 0)
						{
							std::cout << "loading " << i << "\r";
							std::cout.flush();
						}
						localFrames.push_back(aRenderer->loadSurface(entry.second.string(), false));
					}
					i++;
				}
			}
			std::cout << "pre loaded (" << aAll << ") " << path << std::endl;
			std::scoped_lock guard(fLock);
			frames = localFrames;
			std::cout << "loaded (" << aAll << ") " << path << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << "## exception loading (" << aAll << ") " << path << ": " << e.what() << std::endl;
		}
	}
public:
	std::recursive_mutex fLock;
	fs::path fDirectory;
	std::string fDayPostfix;
	std::string fNightPostfix;
	//std::vector<SDL_Texture*> frames;
	std::vector<SDL_Surface*> frames;
	int currentFrameIndex = -1;
	bool allFramesLoaded = false;
public:
	//SDL_Texture* nextFrame()
	SDL_Surface* nextFrame()
	{
		//std::cout << "nf " << currentFrameIndex << "       \r";
		//std::cout.flush();
		currentFrameIndex++;
		std::scoped_lock guard(fLock);
		if (currentFrameIndex >= int(frames.size()))
		{
			currentFrameIndex = 0;
			if (currentFrameIndex < int(frames.size()))
				return frames[currentFrameIndex];
			else
				return nullptr;
		}
		else
			return frames[currentFrameIndex];
	}
};

Uint32 handleNextAnimationFrame(Uint32 interval, void* param)
{
	auto config = (Config*)param;
	if (config && config->renderer && config->animations.size() > 0)
	{
		std::scoped_lock guard(config->lock);
		for (auto& animation : config->animations)
			animation->nextFrame();
		sendRerenderEvent();
		return interval;
	}
	else
		return 0;
}

Uint32 handleLongTouchDownTimerEvent(Uint32 interval, void* param)
{
	((TouchState*)param)->touchDownTimerID = 0;
	SDL_Event sdl_event;
	SDL_memset(&sdl_event, 0, sizeof(sdl_event));
	sdl_event.type = longTouchDownEvent;
	SDL_PushEvent(&sdl_event);
	return 0;
}

class SDLEventLoop
{
public:
	SDLEventLoop(Config* aConfig)
	{
		fConfig = aConfig;
	}
	void loop()
	{
		fConfig->currentTime = LocalDateTime::now();
		fConfig->recalculateAlarms(fConfig->currentTime);

		// init

		if (handleInit(*fConfig))
		{
			std::scoped_lock guard(fConfig->lock);
			render(fConfig->renderer);
			fConfig->renderer->update();
		}

		// activate
		activateAll(*fConfig, false);


		SDL_Event e;
#ifdef SDL1
		time_t startTime = LocalDateTime::now();
#else
		time_t startTime = 0;
#endif
		while (!fConfig->quit)
		{
			//Handle events on queue
			if (SDL_WaitEvent(&e))
			{
#ifdef SDL1
				auto now = LocalDateTime::now();
				fTouchState.currentTimeStamp = now - startTime;
				fConfig->currentTime = now;
#else
				fTouchState.currentTimeStamp = e.common.timestamp;
				// calculate delta to start time from SDL timestamp
				time_t delta = e.common.timestamp / 1000;
				// check if we must recalculate startTime because it was not yet set or we have a timestamp overflow 
				// by checking if it is less that the last calculated time
				if (startTime + delta < fTouchState.currentTime)
					startTime = LocalDateTime::now() - delta;
				fTouchState.currentTime = startTime + delta;
				fConfig->currentTime = fTouchState.currentTime;
#endif

				int rerender = 0;
				do
				{
#ifdef SDL1
					auto timestamp = fTouchState.currentTimeStamp;
#else
					auto timestamp = e.common.timestamp;
#endif
					// User requests quit
					if (e.type == SDL_QUIT)
					{
						fConfig->quit = true;
						std::cout << "## SDL_QUIT" << std::endl;
					}
					else if (e.type == SDL_KEYDOWN)
					{
						switch (e.key.keysym.sym)
						{
						case SDLK_ESCAPE:
						case SDLK_q:
							fConfig->quit = true;
							std::cout << ">> quit by event SDL_KEYDOWN " << e.key.keysym.sym << " @ " << timestamp << std::endl;
							break;
						default:
							std::cout << "   event SDL_KEYDOWN " << e.key.keysym.sym << " @ " << timestamp << std::endl;
							break;
						}
					}
#ifdef SDL1
					else if (e.type == touchDNEvent)
#else
					else if (e.type == SDL_MOUSEBUTTONDOWN)
#endif
					{
						std::cout << "   event SDL_MOUSEBUTTONDOWN (" << (int)e.type << ") " << e.button.x << "," << e.button.y << " @ " << timestamp << std::endl;
						down(e.button.x, e.button.y, rerender);
					}
#ifndef SDL1
					else if (e.type == SDL_FINGERDOWN)
					{
						std::cout << "   event SDL_FINGERDOWN " << e.tfinger.x << "," << e.tfinger.y << " @ " << e.common.timestamp << std::endl;
						down(int(std::round(e.tfinger.x * float(fConfig->screenSize.x))), int(std::round(e.tfinger.y * float(fConfig->screenSize.y))), rerender);
					}
#endif
#ifdef SDL1
					else if (e.type == touchUPEvent)
#else
					else if (e.type == SDL_MOUSEBUTTONUP)
#endif
					{
						std::cout << "   event SDL_MOUSEBUTTONUP (" << (int)e.type << ") " << e.button.x << "," << e.button.y << " @ " << timestamp << std::endl;
						up(e.button.x, e.button.y, rerender);
					}
#ifndef SDL1
					else if (e.type == SDL_FINGERUP)
					{
						std::cout << "   event SDL_FINGERUP " << e.tfinger.x << "," << e.tfinger.y << " @ " << e.common.timestamp << std::endl;
						up(int(std::round(e.tfinger.x * float(fConfig->screenSize.x))), int(std::round(e.tfinger.y * float(fConfig->screenSize.y))), rerender);
					}
#endif
#ifdef SDL1
					else if (e.type == touchMVEvent)
#else
					else if (e.type == SDL_MOUSEMOTION)
#endif
					{
						if (fTouchState.moving)
							std::cout << "   event SDL_MOUSEMOTION (" << (int)e.type << ") " << e.motion.x << "," << e.motion.y << " @ " << timestamp << std::endl; // "                              \r"; // avoid filling window with mouse motion events 
						move(e.motion.x, e.motion.y, rerender);
						
					}
#ifndef SDL1
					else if (e.type == SDL_FINGERMOTION)
					{
						std::cout << "   event SDL_FINGERMOTION " << e.tfinger.x << "," << e.tfinger.y << " @ " << e.common.timestamp << "                               \r"; // avoid filling window with mouse motion events 
						move(int(std::round(e.tfinger.x * float(fConfig->screenSize.x))), int(std::round(e.tfinger.y * float(fConfig->screenSize.y))), rerender);
					}
#endif
					else if (e.type == longTouchDownEvent)
					{
						// clear previous timer
						if (fTouchState.touchDownTimerID != 0)
						{
							SDL_RemoveTimer(fTouchState.touchDownTimerID);
							fTouchState.touchDownTimerID = 0;
						}
						std::cout << "   event LONG PRESS user event " << " @ " << timestamp << std::endl;
						// end of touch event, no position update
						if (fTouchState.touchDownElement && !fTouchState.moving)
						{
							if (fTouchState.touchDownElement->onTouchDownLong)
								rerender = fTouchState.touchDownElement->onTouchDownLong(fTouchState, *fConfig);
						}
					}
					else if (e.type == rerenderEvent)
					{
						rerender = RERENDER_ALL;
					}
					else
					{
						std::cout << ">> event " << e.type << " @ " << timestamp << std::endl;
					}
				} while (SDL_PollEvent(&e));
				{
					// dispatch 
					std::scoped_lock guard(fConfig->lock);
					for (auto& f : dispatch)
					{
						f();
						// if a dispatch function is found 'forget' last touchDownElement
						fTouchState.touchDownElement = nullptr;
					}
					dispatch.clear();
				}
				switch (rerender)
				{
				case RERENDER_ALL:
					{
					std::scoped_lock guard(fConfig->lock);
						render(fConfig->renderer);
						fConfig->renderer->update();
					}
					break;
				case RERENDER_ALL_CLEAR_FIRST:
					{
					std::scoped_lock guard(fConfig->lock);
						fConfig->renderer->clear();
						render(fConfig->renderer);
						fConfig->renderer->update();
					}
				}
			}
		}

		deactivateAll(*fConfig);

		handleExit(*fConfig);
	}
	void down(int x, int y, int& rerender)
	{
		// clear previous timer
		if (fTouchState.touchDownTimerID != 0)
		{
			SDL_RemoveTimer(fTouchState.touchDownTimerID);
			fTouchState.touchDownTimerID = 0;
		}
		
		// start of touch event
		fTouchState.currentX = x;
		fTouchState.currentY = y;
		// store time
		fTouchState.touchDownTimeStamp = fTouchState.currentTimeStamp;
		// store position
		fTouchState.touchDownX = x;
		fTouchState.touchDownY = y;
		// find element
		fTouchState.touchDownElement = findElementRev(x, y, true, true);
		// pass event
		if (fTouchState.touchDownElement)
		{
			std::cout << "   down @ " << fTouchState.touchDownElement->elementName() << std::endl;
			auto r = fTouchState.touchDownElement->onTouchDown(fTouchState, *fConfig);
			if (rerender < r)
				rerender = r;
			if (fTouchState.touchDownElement->onTouchDownLong)
				fTouchState.touchDownTimerID = SDL_AddTimer(TOUCH_SHORT_TOUCH_TIMESTAMP_LIMIT, &handleLongTouchDownTimerEvent, &fTouchState);
		}
	}
	void up(int x, int y, int& rerender)
	{
		// clear previous timer
		if (fTouchState.touchDownTimerID != 0)
		{
			SDL_RemoveTimer(fTouchState.touchDownTimerID);
			fTouchState.touchDownTimerID = 0;
		}
		// end of touch event
		fTouchState.currentX = x;
		fTouchState.currentY = y;
		if (fTouchState.touchDownElement)
		{
			int r;
			if (fTouchState.moving)
			{
				r = fTouchState.touchDownElement->onTouchMoveEnd(fTouchState, *fConfig);
				fTouchState.moving = false;
			}
			else if (fTouchState.currentTimeStamp - fTouchState.touchDownTimeStamp <= TOUCH_SHORT_TOUCH_TIMESTAMP_LIMIT)
				r = fTouchState.touchDownElement->onTouchUp(fTouchState, *fConfig);
			else
				r = fTouchState.touchDownElement->onTouchUpLong(fTouchState, *fConfig);
			if (rerender < r)
				rerender = r;
			fTouchState.touchDownElement = nullptr;
		}
	}
	void move(int x, int y, int& rerender)
	{
		// moving
		fTouchState.currentX = x;
		fTouchState.currentY = y;
		// handle if touch is registered
		if (fTouchState.touchDownElement)
		{
			int r = 0;
			if (!fTouchState.moving)
			{
				if (fTouchState.moved() >= TOUCH_MOVED_MIN_DISTANCE)
				{
					// clear previous timer
					if (fTouchState.touchDownTimerID != 0)
					{
						SDL_RemoveTimer(fTouchState.touchDownTimerID);
						fTouchState.touchDownTimerID = 0;
					}
					fTouchState.moving = true;
					r = fTouchState.touchDownElement->onTouchMoveStart(fTouchState, *fConfig);
				}
			}
			else
				r = fTouchState.touchDownElement->onTouchMove(fTouchState, *fConfig);
			if (rerender < r)
				rerender = r;
		}
	}
public:
	void addElement(std::shared_ptr<Element> aElement)
	{
		fChildren.push_back(aElement);
	}
	Element* findElementRev(int x, int y, bool aOnlyVisible, bool aOnlyEnabled)
	{
		for (int i = (int)fChildren.size() - 1; i >= 0; i--)
		{
			auto& c = fChildren[i];
			auto s = c->findElementRev(x, y, aOnlyVisible, aOnlyEnabled);
			if (s)
				return s;
		}
		// fall through, not in any child element os sub child
		return nullptr;
	}
	bool render(SDLRenderer* aRenderer)
	{
		auto res = false;
		for (auto& c : fChildren)
		{
			if (c->visible && c->render(*fConfig, 0, 0))
				res = true;
		}
		return res;
	}
	int handleInit(Config& aConfig)
	{
		auto res = RERENDER_NONE;
		for (auto& c : fChildren)
		{
			auto resChild = c->handleInit(aConfig);
			if (res < resChild)
				res = resChild;
		}
		return res;
	}
	void handleExit(Config& aConfig)
	{
		for (auto& c : fChildren)
			c->handleExit(aConfig);
	}
	void activateAll(Config& aConfig, bool aFull)
	{
		for (auto& c : fChildren)
			c->activate(aConfig, aFull);
	}
	void deactivateAll(Config& aConfig)
	{
		for (auto& c : fChildren)
			c->deactivate(aConfig);
	}
private:
	std::vector<std::shared_ptr<Element>> fChildren;
	Config* fConfig;
	TouchState fTouchState;
};
