#pragma once

#include <iostream>
#include <cstdlib>
#include <vector>
#include <unordered_map>
#include <mutex>

#include <filesystem>
namespace fs = std::filesystem;

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_rotozoom.h>


class SDL12
{
public:
	bool sdl_init = false;
	bool ttf_init = false;
	bool mix_init = false;
	bool img_init = false;

	SDL12(bool aShowMouse)
	{
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER ) < 0) {
			std::cerr << "error initializing SDL: " << SDL_GetError() << std::endl;
			exit(1);
		}
		sdl_init = true;
		std::cout<< "after sdl init" << std::endl;
		if (TTF_Init() < 0)
		{
			std::cerr << "## could not initialize sdl ttf, error: " << TTF_GetError() << std::endl;
			exit(1);
		}
		ttf_init = true;
		std::cout << "after sdl ttf init" << std::endl;
		if (!(Mix_Init(MIX_INIT_MP3) & MIX_INIT_MP3))
		{

		}
		mix_init = true;
		std::cout << "after sdl mix init" << std::endl;
		if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
		{
			std::cerr << "##could not initialize sdl image, error: " << IMG_GetError() << std::endl;
			exit(1);
		}
		img_init = true;
		std::cout << "after sdl image init" << std::endl;
		if (!aShowMouse)
			SDL_ShowCursor(SDL_DISABLE);
		std::cout << "sdl initialized" << std::endl;
		// open audio device
		Mix_Volume(-1, 0);
		Mix_OpenAudio(48000, MIX_DEFAULT_FORMAT, 2, 4096);
		std::cout << "audio initialized" << std::endl;
	}
	~SDL12()
	{
		stop();
	}
	void stop()
	{
		std::cout << "sdl stop" << std::endl;
		if (img_init)
		{
			IMG_Quit();
			img_init = false;
		}
		if (mix_init)
		{
			Mix_Quit();
			mix_init = false;
		}
		if (ttf_init)
		{
			TTF_Quit();
			ttf_init = false;
		}
		//SDL_ShowCursor(SDL_ENABLE); // show mouse
		if (sdl_init)
		{
			SDL_Quit();
			sdl_init = false;
		}
		std::cout << "sdl stopped" << std::endl;
	}
};


typedef struct SDL_Point
{
	Sint16 x;
	Sint16 y;
} SDL_Point;

typedef SDL_Surface SDL_Texture;

typedef SDL_Surface SDL_Renderer;

/*
todo: SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");
https://discourse.libsdl.org/t/drawing-lines-with-float-coordinates/27140
prob. not helping..
*/

class SDLRenderer
{
private:
	SDL_Surface* fScreen = nullptr;
	std::unordered_map<std::string, std::shared_ptr<std::unordered_map<int, TTF_Font*>>> fFonts;
	std::unordered_map<std::string, SDL_Surface*> fSurfaces;
	fs::path fFontPath;
public:
	int width = 0;
	int height = 0;
	int depth = 0;
public:
	SDLRenderer(fs::path aFontPath, bool aRenderToWindow = false, int aWidth = 256, int aHeight = 256, int aDepth = 32)
	{
		width = aWidth;
		height = aHeight;
		depth = aDepth;
		fFontPath = aFontPath;
		fScreen = SDL_SetVideoMode(aWidth, aHeight, aDepth, SDL_HWSURFACE | SDL_DOUBLEBUF); // SDL_FULLSCREEN | SDL_DOUBLEBUF | SDL_HWSURFACE); // Create a window with SDL
		if (!fScreen)
			std::cerr << "## could not create a main rendering surface" << std::endl;
		else
		{
			std::cout << "created a main rendering surface" << std::endl;
		}

	}
	~SDLRenderer()
	{
		stop();
	}
	void stop()
	{
		for (auto& surface : fSurfaces)
		{
			SDL_FreeSurface(surface.second);
		}
		fSurfaces.clear();
		for (auto& fontGroup : fFonts)
		{
			if (fontGroup.second)
			{
				for (auto& font : *fontGroup.second)
				{
					if (font.second)
						TTF_CloseFont(font.second);
				}
			}
		}
		fFonts.clear();
		if (fScreen)
		{
			SDL_FreeSurface(fScreen);
			fScreen = nullptr;
		}
		// cleanup cache
		if (gfxPrimitivesPolyIntsCache != nullptr)
		{
			free(gfxPrimitivesPolyIntsCache);
			gfxPrimitivesPolyIntsCache = nullptr;
		}
		gfxPrimitivesPolyAllocatedCache = 0;
	}
	void clear()
	{
		SDL_FillRect(fScreen, nullptr, 0xff000000);
	}
	void update()
	{
		SDL_Flip(fScreen);
	}
	//SDL_Surface* getRenderer() { return fScreen; }
	TTF_Font* getFont(int aSize = 10, std::string aFontName = "")
	{ 
		// if not font name specified use the default ariel
		if (aFontName.length() == 0)
			aFontName = "arial.ttf";
		// find font group (font name) or add when not there yet
		std::shared_ptr<std::unordered_map<int, TTF_Font*>> fg;
		auto fgp = fFonts.find(aFontName);
		if (fgp == fFonts.end())
		{
			fg = std::make_shared<std::unordered_map<int, TTF_Font*>>();
			fFonts.insert({ aFontName, fg });
		}
		else
			fg = fgp->second;
		// find font on size in group, create and add if not there yet
		auto f = fg->find(aSize);
		if (f == fg->end())
		{
			auto newFontPath = fFontPath / aFontName;
			auto font = TTF_OpenFont(newFontPath.string().c_str(), aSize);
			fg->insert({ aSize, font });
			return font;
		}
		// fall through when already in list
		return f->second; 
	} 
public:
	void drawRectFilled(SDL_Rect aRect, uint32_t aColor)
	{ 
		auto color = colorToSDLColor(aColor);
		boxRGBA(fScreen, Sint16(aRect.x), Sint16(aRect.y), Sint16(aRect.x + aRect.w), Sint16(aRect.y + aRect.h), color.r, color.g, color.b, color.unused);
	}
	void drawRoundedRectFilled(SDL_Rect rect, int rr_radius, uint32_t aColor)
	{
		// aColor is 0xAABBGGRR
		// SDL_Color is rgba
		//Uint32 color = (Uint8)((aColor >> 0) & 0xFF) << 24 | (Uint8)((aColor >> 8) & 0xFF) << 16 | (Uint8)((aColor >> 16) & 0xFF) << 8 | (Uint8)((aColor >> 24) & 0xFF);
		roundedBoxColor(fScreen, (Sint16)rect.x, (Sint16)rect.y, (Sint16)(rect.x + rect.w), (Sint16)(rect.y + rect.h), (Sint16)rr_radius, convertColor(aColor));
	}
	void drawLine(int x1, int y1, int x2, int y2, uint32_t color)
	{
		lineColor(fScreen, (Sint16)x1, (Sint16)y1, (Sint16)x2, (Sint16)y2, color);
	}
	void drawAALine(int x1, int y1, int x2, int y2, uint32_t aColor)
	{
		aalineColor(fScreen, Sint16(x1), Sint16(y1), Sint16(x2), Sint16(y2), convertColor(aColor));
	}
	void drawHLineColor(int x1, int x2, int y, uint32_t aColor)
	{
		hlineColor(fScreen, (Sint16)x1, (Sint16)x2, (Sint16)y, convertColor(aColor));
	}
	void drawFilledTrigon(int x1, int y1, int x2, int y2, int x3, int y3, uint32_t aColor)
	{
		auto color = colorToSDLColor(aColor);
		filledTrigonRGBA(fScreen, Sint16(x1), Sint16(y1), Sint16(x2), Sint16(y2), Sint16(x3), Sint16(y3), color.r, color.g, color.b, color.unused);
	}
	void drawAAFilledTrigon(int x1, int y1, int x2, int y2, int x3, int y3, uint32_t aColor)
	{
		auto color = colorToSDLColor(aColor);
		filledTrigonRGBA(fScreen, Sint16(x1), Sint16(y1), Sint16(x2), Sint16(y2), Sint16(x3), Sint16(y3), color.r, color.g, color.b, color.unused);
		aatrigonRGBA(fScreen, Sint16(x1), Sint16(y1), Sint16(x2), Sint16(y2), Sint16(x3), Sint16(y3), color.r, color.g, color.b, color.unused);

	}
	SDL_Rect textRect(const std::string& aText, TTF_Font* aFont, int x, int y)
	{
		int w, h;
		TTF_SizeText(aFont, aText.c_str(), &w, &h);
		return SDL_Rect{ (Sint16)x,(Sint16)y,(Uint16)w,(Uint16)h };
	}
	void  drawText(const std::string& aText, TTF_Font* aFont, uint32_t aColor, SDL_Rect* aRect)
	{
		auto color = colorToSDLColor(aColor);
		auto alpha = color.unused;
		if (alpha != 0xff)
		{
			color.unused = 0xff;
			auto surface = TTF_RenderUTF8_Solid(aFont, aText.c_str(), color);
			SDL_SetAlpha(surface, SDL_SRCALPHA, alpha);
			auto rect = *aRect; // SDL_BlitSurface updates rect!
			SDL_BlitSurface(surface, NULL, fScreen, &rect);
			//auto alphaSurface = SDL_DisplayFormatAlpha(surface);
			SDL_FreeSurface(surface);
			//SDL_BlitSurface(alphaSurface, NULL, fScreen, aRect);
			//SDL_FreeSurface(alphaSurface);
		}
		else
		{
			auto surface = TTF_RenderUTF8_Blended(aFont, aText.c_str(), color);
			auto rect = *aRect; // SDL_BlitSurface updates rect!
			SDL_BlitSurface(surface, NULL, fScreen, &rect);
			SDL_FreeSurface(surface);
		}
	}
	/*
	void drawTextCenterAngled(const std::string& aText, const std::string& aFontName, int aFontSize, int x, int y, double aAngle, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE, Uint8 rb = 0, Uint8 gb = 0, Uint8 bb = 0, Uint8 ab = SDL_ALPHA_TRANSPARENT)
	{
		auto font = getFont(aFontSize, aFontName);
		if (font)
		{
			auto textSurface = TTF_RenderText_Blended(font, aText.c_str(), { r, g, b, a });
			auto rotatedTextSurface = aAngle != 0 ? rotozoomSurface(textSurface, aAngle, 1.0, SMOOTHING_ON) : nullptr;
			auto textTexture = SDL_CreateTextureFromSurface(fRenderer, rotatedTextSurface ? rotatedTextSurface : textSurface);
			auto w = rotatedTextSurface ? rotatedTextSurface->w : textSurface->w;
			auto h = rotatedTextSurface ? rotatedTextSurface->h : textSurface->h;
			SDL_Rect textRect{ x - (int)std::round((double)w / 2.0),y - (int)std::round((double)h / 2.0),w,h };
			SDL_FreeSurface(rotatedTextSurface);
			SDL_FreeSurface(textSurface);
			if (ab != SDL_ALPHA_TRANSPARENT)
			{
				// add some extra pixels in front en at end of string half font size 
				SDL_Surface* backgroundSurface = SDL_CreateRGBSurface(0, textSurface->w + aFontSize, textSurface->h, depth, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
				SDL_Rect backgroundSurfaceRect{0,0,backgroundSurface->w,backgroundSurface->h };
				SDL_FillRect(backgroundSurface, &backgroundSurfaceRect, SDL_MapRGB(backgroundSurface->format, rb, gb, bb));
				auto rotatedBackgroundSurface = rotozoomSurface(backgroundSurface, aAngle, 1.0, SMOOTHING_ON);
				if (rotatedBackgroundSurface)
				{
					auto backgroundTexture = SDL_CreateTextureFromSurface(fRenderer, rotatedBackgroundSurface);
					SDL_Rect backgroundRect{ x - (int)std::round((double)rotatedBackgroundSurface->w / 2.0),y - (int)std::round((double)rotatedBackgroundSurface->h / 2.0),rotatedBackgroundSurface->w,rotatedBackgroundSurface->h };
					SDL_RenderCopy(fRenderer, backgroundTexture, nullptr, &backgroundRect);
					SDL_FreeSurface(rotatedBackgroundSurface);
				}
				// else error, just ignore
				SDL_FreeSurface(backgroundSurface);
			}
			SDL_RenderCopy(fRenderer, textTexture, nullptr, &textRect);
			SDL_DestroyTexture(textTexture);
		}
	}
	*/
	/*
	void drawPoint(int x, int y, uint32_t color) // alpha color from world argb -> r,g,b,a
	{
		auto a = (Uint8)((color >> 24) & 0xff);
		if (a < 255)
			SDL_SetRenderDrawBlendMode(fRenderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(fRenderer, (Uint8)((color >> 16) & 0xff), (Uint8)((color >> 8) & 0xff), (Uint8)((color >> 0) & 0xff), a);
		SDL_RenderDrawPoint(fRenderer, x, y);
	}
	*/
	void drawEllipseFilled(int x, int y, int rx, int ry, uint32_t aColor)
	{
		SDL_Color color = colorToSDLColor(aColor);
		filledEllipseRGBA(fScreen, (Sint16)x, (Sint16)y, (Sint16)rx, (Sint16)ry, color.r, color.g, color.b, color.unused);
	}
	void drawEllipseOutline(int x, int y, int rx, int ry, uint32_t aColor)
	{
		SDL_Color color = colorToSDLColor(aColor);
		aaellipseRGBA(fScreen, (Sint16)x, (Sint16)y, (Sint16)rx, (Sint16)ry, color.r, color.g, color.b, color.unused);
	}
	void drawTexture(SDL_Texture* aTexture, SDL_Rect* aRect)
	{
		auto rect = *aRect; // SDL_BlitSurface updates rect!
		SDL_BlitSurface(aTexture, NULL, fScreen, &rect);
	}
	void drawTextureScale(SDL_Texture* aTexture, SDL_Rect* aRect)
	{
		if (aRect->w != aTexture->w || aRect->h != aTexture->h)
		{
			auto zoomed = zoomSurface(aTexture, double(aRect->w) / double(aTexture->w), double(aRect->h) / double(aTexture->h), 1);
			auto rect = *aRect; // SDL_BlitSurface updates rect!
			SDL_BlitSurface(zoomed, NULL, fScreen, &rect);
			SDL_FreeSurface(zoomed);
		}
		else
		{
			auto rect = *aRect; // SDL_BlitSurface updates rect!
			SDL_BlitSurface(aTexture, NULL, fScreen, &rect);
		}
	}
	void drawTextureRotated(SDL_Texture* aTexture, SDL_Rect* aRect, double aAngle, SDL_Point* aCenter)
	{
		auto zoomed = rotozoomSurfaceXY(aTexture, -aAngle, double(aRect->w) / double(aTexture->w), double(aRect->h) / double(aTexture->h), 1);
		SDL_Rect zoomedRect{ aRect->x + aCenter->x - (zoomed->w / 2),aRect->y + aCenter->y - (zoomed->h / 2),zoomed->w,zoomed->h };
		SDL_BlitSurface(zoomed, NULL, fScreen, &zoomedRect);
		SDL_FreeSurface(zoomed);
	}
	/*
	void drawTextureA(SDL_Texture* aTexture, SDL_Rect* aRect, uint8_t aAlpha)
	{
		SDL_SetAlpha(aTexture, SDL_SRCALPHA, aAlpha);
		auto rect = *aRect; // SDL_BlitSurface updates rect!
		SDL_BlitSurface(aTexture, NULL, fScreen, &rect);
	}
	*/
	SDL_Texture* createTargetTexture(const fs::path& aPath, SDL_Texture* aRefTexture)
	{
/*
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
		auto res = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, aRefTexture->w, aRefTexture->h, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
#else
		auto res = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, aRefTexture->w, aRefTexture->h, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
#endif
		return res;
*/
		return SDL_CreateRGBSurface(0, aRefTexture->w, aRefTexture->h, 32, 0, 0, 0, 0);
	}
	void drawTextureOnTexture(SDL_Texture* aSrcTexture, SDL_Texture* aDstTexture)
	{
		SDL_BlitSurface(aSrcTexture, nullptr, aDstTexture, nullptr);
	}
public:
	uint32_t rgbaToColor(Uint8 r = 255, Uint8 g = 255, Uint8 b = 255, Uint8 a = SDL_ALPHA_OPAQUE)
	{
		return (uint32_t(a) << 24) | (uint32_t(b) << 16) | (uint32_t(g) << 8) | uint32_t(r);
	}
	SDL_Color colorToSDLColor(uint32_t aColor)
	{
		// aColor is 0xAABBGGRR
		// SDL_Color is rgba
		return SDL_Color{ (Uint8)((aColor >> 0) & 0xFF),(Uint8)((aColor >> 8) & 0xFF),(Uint8)((aColor >> 16) & 0xFF),(Uint8)((aColor >> 24) & 0xFF) };
	}
	uint32_t convertColor(uint32_t aColor)
	{
		// aColor is 0xAABBGGRR
		// SDL_Color is rgba
		return ((aColor & 0xFF) << 24) | (((aColor >> 8) & 0xFF) << 16) | (((aColor >> 16) & 0xFF) << 8) | ((aColor >> 24) & 0xFF);
	}
	uint32_t alphaColor(uint32_t aColor, double aAlpha)
	{
		auto color = colorToSDLColor(aColor);
		if (aAlpha >= 1)
			color.unused = Uint8(double(color.unused) * aAlpha);
		return rgbaToColor(color.r, color.g, color.b, color.unused);
	}
	SDL_Texture* loadTexture(const fs::path& aPath, bool aAlpha = true, bool aOptimized = true, bool aStore = true)
	{
		return loadSurface(aPath, aAlpha, aOptimized, aStore);
	}
	SDL_Surface* loadSurface(const fs::path& aPath, bool aAlpha = true, bool aOptimized = true, bool aStore = true)
	{
		auto pathStr = aPath.string();
		auto it = fSurfaces.find(pathStr);
		if (it == fSurfaces.end())
		{
			if (fs::exists(aPath))
			{
				auto surface = IMG_Load(pathStr.c_str());
				if (aOptimized)
				{
					auto optimizedSurface = aAlpha ? SDL_DisplayFormatAlpha(surface) : SDL_DisplayFormat(surface);
					SDL_FreeSurface(surface);
					if (aStore)
						fSurfaces.insert({ pathStr, optimizedSurface });
					return optimizedSurface;
				}
				else
				{
					if (aStore)
						fSurfaces.insert({ pathStr, surface });
					return surface;
				}
			}
			else
			{
				std::cerr << "## cannot find surface file " << pathStr << std::endl;
				return nullptr;
			}
		}
		else
			return it->second;
	}
	void freeSurface(SDL_Surface* aSurface)
	{
		if (aSurface)
			SDL_FreeSurface(aSurface);
	}
	void freeTexture(SDL_Texture* aTexture)
	{
		if (aTexture)
			SDL_FreeSurface(aTexture);
	}
	SDL_Rect surfaceRect(SDL_Surface* aSurface)
	{
		return SDL_Rect{ 0,0,(Uint16)aSurface->w,(Uint16)aSurface->h };
	}
	SDL_Rect textureRect(SDL_Texture* aTexture)
	{
		return SDL_Rect{ 0,0,(Uint16)aTexture->w,(Uint16)aTexture->h };
	}
public:
	// polygon cached for SDL2_gfx MT functions
	int* gfxPrimitivesPolyIntsCache = nullptr;
	int gfxPrimitivesPolyAllocatedCache = 0;
};
